<?php 
	$date = $e->create_date;
    $tgl =nama_hari($date).' '. tgl_indo($date);

 ?>

<div class="clearfix pt20"></div>
<div class="detail">
	<div class="date"><?= $tgl ?></div>
	<h1><?= $e->judul ?></h1>
	<div class="text_detail">
		<div class="pic">
			<img src="<?= base_url('image/berita/'.$e->img) ?>" alt="" onerror="this.onerror=null;this.src='<?= base_url() ?>asset/kemenag/assets/themes/img-default.jpg';">
			<div class="caption" "=""> <i></i></div>
		</div>

		</div>
		<p style="text-align: justify;"><?= $e->isi ?></p>
	</div>
	<div class="clearfix">
		
	</div>
	<div class="share_top">
		<div class="total_share total_baca">
			<?php if ($e->dibaca == '') {
				echo "<span>0</span>";
			}else {
			 	echo $e->dibaca;				
			} ?>
			<strong>DIBACA</strong>
		</div>
		<!-- <div class="total_share">
			<span>0</span>
			<strong>DIBAGIKAN</strong>
		</div> -->
		<div class="pull-right">
			<?php echo sharethis('facebook' , curPageURL()); ?>
			<?php echo sharethis('twitter' , curPageURL()); ?>
		</div>
			
			
			
	
		<div class="clearfix"></div>
	</div>
	<div class="clearfix pt15"></div>
</div>
<div class="t-page">Berita Lainnya</div>
<?php 
	if (!empty($berita_random)) {
		foreach ($berita_random as $b_r) { 
			$date = $b_r->create_date;
            $tgl =nama_hari($date).' '. tgl_indo($date);
			?>

			
			<a href="<?= site_url('berita-kemenag/'.$b_r->flag) ?>" class="list-4 list-6">
				<div class="ratio16_9 box_img">
					<div class="img_con"> <img src="<?= base_url('image/berita/'.$b_r->img) ?>" alt="" onerror="this.onerror=null;this.src='<?= base_url() ?>asset/kemenag/assets/themes/gorontalo/img/img-default.jpg';"> </div>
				</div>
				<div class="date"><?= $tgl ?></div>
				<h2><?= $b_r->judul ?></h2>
			</a>
<?php	}
	}
	
 ?>


<div class="clearfix"></div>
