var TableManaged = function () {

    return {
        //main function to initiate the module
        init: function () {

            if (!jQuery().dataTable) {
                return;
            }

            // begin first table
            $('#datatabel').dataTable({
                "aLengthMenu": [
                    [15, 50, 100, -1],
                    [15, 50, 100, "All"] // change per page values here
                ],
                // set the initial value
                "iDisplayLength": 15,
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ records",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
                "aoColumnDefs": [
                    {'bSortable': false, 'aTargets': [0]},
                    {"bSearchable": false, "aTargets": [0]}
                ]
            });

            jQuery('#datatabel_wrapper .dataTables_filter input').addClass("form-control input-medium input-inline"); // modify table search input
            jQuery('#datatabel_wrapper .dataTables_length select').addClass("form-control input-xsmall"); // modify table per page dropdown
            jQuery('#datatabel_wrapper .dataTables_length select').select2(); // initialize select2 dropdown
            
            
            // $('#list_pasien').dataTable({
            //     "bProcessing": true,
            //     "bServerSide": true,
            //     "sAjaxSource": baseURL + 'apps/pasien/get-list.asp',
            //     "bJQueryUI": true,
            //     "aLengthMenu": [
            //         [50, 100, 150],
            //         [50, 100, 150, "All"] // change per page values here
            //     ],
            //     "sPaginationType": "full_numbers",
            //     "iDisplayLength": 50,
            //     "iDisplayStart ": 50,
            //     "sPaginationType": "bootstrap",
            //             "oLanguage": {
            //                 "sProcessing": "<img src='" + baseURL + "assets/dist/img/loading.gif' style='margin: 0 auto;'>",
            //                 "sLengthMenu": "_MENU_ records",
            //                 "oPaginate": {
            //                     "sPrevious": "Prev",
            //                     "sNext": "Next"
            //                 }
            //             },
            //     "fnInitComplete": function () {
            //         //oTable.fnAdjustColumnSizing();
            //     },
            //     'fnServerData': function (sSource, aoData, fnCallback) {
            //         $.ajax
            //                 ({
            //                     'dataType': 'json',
            //                     'type': 'POST',
            //                     'url': sSource,
            //                     'data': aoData,
            //                     'success': fnCallback
            //                 });
            //     }
            // });

            // jQuery('#list_pasien_wrapper .dataTables_filter input').addClass("form-control input-medium input-inline"); // modify table search input
            // jQuery('#list_pasien_wrapper .dataTables_length select').addClass("form-control input-xsmall"); // modify table per page dropdown
            // jQuery('#list_pasien_wrapper .dataTables_length select').select2(); // initialize select2 dropdown

            // load data tabel provinsi
            $('#list_provinsi').dataTable({
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": baseURL + 'apps/provinsi/get-list.asp',
                "bJQueryUI": true,
                "aLengthMenu": [
                    [10, 20, 30 ,"All"],
                    [10, 20, 30, "All"] // change per page values here
                ],
                "sPaginationType": "full_numbers",
                "iDisplayLength": 50,
                "iDisplayStart ": 50,
                "sPaginationType": "bootstrap",
                        "oLanguage": {
                            "sProcessing": "<img src='" + baseURL + "assets/dist/img/loading.gif' style='margin: 0 auto;'>",
                            "sLengthMenu": "_MENU_ records",
                            "oPaginate": {
                                "sPrevious": "Prev",
                                "sNext": "Next"
                            }
                        },
                "fnInitComplete": function () {
                    //oTable.fnAdjustColumnSizing();
                },
                'fnServerData': function (sSource, aoData, fnCallback) {
                    $.ajax
                            ({
                                'dataType': 'json',
                                'type': 'POST',
                                'url': sSource,
                                'data': aoData,
                                'success': fnCallback
                            });
                }
            });

            jQuery('#list_provinsi_wrapper .dataTables_filter input').addClass("form-control input-medium input-inline"); // modify table search input
            jQuery('#list_provinsi_wrapper .dataTables_length select').addClass("form-control input-xsmall"); // modify table per page dropdown
            jQuery('#list_provinsi_wrapper .dataTables_length select').select2(); // initialize select2 dropdown

            // load tabel kabupaten
            $('#list_kabupaten').dataTable({
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": baseURL + 'apps/kabupaten/get-list.asp',
                "bJQueryUI": true,
                "aLengthMenu": [
                    [50, 100, 150],
                    [50, 100, 150] // change per page values here
                ],
                "sPaginationType": "full_numbers",
                "iDisplayLength": 50,
                "iDisplayStart ": 50,
                "sPaginationType": "bootstrap",
                        "oLanguage": {
                            "sProcessing": "<img src='" + baseURL + "assets/dist/img/loading.gif' style='margin: 0 auto;'>",
                            "sLengthMenu": "_MENU_ records",
                            "oPaginate": {
                                "sPrevious": "Prev",
                                "sNext": "Next"
                            }
                        },
                "fnInitComplete": function () {
                    //oTable.fnAdjustColumnSizing();
                },
                'fnServerData': function (sSource, aoData, fnCallback) {
                    $.ajax
                            ({
                                'dataType': 'json',
                                'type': 'POST',
                                'url': sSource,
                                'data': aoData,
                                'success': fnCallback
                            });
                }
            });

            jQuery('#list_kabupaten_wrapper .dataTables_filter input').addClass("form-control input-medium input-inline"); // modify table search input
            jQuery('#list_kabupaten_wrapper .dataTables_length select').addClass("form-control input-xsmall"); // modify table per page dropdown
            jQuery('#list_kabupaten_wrapper .dataTables_length select').select2(); // initialize select2 dropdown

            // load tabel Kecamatan
            $('#list_kecamatan').dataTable({
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": baseURL + 'apps/kecamatan/get-list.asp',
                "bJQueryUI": true,
                "aLengthMenu": [
                    [50, 100, 150],
                    [50, 100, 150] // change per page values here
                ],
                "sPaginationType": "full_numbers",
                "iDisplayLength": 50,
                "iDisplayStart ": 50,
                "sPaginationType": "bootstrap",
                        "oLanguage": {
                            "sProcessing": "<img src='" + baseURL + "assets/dist/img/loading.gif' style='margin: 0 auto;'>",
                            "sLengthMenu": "_MENU_ records",
                            "oPaginate": {
                                "sPrevious": "Prev",
                                "sNext": "Next"
                            }
                        },
                "fnInitComplete": function () {
                    //oTable.fnAdjustColumnSizing();
                },
                'fnServerData': function (sSource, aoData, fnCallback) {
                    $.ajax
                            ({
                                'dataType': 'json',
                                'type': 'POST',
                                'url': sSource,
                                'data': aoData,
                                'success': fnCallback
                            });
                }
            });

            jQuery('#list_kecamatan_wrapper .dataTables_filter input').addClass("form-control input-medium input-inline"); // modify table search input
            jQuery('#list_kecamatan_wrapper .dataTables_length select').addClass("form-control input-xsmall"); // modify table per page dropdown
            jQuery('#list_kecamatan_wrapper .dataTables_length select').select2(); // initialize select2 dropdown


             
            // load wilayah desa
            $('#list_desa').dataTable({
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": baseURL + 'apps/desa/get-list.asp',
                "bJQueryUI": true,
                "aLengthMenu": [
                    [50, 100, 150],
                    [50, 100, 150] // change per page values here
                ],
                "sPaginationType": "full_numbers",
                "iDisplayLength": 50,
                "iDisplayStart ": 50,
                "sPaginationType": "bootstrap",
                        "oLanguage": {
                            "sProcessing": "<img src='" + baseURL + "assets/dist/img/loading.gif' style='margin: 0 auto;'>",
                            "sLengthMenu": "_MENU_ records",
                            "oPaginate": {
                                "sPrevious": "Prev",
                                "sNext": "Next"
                            }
                        },
                "fnInitComplete": function () {
                    //oTable.fnAdjustColumnSizing();
                },
                'fnServerData': function (sSource, aoData, fnCallback) {
                    $.ajax
                            ({
                                'dataType': 'json',
                                'type': 'POST',
                                'url': sSource,
                                'data': aoData,
                                'success': fnCallback
                            });
                }
            });

            jQuery('#list_desa_wrapper .dataTables_filter input').addClass("form-control input-medium input-inline"); // modify table search input
            jQuery('#list_desa_wrapper .dataTables_length select').addClass("form-control input-xsmall"); // modify table per page dropdown
            jQuery('#list_desa_wrapper .dataTables_length select').select2(); // initialize select2 dropdown



            // load data user
            $('#list_user').dataTable({
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": baseURL + 'apps/user/get-list.asp',
                "bJQueryUI": true,
                "aLengthMenu": [
                    [50, 100, 150],
                    [50, 100, 150] // change per page values here
                ],
                "sPaginationType": "full_numbers",
                "iDisplayLength": 50,
                "iDisplayStart ": 50,
                "sPaginationType": "bootstrap",
                        "oLanguage": {
                            "sProcessing": "<img src='" + baseURL + "assets/dist/img/loading.gif' style='margin: 0 auto;'>",
                            "sLengthMenu": "_MENU_ records",
                            "oPaginate": {
                                "sPrevious": "Prev",
                                "sNext": "Next"
                            }
                        },
                "fnInitComplete": function () {
                    //oTable.fnAdjustColumnSizing();
                },
                'fnServerData': function (sSource, aoData, fnCallback) {
                    $.ajax
                            ({
                                'dataType': 'json',
                                'type': 'POST',
                                'url': sSource,
                                'data': aoData,
                                'success': fnCallback
                            });
                }
            });

            jQuery('#list_user_wrapper .dataTables_filter input').addClass("form-control input-medium input-inline"); // modify table search input
            jQuery('#list_user_wrapper .dataTables_length select').addClass("form-control input-xsmall"); // modify table per page dropdown
            jQuery('#list_user_wrapper .dataTables_length select').select2(); // initialize select2 dropdown


            // load data dokter
            $('#list_dokter').dataTable({
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": baseURL + 'apps/admission/dokter/get-list.asp',
                "bJQueryUI": true,
                "aLengthMenu": [
                    [50, 100, 150],
                    [50, 100, 150] // change per page values here
                ],
                "sPaginationType": "full_numbers",
                "iDisplayLength": 50,
                "iDisplayStart ": 50,
                "sPaginationType": "bootstrap",
                        "oLanguage": {
                            "sProcessing": "<img src='" + baseURL + "assets/dist/img/loading.gif' style='margin: 0 auto;'>",
                            "sLengthMenu": "_MENU_ records",
                            "oPaginate": {
                                "sPrevious": "Prev",
                                "sNext": "Next"
                            }
                        },
                "fnInitComplete": function () {
                    //oTable.fnAdjustColumnSizing();
                },
                'fnServerData': function (sSource, aoData, fnCallback) {
                    $.ajax
                            ({
                                'dataType': 'json',
                                'type': 'POST',
                                'url': sSource,
                                'data': aoData,
                                'success': fnCallback
                            });
                }
            });

            jQuery('#list_dokter_wrapper .dataTables_filter input').addClass("form-control input-medium input-inline"); // modify table search input
            jQuery('#list_dokter_wrapper .dataTables_length select').addClass("form-control input-xsmall"); // modify table per page dropdown
            jQuery('#list_dokter_wrapper .dataTables_length select').select2(); // initialize select2 dropdown


            // passien
            $('#list_pasien').dataTable({
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": baseURL + 'apps/pasien/get-list.asp',
                "bJQueryUI": true,
                "aLengthMenu": [
                    [10, 20, 30 ,"All"],
                    [10, 20, 30, "All"] // change per page values here
                ],
                "sPaginationType": "full_numbers",
                "iDisplayLength": 50,
                "iDisplayStart ": 50,
                "sPaginationType": "bootstrap",
                        "oLanguage": {
                            "sProcessing": "<img src='" + baseURL + "assets/dist/img/loading.gif' style='margin: 0 auto;'>",
                            "sLengthMenu": "_MENU_ records",
                            "oPaginate": {
                                "sPrevious": "Prev",
                                "sNext": "Next"
                            }
                        },
                "fnInitComplete": function () {
                    //oTable.fnAdjustColumnSizing();
                },
                'fnServerData': function (sSource, aoData, fnCallback) {
                    $.ajax
                            ({
                                'dataType': 'json',
                                'type': 'POST',
                                'url': sSource,
                                'data': aoData,
                                'success': fnCallback
                            });
                }
            });

            jQuery('#list_pasien_wrapper .dataTables_filter input').addClass("form-control input-medium input-inline"); // modify table search input
            jQuery('#list_pasien_wrapper .dataTables_length select').addClass("form-control input-xsmall"); // modify table per page dropdown
            jQuery('#list_pasien_wrapper .dataTables_length select').select2(); // initialize select2 dropdown


            // passien
            $('#list_pengunjung').dataTable({
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": baseURL + 'apps/pengunjung/get-list.asp',
                "bJQueryUI": true,
                "aLengthMenu": [
                    [10, 20, 30 ,"All"],
                    [10, 20, 30, "All"] // change per page values here
                ],
                "sPaginationType": "full_numbers",
                "iDisplayLength": 50,
                "iDisplayStart ": 50,
                "sPaginationType": "bootstrap",
                        "oLanguage": {
                            "sProcessing": "<img src='" + baseURL + "assets/dist/img/loading.gif' style='margin: 0 auto;'>",
                            "sLengthMenu": "_MENU_ records",
                            "oPaginate": {
                                "sPrevious": "Prev",
                                "sNext": "Next"
                            }
                        },
                "fnInitComplete": function () {
                    //oTable.fnAdjustColumnSizing();
                },
                'fnServerData': function (sSource, aoData, fnCallback) {
                    $.ajax
                            ({
                                'dataType': 'json',
                                'type': 'POST',
                                'url': sSource,
                                'data': aoData,
                                'success': fnCallback
                            });
                }
            });

            jQuery('#list_pengunjung_wrapper .dataTables_filter input').addClass("form-control input-medium input-inline"); // modify table search input
            jQuery('#list_pengunjung_wrapper .dataTables_length select').addClass("form-control input-xsmall"); // modify table per page dropdown
            jQuery('#list_pengunjung_wrapper .dataTables_length select').select2(); // initialize select2 dropdown

        }

    };

}();
