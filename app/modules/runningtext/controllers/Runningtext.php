<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Runningtext extends MX_Controller {

    
    public function __construct() {
        parent::__construct();
        if (!$this->autentifikasi->sudah_login())
            redirect('adminpage/site-login.asp','refresh');
        $this->load->model('m_runtext');
        $this->load->model('berita/m_berita');

    }

    public function index() {
      $idincrement = auto_inc('m_runtext','id_run_text');
      if ($this->input->post()) {
        $data = array(
          'id_run_text'=> $idincrement,
          'run_text'=> $this->input->post('run_text'),
        );
            $this->m_runtext->insert($data);
            redirect('adminweb/runningtext.asp');
      }
    	  $data['list'] = $this->m_runtext->get_all();
        $data['module'] = "runningtext";
        $data['view_file'] = "list";
        echo Modules::run('template/render_master',$data);
    }
    public function edit(){
      if ($this->input->post()) {
        $data = array(
          'run_text'=> $this->input->post('run_text'),
        );
        if ($this->m_runtext->update($this->input->post('id_run_text'),$data)) {
            redirect('adminweb/runningtext.asp');
        }
      }else{
        redirect('adminweb/runningtext.asp');
      }
    }
    public function hapus($id){
        $this->m_runtext->delete($id);
        redirect('adminweb/runningtext.asp');
    }
    public function search(){
  // memproses hasil pengetikan keyword pada form
      $keyword = $this->input->post('term');
      $data['response'] = 'false'; //mengatur default response
      $query = $this->m_berita->get_judul($keyword); //memanggil fungsi pencarian pada model

      if (!empty($query)) {
          $data['response'] = 'true'; //mengatur response
          $data['message'] = array(); //membuat array
          foreach ($query as $row) {
              $data['message'][] = array('label' => $row->judul, 'value' => $row->judul); //mengisi array dengan record yang diperoleh
          }
      }
      //konstanta IS_AJAX
      if (IS_AJAX) {
          echo json_encode($data); //mencetak json jika merupakan permintaan ajax
      } else {
          $data['message'][] = array('label' => $keyword, 'value' => $keyword);
          echo json_encode($data);
      }
  }
}
