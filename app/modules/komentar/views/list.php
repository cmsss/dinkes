<section class="content-header">
    <h1>
        Komentar
    </h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div>
              <!-- Nav tabs -->
              <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Coment All</a></li>
                <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Req Coment <?php if ($jumlah_coment == '0') {
                   echo " ";
                }else{
                    echo '<span class="label label-danger">'.$jumlah_coment;
                    } ?></a></li>
                
              </ul>

              <!-- Tab panes -->
              <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="home">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Coment All</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <div class="table-responsive">
                                <table class="table table-hover table-condensed" id="list_kategori">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Judul Artikel</th>
                                            <th>Isi Komentar</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $no = 1;
                                        if (!empty($komentar_all)) {
                                            foreach ($komentar_all as $lk) {
                                       
                                       
                                            ?>
                                            <tr>
                                                <td><?php echo $no++; ?></td>
                                                <td><?php echo $lk->nama; ?></td>
                                                <td><?php echo $lk->judul; ?></td>
                                                <td><?php echo $lk->komentar; ?></td>
                                                <td>
                                                    <a onclick="return confirm('Anda yakin ingin Menghapus Komentar ini....?')"  href="<?php echo site_url('admin/komentar/delete/' . $lk->id_komentar); ?>" class="btn btn-warning"><i class=" fa fa-trash"></i></a>
                                                </td>
                                                
                                            </tr>
                                            <?php
                                            
                                        }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="profile">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">ReQ Coment</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <div class="table-responsive">
                                <table class="table table-hover table-condensed" id="list_kategori">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Judul Artikel</th>
                                            <th>Isi Komentar</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $no = 1;
                                        if (!empty($komentar)) {
                                            foreach ($komentar as $lk) {
                                       
                                       
                                            ?>
                                            <tr>
                                                <td><?php echo $no; ?></td>
                                                <td><?php echo $lk->nama; ?></td>
                                                <td><?php echo $lk->judul; ?></td>
                                                <td><?php echo $lk->komentar; ?></td>
                                                <td>
                                                    <?php echo form_open(); ?>
                                                    <?php echo form_hidden('id_komentar', $lk->id_komentar); ?>
                                                    <button type="submit" class="btn btn-primary" onclick="return confirm('Apakah anda yakin akan menampilkan komentar ini?')">Tampilkan</button>

                                                    <?php echo form_close(); ?>
                                                </td>
                                            </tr>
                                            <?php
                                            $no++;
                                        }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                
              </div>

            </div>
            
        </div>
    </div>
</section>
