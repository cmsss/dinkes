<div class="content">
    <p>Produk Hukum </p>
</div>
<div class="isi">
    <?php 
    foreach ($produk_hukum as $b) {
        ?>
        <div class="media">
            <h4 class="media-heading">
                <div class="pull-left">
                    <b><?php echo $b->produk_hukum; ?></b><br/>
                    <small><?php echo $b->deskripsi; ?></small>
                </div>
                <div class="pull-right">
                    <a href="<?php echo site_url('uploads/'.$b->file) ?>" title="<?php echo $b->file; ?>"><small><span class="glyphicon glyphicon-download"></span> Download File</small></a>
                </div>
            </h4> 
        </div>
    <hr/>
    <?php } ?>
    <?php
    ?>
</div>