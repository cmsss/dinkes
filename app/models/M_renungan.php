<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_renungan extends MY_Model {

    public function __construct() {
        parent::__construct();
        parent::set_table('renungan', 'id');
    }


    public function get_page($limit, $start) {
        $this->db->limit($limit, $start);
        return $this->get_all_v_k();
    }

    public function get_all_v_k()
    {
        $this->db->where(array('tampil' => '1'));
    	return parent::get_all();
    }


    public function get_page_v($limit) {
        $this->db->limit($limit);
        return $this->get_all_v_k();
    }


    
    

}
