<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>asset/plugin/maskmoney/jquery.maskMoney.js" type="text/javascript"></script>
<div class="desc_calczakat">
	<h4><strong>Zakat Pertanian, Perkebunan, dan Kehutanan</strong></h4>
	<blockquote> zakat yang dikenakan atas hasil pertanian, perkebunan, dan hasil hutan pada saat panen. Nisab zakat pertanian, perkebunan, dan kehutanan senilai <strong>653 kg gabah</strong>. Kadar zakat pertanian, perkebunan, dan kehutanan sebesar <strong>10% jika tadah hujan</strong> <strong>atau 5% jika menggunakan irigasi dan perawatan lainnya</strong>.</blockquote><br>
</div>
<div class="table-responsive">
	<table class="table table-striped table-bordered table-hover">
		<tbody>
		<tr>
			<th colspan="2" class="header"><strong>Hasil pertanian, perkebunan, dan kehutanan</strong></th>
		</tr>
		<tr>
			<th class="label_zakat">Hasil Panen</th>
			<th class="value_zakat"><input onkeyup="Panen(this);" type="text" id="panen" class="input_angka" style="width:100px" > kg</th>
		</tr>
		<tr>
			<th class="label_zakat">Jenis Pengairan</th>
			<th class="value_zakat">
				<select id="persen_zakat"  class="form-control" onchange="persenZakat(this);" style="width: 300px;">
					<option value="0.1">tanpa biaya</option>
					<option value="0.05">dengan biaya</option>
				</select>
			</th>
		</tr>

		<tr>
			<th colspan="2" class="header"><strong>Zakat</strong></th>
		</tr>
		<tr class="total_zakat">
			<th class="label_zakat"><strong>Zakat yang dikeluarkan</strong></th>
			<th class="value_zakat"><input type="text" id="zakatpertanian" class="input_angka" style="width:100px" disabled="disabled"> kg</th>
		</tr>

		<tr>
			<th colspan="2" class="header" id="keterangan">&nbsp;</th>
		</tr>
	</tbody>
</table>
</div>



<script>

	function Panen(data){
		var data = parseFloat(data.value);
		if (data >= 653) {
			var persen_zakat =  $('#persen_zakat').val();
			$("#zakatpertanian").val(data * persen_zakat);
		}else{
			$("#zakatpertanian").val(0);
		}
	}

	function persenZakat(data){
		var data = parseFloat(data.value);
		// console.log(data);
		var panen = parseFloat($("#panen").val());
		if (panen >= 653) {
			$("#zakatpertanian").val(panen * data );
		}else{
			$("#zakatpertanian").val(0);
		}
	}

	$(function() {
		$(".harga").maskMoney({ allowNegative: true, thousands:',', affixesStay: false});
	})
</script>