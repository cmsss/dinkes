<!-- ##### Partner Area Start ##### -->
<div class="partner-area section-padding-0-100">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="partners-logo d-flex align-items-center justify-content-between flex-wrap wow fadeInUp" data-wow-delay="300ms">
                    <?php if (!empty($banner2)): ?>
                        <?php foreach ($banner2 as $b1): ?>
                            <?php 
                                if ($b1->grup == '2') {
                                    $link = "#";
                                }else {
                                    $link = $b1->url;
                                    
                                }
                             ?>
                            <img style="max-width: 200px;" src="<?= base_url('files/gorontalo/file/banner/'.$b1->filegambar) ?>" alt="benner">
                        <?php endforeach ?>
                    <?php endif ?>
                    
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ##### Partner Area End ##### -->