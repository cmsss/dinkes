  <section class="content-header">
    <h1></i>Galery</h1>
  </section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
            <h3 class="box-title">List Galery</h3>
            <div class="pull-right">
                <a href="<?php echo site_url('adminweb/galery/post.asp'); ?>" class="btn btn-info">Tambah</a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body">
			<?php echo form_open(); ?>

			<table class="table table-hover table-condensed" id="list_kategori">
				<thead>
					<tr>
						<td>No</td>
						<td>Judul</td>
						<td>Deskripsi</td>
						<td>Kategori</td>
						<td>cretae</td>
						<td>File</td>
						<td>Flag</td>
						<td>Album</td>
						<td>aksi</td>
					</tr>
				</thead>
				<tbody>

					<?php
						$no=1;
						foreach ($list as $row) {?>
					<tr>
						<td><?php echo $no++; ?></td>
						<td><?php echo $row->judul; ?></td>
						<td><?php echo $row->deskripsi; ?></td>
						<td><?php echo $row->kategori; ?></td>
						<td><?php echo $row->create_date; ?></td>
						<td>
							<?php if ($row->file!='') { ?>
								<img src="<?php echo base_url('uploads/'.$row->file); ?>" width="100px" height="100px">
							<?php }else{

								} ?>
							
						</td>
						<td><?php echo $row->flag; ?></td>
						<td><?php echo list_album()[$row->album]; ?></td>
						<td>
							<a href="<?php echo site_url('adminweb/galery/update/'.$row->id_galery); ?>" class="btn btn-info"><i class="fa fa-edit"></i></a>
							<a href="<?php echo site_url('adminweb/galery/delete/'.$row->id_galery); ?>" onclick="return confirm('anda yakin untuk menghapus data ini');" class="btn btn-warning"><i class="fa fa-trash"></i></a>
						</td>
					</tr>
					<?php } ?>

				</tbody>
			</table>
			<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
</section>

