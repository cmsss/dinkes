<div class="marginTop">
    <div class="panel-heading">
        <h3 class="panel-title">Berita</h3>
    </div>
    <div class="panel-body">
    	<?php if (!empty($list)) {
    		foreach ($list as $l_berita) {

    			?>

    			<?php 
                    $tgl = explode(' ', $l_berita->tgl_input);
                    $judul = $l_berita->judul;
                    
                    $judul_fix = flag($judul);

                    // waktu ts
                    $date = new DateTime();
                    $tgl_n = $date->setTimestamp($l_berita->ts);
                    $tgl_pecah1 = explode(' ', $date->format('Y-m-d H:i:s'));

                    // waktu ts nama_hari($tgl_pecah1[0]).' '.  tgl_indo($tgl_pecah1[0]) .' '.$tgl_pecah1[1]

                 ?>

    				<div class="col-md-12">
			            <div class="marginBottomMedia">
			                <div class="media">
			                    <a class="pull-left" href="<?= site_url('berita/' .$l_berita->id.'/'.$judul_fix) ?>">
			                    <!-- 100x100 -->
			                        <img class="media-object thumbnail img-responsive marginRightMedia" style="width: 200px;" src="<?= base_url('files/gorontalo/file/fotoberita/' .$l_berita->id.'.jpg') ?>" onerror="this.onerror=null;this.src='<?= base_url('uploads/kemenag/200x200.png') ?>';"  alt="...">
			                    </a>
			                    <div class="media-body">
                                    <a  href="<?= site_url('berita/' .$l_berita->id.'/'.$judul_fix) ?>">
        		                        <h4 class="media-heading"><?= $l_berita->judul ?></h4>
                                    </a>
			                        <p>
    			                        <ul class="blog-meta">
    			                            <!-- <li>By: <a href="http://djavaui.com/" target="_blank">Djava UI</a></li> -->
    			                            <li><?php echo nama_hari($tgl_pecah1[0]).' '.  tgl_indo($tgl_pecah1[0]) .' '.$tgl_pecah1[1] ?></li>
    			                            <li><a href=""><?= $l_berita->counters ?>x dibaca</a></li>
    			                        </ul>
			                        </p>

			                    </div>
			                </div>
			                
		                    
			            </div>
			        </div>

    			<?php
    		}
    	} ?>
    	<?php echo $page; ?>
        
    </div>
</div>