<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Berita extends MX_Controller {

    private $level;
    private $id_login;

    public function __construct() {
        parent::__construct();
        if (!$this->autentifikasi->sudah_login())
            redirect('adminpage/site-login.asp','refresh');

        $this->level = $this->session->userdata('level');
        $this->id_login = $this->session->userdata('id_login');

        $this->load->model('m_berita');
        $this->load->model('kategori/m_kategori', 'kategori');
        
    }

    public function index() {
        switch ($this->level) {
            case '1':
                $data['l_kategori']=list_kategori();
                $data['list'] = query_sql("select * from v_berita order by id_berita desc");
                $data['view_file'] = "list";
                break;
            
            default:
                $data['l_kategori']=list_kategori();
                $data['list'] = query_sql("SELECT * FROM v_berita WHERE id_user = '$this->id_login'  order by id_berita desc");
                $data['view_file'] = "list_bukan_admin";
                break;
        }
        
        $data['module'] = "berita";
        echo Modules::run('template/render_master',$data);
    }
    public function add(){
        if ($this->input->post()) {
                switch ($this->level) {
                    case '1':
                        $config['upload_path']="./image/berita/";
                        $config['allowed_types']="jpg|png";
                        $config['max_size']="1024";
                        $config['max_width']="2000";
                        $config['max_height']="1000";
                        $config['encrypt_name']= TRUE;
                        $this->upload->initialize($config);
                        if ($this->input->post('tgl_post') ==''){
                            $tanggal = date('Y-m-d H:i:s');
                        }else{
                            $pecahtgl = explode('/', $this->input->post('tgl_post'));
                            $tanggal = $pecahtgl['2'].'-'.$pecahtgl['0'].'-'.$pecahtgl['1'].' '.date('H:i:s');
                        }
                        $idincrement = auto_inc('m_berita','id_berita');
                        if ($this->upload->do_upload('img')) {
                            $data= array(
                                'id_berita'=> $idincrement,
                                'judul'=>$this->input->post('judul'),
                                'img'=>$this->upload->file_name,
                                'isi'=>$this->input->post('isi'),
                                'kategori'=>$this->input->post('kategori'),
                                'create_date'=>$tanggal,
                                'flag'=>url_title($this->input->post('judul'),'dash',TRUE).'.html',
                                'headline'=>$this->input->post('headline'),
                                'status'=>$this->input->post('status'),
                                'penulis' => $this->session->userdata('id_login')
                                );
                            if (!$this->m_berita->insert($data)){
                                echo "<script>alert('Berita berhasil disimpan');
                        window.location=('" . site_url('adminweb/berita.asp') . "');</script>";
                            }else{
                                echo "<script>alert('Gagal simpan berita');
                        window.location=('" . site_url('adminweb/berita.asp') . "');</script>";
                            }

                        }else {
                            $data= array(
                                'id_berita'=> $idincrement,
                                'judul'=>$this->input->post('judul'),
                                // 'img'=>$this->upload->file_name,
                                'isi'=>$this->input->post('isi'),
                                'kategori'=>$this->input->post('kategori'),
                                'create_date'=>$tanggal,
                                'flag'=>url_title($this->input->post('judul'),'dash',TRUE).'.html',
                                'headline'=>$this->input->post('headline'),
                                'status'=>$this->input->post('status'),
                                'penulis' => $this->session->userdata('id_login')
                                );
                            if (!$this->m_berita->insert($data)){
                                echo "<script>alert('Berita berhasil disimpan');
                        window.location=('" . site_url('adminweb/berita.asp') . "');</script>";
                            }else{
                                echo "<script>alert('Gagal simpan berita');
                        window.location=('" . site_url('adminweb/berita.asp') . "');</script>";
                            }
                        }
                        break;
                    
                    default:
                        $config['upload_path']="./image/berita/";
                        $config['allowed_types']="jpg|png";
                        $config['max_size']="1024";
                        $config['max_width']="2000";
                        $config['max_height']="1000";
                        $config['encrypt_name']= TRUE;
                        $this->upload->initialize($config);
                        if ($this->input->post('tgl_post') ==''){
                            $tanggal = date('Y-m-d H:i:s');
                        }else{
                            $pecahtgl = explode('/', $this->input->post('tgl_post'));
                            $tanggal = $pecahtgl['2'].'-'.$pecahtgl['0'].'-'.$pecahtgl['1'].' '.date('H:i:s');
                        }
                        $idincrement = auto_inc('m_berita','id_berita');
                        if ($this->upload->do_upload('img')) {
                            $data= array(
                                'id_berita'=> $idincrement,
                                'judul'=>$this->input->post('judul'),
                                'img'=>$this->upload->file_name,
                                'isi'=>$this->input->post('isi'),
                                'kategori'=>$this->input->post('kategori'),
                                'create_date'=>$tanggal,
                                'flag'=>url_title($this->input->post('judul'),'dash',TRUE).'.html',
                                'headline'=>$this->input->post('headline'),
                                'status'=>'0',
                                'penulis' => $this->session->userdata('id_login')
                                );
                            if (!$this->m_berita->insert($data)){
                                echo "<script>alert('Berita berhasil disimpan');
                        window.location=('" . site_url('adminweb/berita.asp') . "');</script>";
                            }else{
                                echo "<script>alert('Gagal simpan berita');
                        window.location=('" . site_url('adminweb/berita.asp') . "');</script>";
                            }

                        }else {
                            $data= array(
                                'id_berita'=> $idincrement,
                                'judul'=>$this->input->post('judul'),
                                // 'img'=>$this->upload->file_name,
                                'isi'=>$this->input->post('isi'),
                                'kategori'=>$this->input->post('kategori'),
                                'create_date'=>$tanggal,
                                'flag'=>url_title($this->input->post('judul'),'dash',TRUE).'.html',
                                'headline'=>$this->input->post('headline'),
                                'status'=>$this->input->post('status'),
                                'penulis' => $this->session->userdata('id_login')
                                );
                            if (!$this->m_berita->insert($data)){
                                echo "<script>alert('Berita berhasil disimpan');
                        window.location=('" . site_url('adminweb/berita.asp') . "');</script>";
                            }else{
                                echo "<script>alert('Gagal simpan berita');
                        window.location=('" . site_url('adminweb/berita.asp') . "');</script>";
                            }
                        }
                        break;
                }
                
            }else{
                switch ($this->level) {
                    case '1':
                        $data['kategori']=list_kategori();
                        $data['view_file']="add_berita";
                        break;
                    
                    default:
                        $data['kategori']=list_kategori();
                        $data['view_file']="add_berita_bukan_admin";
                        break;
                }


                
                $data['module']="berita";
                echo Modules::run('template/render_master',$data);
            }
        
    }
    public function edit($id){
        if ($cek = $this->m_berita->get_by(array('id_berita'=>$id))){
            $nama_file = "file_". time();
            $config['upload_path']="./image/berita/";
            $config['allowed_types']="jpg|png";
            $config['max_size']="1024";
            $config['max_width']="2000";
            $config['max_height']="1000";
            $config['encrypt_name']= TRUE;
            $this->upload->initialize($config);
            if ($this->input->post()) {

                switch ($this->level) {
                    case '1':
                        if ($this->input->post('tgl_post') ==''){
                            $tanggal = date('Y-m-d H:i:s');
                        }else{
                            $pecahtgl = explode('/', $this->input->post('tgl_post'));
                            $tanggal = $pecahtgl['2'].'-'.$pecahtgl['0'].'-'.$pecahtgl['1'].' '.date('H:i:s');
                        }
                        if ($this->upload->do_upload('img')) {
                            $pathgambar = realpath(APPPATH . '../image/berita');
                            if ($cek->img != '') {
                                unlink($pathgambar . '/' . $cek->img);
                            }
                               $img = $this->upload->data();
                                $data= array(
                                    'judul'=>$this->input->post('judul'),
                                    'isi'=>$this->input->post('isi'),
                                    'img'=>$this->upload->file_name,
                                    'kategori'=>$this->input->post('kategori'),
                                    'create_date' => $tanggal,
                                    'update_date'=>date('Y-m-d H:i:s'),
                                    'flag'=>url_title($this->input->post('judul'),'dash',TRUE).'.html',
                                    'headline' => $this->input->post('headline', TRUE),
                                    //'album_foto'=>$this->input->post('album_foto'),
                                    'status'=>$this->input->post('status'),
                                    );
                                $this->m_berita->update($id,$data);
                                redirect('adminweb/berita.asp','refresh');
                            }else {
                             $data= array(
                                    'judul'=>$this->input->post('judul'),
                                    'isi'=>$this->input->post('isi'),
                                    'kategori'=>$this->input->post('kategori'),
                                    'create_date' => $tanggal,
                                    'update_date'=>date('Y-m-d H:i:s'),
                                    'flag'=>url_title($this->input->post('judul'),'dash',TRUE).'.html',
                                    'headline' => $this->input->post('headline', TRUE),
                                    //'album_foto'=>$this->input->post('album_foto'),
                                    'status'=>$this->input->post('status'),
                                    );
                                $this->m_berita->update($id,$data);
                                redirect('adminweb/berita.asp','refresh');
                        }
                        break;
                    
                    default:
                        if ($this->input->post('tgl_post') ==''){
                            $tanggal = date('Y-m-d H:i:s');
                        }else{
                            $pecahtgl = explode('/', $this->input->post('tgl_post'));
                            $tanggal = $pecahtgl['2'].'-'.$pecahtgl['0'].'-'.$pecahtgl['1'].' '.date('H:i:s');
                        }
                        if ($this->upload->do_upload('img')) {
                            $pathgambar = realpath(APPPATH . '../image/berita');
                            if ($cek->img != '') {
                                unlink($pathgambar . '/' . $cek->img);
                            }
                               $img = $this->upload->data();
                                $data= array(
                                    'judul'=>$this->input->post('judul'),
                                    'isi'=>$this->input->post('isi'),
                                    'img'=>$this->upload->file_name,
                                    'kategori'=>$this->input->post('kategori'),
                                    'create_date' => $tanggal,
                                    'update_date'=>date('Y-m-d H:i:s'),
                                    'flag'=>url_title($this->input->post('judul'),'dash',TRUE).'.html',
                                    'headline' => $this->input->post('headline', TRUE),
                                    //'album_foto'=>$this->input->post('album_foto'),
                                    );
                                $this->m_berita->update($id,$data);
                                redirect('adminweb/berita.asp','refresh');
                            }else {
                             $data= array(
                                    'judul'=>$this->input->post('judul'),
                                    'isi'=>$this->input->post('isi'),
                                    'kategori'=>$this->input->post('kategori'),
                                    'create_date' => $tanggal,
                                    'update_date'=>date('Y-m-d H:i:s'),
                                    'flag'=>url_title($this->input->post('judul'),'dash',TRUE).'.html',
                                    'headline' => $this->input->post('headline', TRUE),
                                    //'album_foto'=>$this->input->post('album_foto'),
                                    );
                                $this->m_berita->update($id,$data);
                                redirect('adminweb/berita.asp','refresh');
                        }
                        break;
                }


                   
            }else{
                switch ($this->level) {
                    case '1':
                        $data['kategori']=list_kategori();
                        $data['view_file']="edit_berita";
                        break;
                    
                    default:
                        if ($cek_data = query_sql("SELECT * FROM v_berita WHERE id_user = '$this->id_login' AND id_berita = '$cek->id_berita'  order by id_berita desc" , "row")) {
                            $data['kategori']=list_kategori();
                            $data['view_file']="edit_berita_bukan_admin";
                        }else{
                            redirect('adminweb/berita.asp');
                        }
                        
                        break;
                }
                $edit = $this->m_berita->get_by(array('id_berita'=>$id));
                $pecah = explode(' ', $edit->create_date);
                $tanggal = explode('-', $pecah['0']);
                $data['tgl'] = $tanggal['1'].'/'.$tanggal['2'].'/'.$tanggal['0'];
                $data['edit']= $edit;
                
                $data['module']="berita";
                echo Modules::run('template/render_master',$data);
            }
        }else{
            redirect('adminweb/berita.asp' ,'refresh');
        }
        
    }
    public function hapus($id){
        if ($cek = $this->m_berita->get_by(array('id_berita'=>$id))){
            if ($this->session->userdata('level') == '1'){
                $pathgambar = realpath(APPPATH . '../image/berita');
                if ($cek->img != '') {
                    unlink($pathgambar . '/' . $cek->img);
                }
                $this->m_berita->delete($id);
                redirect('adminweb/berita.asp');
            }
            if ($this->session->userdata('id_login') == $cek->penulis){
                 $pathgambar = realpath(APPPATH . '../image/berita');
                if ($cek->img != '') {
                    unlink($pathgambar . '/' . $cek->img);
                }
                $this->m_berita->delete($id);
                redirect('adminweb/berita.asp');
            }
                echo "<script>alert('Maaf anda tidak memiliki hak untuk menghapus berita ini !!!' );
            window.location=('" . site_url('adminweb/berita.asp') . "');</script>";
            }else{
                redirect('adminweb/berita.asp');
            }
        
    }
}

