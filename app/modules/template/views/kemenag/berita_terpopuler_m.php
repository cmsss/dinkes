<div class="cont-mobile">

                        <!-- ==============BERITA POPULER=============== -->
    <div class="shadow box-1">
        <div class="title-box">
            <span class="fl">Berita Terpopuler</span>
        </div>
        <div class="list-3">
            <?php

                if (!empty($berita_pp)) {
                    $nomor = 1;
                    foreach ($berita_pp as $b_p) { ?>
                        <a href="<?= site_url('berita-kemenag/' . $b_p->flag) ?>">
                            <div class="no">
                                <div class="no2"><?= $nomor++ ?></div>
                                <div class="ratio1_1 box_img">
                                    <div class="img_con lqd"> <img src="<?= base_url('image/berita/'.$b_p->img) ?>" onError="this.onerror=null;this.src='<?= base_url() ?>asset/kemenag/assets/themes/gorontalo/img/img-default.jpg';"/> </div>
                                </div>
                            </div>
                            <h2><span>Kakankemenag Pohuwato Jalin Keakraban dengan Bawahannya </span></h2>
                            <div class="clearfix"></div>
                        </a>
            <?php   }
                }
            ?>
        </div>
    </div>
    <!-- ==============END BERITA POPULER=============== -->
</div>