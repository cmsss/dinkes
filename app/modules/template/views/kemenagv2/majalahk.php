<div class="panel panel-tab panel-tab-double shadow wow zoomIn">
    <!-- Start tabs heading -->
    <div class="panel-heading no-padding">
        <ul class="nav nav-tabs">
           
            <li class="active nav-border nav-border-top-info">
                <a href="#tab6-211" data-toggle="tab" class="text-center">
                    <span>Majalah</span>
                </a>
            </li>
            
           
            
            
            
            
        </ul>
    </div><!-- /.panel-heading -->
    <!--/ End tabs heading -->

    <!-- Start tabs content -->
    <div class="panel-body">
        <div class="tab-content">
            
            <div class="tab-pane fade in active" id="tab6-211">
                <div class="panel rounded shadow ">
                    
                    <div class="panel-body text-center">
                        <?php 

                            if (!empty($majalah1)) {
                                foreach ($majalah1 as $l_majalah) {
                                    ?>

                                         <?php 
                                            // $tgl = explode(' ', $l_majalah->tgl_input);

                                            $judulo = $l_majalah->judul;
                                           
                                            $judul_fixo = flag($judulo);

                                         ?>

                                        <div class="col-sm-12" style="margin-bottom: -100px;">
                                            <div  class="cbp cbp-caption-active cbp-caption-revealBottom cbp-ready">
                                                <div class="cbp-item art">
                                                    <a target="_blank" href="<?= base_url($l_majalah->pdf) ?>" class="cbp-caption">
                                                        <div class="cbp-caption-defaultWrap">
                                                            <!-- 340x210 -->
                                                            <div class=" fileinput-preview fileinput-exists" style=" max-height: 250px; line-height: 10px;">
                                                                <img src="<?= base_url($l_majalah->cover) ?>" class="img-responsive"  alt="..." onerror="this.onerror=null;this.src='<?= base_url('uploads/kemenag/kemenag.jpg') ?>';">
                                                            </div>
                                                        </div>
                                                        <div class="cbp-caption-activeWrap">
                                                            <div class="cbp-l-caption-alignCenter">
                                                                <div class="cbp-l-caption-body">
                                                                    <div class="cbp-l-caption-text">Lihat Majalah</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </a>
                                                   
                                                    
                                                </div>
                                            </div>
                                        </div>


                                    <?php
                                }
                                ?>
                                <div class="pull-right">
                                    <a href="<?= site_url('majalah-gorontalo') ?>" class="btn btn-primary">Selengkapnya</a>
                                </div>
                                <?php
                            }else {
                                echo "Belum Ada data Majalah";
                            }

                         ?>
                    </div><!-- /.panel-body -->
                </div>
            </div>
           
           

            <div class="tab-pane fade" id="tab6-431">
                <div class="panel rounded shadow ">
                    
                    <div class="panel-body no-padding">
                        <div class="list-group no-margin">
                            <?php 

                                if (!empty($opini1)) {
                                    foreach ($opini1 as $op1 ) {

                                        $tgl = explode(' ', $op1->tgl_input);

                                        $judulo = $op1->judul;
                                        $judul_sterliro = str_replace(array('.' ,',' ,'(' , ')' ,'{' ,'}','?' ,'!' , '=' , '/'), '', $judulo);
                                        $judul_sterliro2 = str_replace(' ', '-', $judul_sterliro);
                                        $judul_fixo = strtolower($judul_sterliro2);
                                        ?>
                                            <a href="<?= site_url('opini/'.$op1->id. '/' .$judul_fixo) ?>" class="list-group-item">
                                                <?= $op1->judul ?>
                                                
                                            </a>

                                        <?php
                                    }
                                }

                             ?>
                            
                        </div>
                        <div class="pull-right">
                            <a class="btn btn-primary" href="<?= site_url('opini-gorontalo') ?>">Selengkapnya</a>
                        </div>
                    </div>
                </div>
            </div>
             
            
        </div>
    </div><!-- /.panel-body -->
    <!--/ End tabs content -->
</div><!-- /.panel -->








