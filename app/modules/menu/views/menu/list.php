<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		<?php echo $judul1?>
	</h1>
	
</section>

<!-- Main content -->

<section class="content">

	<div class="row">
		<!-- Left col -->
        <div class="col-sm-12">
            <div class=" alert alert-success">
                <ul>
                    <li>Berita = <?php echo site_url() ?>berita-gorontalo</li>
                    <li>Artikel = <?php echo site_url() ?>artikel-gorontalo</li>
                    <li>Foto = <?php echo site_url() ?>foto-gorontalo</li>
                    <li>Video = <?php echo site_url() ?>video-gorontalo</li>
                    <li>Audio = <?php echo site_url() ?>audio-gorontalo</li>
                </ul>
            </div>
        </div>
		<div class="col-lg-4">
			<!-- TO DO List -->
			<div class="box box-primary">
				<div class="box-header">
					<i class="ion ion-clipboard"></i>
					<h3 class="box-title"><?php echo $judul2;?></h3>
					<button type="button" class="btn btn-sm btn-primary pull-right" onclick="viewTambah()" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-fw fa-plus"></i> <?php echo $judul1; ?></button>
					

				</div><!-- /.box-header -->
				<div class="box-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover table-condensed table-bordered">
							<thead>
								<tr>
									<th style="width: 5%;">No</th>
									<th>Nama</th>
									<!-- <th>Url</th> -->
									<!-- <th width="1%">Status</th> -->
									<th width="30%">#</th>
								</tr>
							</thead>
							<tbody>
								<?php $no=1;  if (!empty($list)): ?>
									<?php foreach ($list as $l): ?>
										<?php 
										  $url = $l->url;
										  $pecah = explode('punyaku/', $url);
										  if (!empty($pecah[1])) {
										      $urlGet = base_url().$pecah[1];
										  }else{
										      $urlGet = $url;
										  }

										?>
										<tr>
											<td><?php echo $no++ ?></td>
											<td><?php echo $l->nama ?></td>
											<!-- <td><?php echo $urlGet ?></td> -->
											<!-- <td align="center">
												<?php 
													// if ($l->status == '1'){
													// 	$chek = "checked";
													// }else{
													// 	$chek = "";
													// }
												 ?>
												<div class="form-group">
								                      <input  id="chek<?php //echo $l->id?>" onchange="cekSatatus('<?php //echo $l->id?>')" type="checkbox"  <?php echo $chek;?> />
								                    								                    
								                  </div>
											</td> -->
											<td align="center">
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-xs btn-success" onclick="viewListSub2('<?php echo $l->id ?>')"><i class="fa fa-bars"></i></button>
                                                    <button type="button" class="btn btn-xs btn-primary" onclick="viewTambahSub2('<?php echo $l->id ?>')" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-plus"></i></button>
    												<button type="button" class="btn btn-xs btn-warning" onclick="viewUpdate('<?php echo $l->id ?>')" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-pencil"></i></button>
    												<button type="button" class="btn btn-xs btn-danger" onclick="haspusData('<?php echo $l->id; ?>');" ><i class="fa fa-trash"></i></button>
                                                </div>
											</td>
										</tr>
									<?php endforeach ?>
								<?php endif ?>
							</tbody>
						</table>
					</div>
				</div><!-- /.box-body -->
				<div class="box-footer clearfix no-border">

				</div>
			</div><!-- /.box -->



		</div><!-- /.Left col -->
		<div class="col-sm-4">
            <div id="subMenu2">
            </div>
        </div>
        <div class="col-sm-4">
            <div id="subMenu3">
            </div>
        </div>

	</div><!-- /.row (main row) -->

</section><!-- /.content -->
<div class="modal fade" id="exampleModal"  role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel"><span id="formApa"></span> <?php echo $judul1; ?></h4>
      </div>
       <div id="viewForm">
	      <div class="modal-body">
	        <?php echo form_open('' ,'id="dataData"');?>
		        <div class="form-group">
		          <label for="recipient-name" class="control-label">Kategori</label>
		          <?php echo form_input('kategori','','class="form-control " required id="kategori"');?>
		        </div>
	      </div>
	      <div class="modal-footer">
	          <div id="simpanData">
	            <button type="button"  onclick="simpanData();" class="btn btn-primary">Simpan</button>
	          </div>
	        <?php echo form_close();?>

	      </div>
	    </div>
    </div>
  </div>
</div>

<script>
	 function cekSatatus(datai){
         // console.log(datai);
        var cek = document.getElementById("chek"+datai);
        if (cek.checked) {
            var chek = '1';
        }else{
        	var chek = '0';
        }

        $.ajax({
            url: '<?php echo site_url('cpanel/menu/update-status') ?>',
            type: 'POST',
            dataType: 'html',
            data: {data: datai , status : chek },
            beforeSend : function(){

            },
            success : function(data){
                
                $.toaster({priority: 'success', title: 'Success', message: 'Berhasil Update Data'});
            },
            error : function (){
                $.toaster({priority: 'error', title: 'Error', message: 'Gagal Update Data'});
            }
        });


    }
	function viewTambah(){
        $.ajax({
            url: '<?php echo site_url('cpanel/menu/vtambah')?>',
            type: 'GET',
            dataType: 'html',
            beforeSend : function (){
                var html = '<div class="overlay">'+
                    '<i class="fa fa-refresh fa-spin"></i>'+
                    '</div>'
                $("#formApa").text('Tambah');
                $("#viewForm").html(html);
            },
            success  : function (get){
                $("#viewForm").html(get);
            },
            error : function(){
                console.log('error');
            }
        });     
        
    }

	function simpanData(){
        var data = $('#dataData').serialize();
        if ($("#nama").val() == '') {
            $.toaster({priority: 'info', title: 'Perhatian!', message: 'Text Tidak Boleh Kosong'});
            $('#nama').focus();
        } 

        else if ($("#url").val() == '') {
            $.toaster({priority: 'info', title: 'Perhatian!', message: 'Url Tidak Boleh Kosong'});
            $('#url').focus();
        }
         else {

            $.ajax({
                type: 'POST',
                url: "<?php echo site_url('cpanel/menu/simpan') ?>",
                data: data,
                dataType: 'html',
                beforeSend: function() {
                    // setting a timeout
                    $("#simpanData").html('Loading ...');
                },
                success: function (status) {
                   
                    $.toaster({priority: 'success', title: 'Success', message: 'Berhasil Simpan data'});
                    $(".modal").modal('hide');
                    $('.modal-backdrop').hide();
                    // $("#lsitBaru").html(status);
                    $("#medsos").val('');
                    $("#simpanData").html('<button type="button"  onclick="simpanData();" class="btn btn-primary">Simpan</button>');
                    window.location = "<?php echo site_url('adminweb/menu.asp') ?>";

                },
                error: function () {
                    $.toaster({priority: 'error', title: 'Error', message: 'Gagal Tambah Data'});
                    $("#simpanData").html('<button type="button"  onclick="simpanData();" class="btn btn-primary">Simpan</button>');
                }

            });
        }
    }

    function viewUpdate(datai){
    	$.ajax({
    		url: '<?php echo site_url('cpanel/menu/vupdate')?>',
    		type: 'GET',
    		dataType: 'html',
    		data: {data: datai},
    		beforeSend : function (){
    			var html = '<div class="overlay">'+
                    '<i class="fa fa-refresh fa-spin"></i>'+
                    '</div>'

    			$("#viewForm").html(html);
    			$("#formApa").text('Edit');
    		},
    		success  : function (get){
    			$("#viewForm").html(get);
    		},
    		error : function(){
    			console.log('error');
    		}
    	});    	
    	
    }

    function simpanDataUpdate(datai){
        var datai = datai;
        var data = $('#dataData'+datai).serialize();
        data += '&id=' +datai;
        // console.log(data);

        // alert(datai);
        if ($("#nama"+datai).val() == '') {
            $.toaster({priority: 'info', title: 'Perhatian!', message: 'Text Tidak Boleh Kosong'});
            $('#nama'+datai).focus();
        } 
        else if ($("#url"+datai).val() == '') {
            $.toaster({priority: 'info', title: 'Perhatian!', message: 'Url Tidak Boleh Kosong'});
            $('#url'+datai).focus();
        } 
         else {

            $.ajax({
                type: 'POST',
                url: "<?php echo site_url('cpanel/menu/update') ?>",
                data: data,
                dataType: 'html',
                beforeSend: function() {
                    // setting a timeout
                    $("#simpanData"+datai).html('Loading ...');
                },
                success: function (status) {
                     
                    $.toaster({priority: 'success', title: 'Success', message: 'Berhasil update Data'});
                    $(".modal").modal('hide');
                    $('.modal-backdrop').hide();
                    // $("#lsitBaru").html(status);
                    $("#simpanData"+datai).html('<button type="button"  onclick="simpanDataUpdate('+datai+');" class="btn btn-primary">Simpan</button>');
                    // console.log(status);
                     window.location = "<?php echo site_url('adminweb/menu.asp') ?>";

                },
                error: function () {
                    $.toaster({priority: 'error', title: 'Error', message: 'Gagal Updatte Data'});
                    $("#simpanData"+datai).html('<button type="button"  onclick="simpanDataUpdate('+datai+');" class="btn btn-primary">Simpan</button>');
                }

            });
        }
    }

    function haspusData(datai){
        var datai = datai;

        var reslut = confirm('Anda Yakin Untuk Menghapus Daata Ini');
        if (reslut) {
            
            $.ajax({
                url: '<?php echo site_url('cpanel/menu/hapus') ?>',
                type: 'POST',
                dataType: 'html',
                data: {data: datai},
                beforeSend : function(){

                },
                success : function(data){
                    
                    $.toaster({priority: 'success', title: 'Success', message: 'Berhasil Hapus Data'});
                    window.location = "<?php echo site_url('adminweb/menu.asp') ?>";
                },
                error : function (){
                    $.toaster({priority: 'error', title: 'Error', message: 'Gagal Hapus Data'});
                }
            });
        }       
        
    }
    function viewListSub2(datai){
        $.ajax({
              type: 'GET',
              url: "<?php echo site_url('cpanel/menu/list-sub2') ?>",
              data: {data : datai},
              dataType: 'html',
              beforeSend: function() {
                  // setting a timeout
                  $("#subMenu2").html('Loading ...');
              },
              success: function (status) {
                  $("#subMenu2").html(status);

              },
              error: function () {
                  $.toaster({priority: 'error', title: 'Error', message: 'Gagal'});                  
              }

          });
    }

    function viewTambahSub2(datai){
        $.ajax({
            url: '<?php echo site_url('cpanel/menu/vtambah-sub-2')?>',
            type: 'GET',
            dataType: 'html',
            data: {data: datai},
            beforeSend : function (){
                var html = '<div class="overlay">'+
                    '<i class="fa fa-refresh fa-spin"></i>'+
                    '</div>';

                $("#viewForm").html(html);
                $("#formApa").text('Tambah Sub');
            },
            success  : function (get){
                $("#viewForm").html(get);
            },
            error : function(){
                console.log('error');
            }
        });     
        
    }
</script>
