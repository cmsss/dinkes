<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_galery extends MY_Model {
    
    public function __construct(){
        parent::__construct();
        parent::set_table('t_galery','id_galery');
    }

    public function get_slider(){
        $this->db->limit(5);
        $this->db->where(array('kategori' => 'Foto'));
        return parent::get_all();
    }

    public function get_max_id() {
        $this->db->limit('1');
        $this->db->where("file <> ''");
        $this->db->where(array('kategori'=>'Foto'));
        $data = parent::get_all();
        if (is_array($data)) {
            foreach ($data as $d) {
                $id = $d->id_galery;
            }
        }
        return $id;
    }
    public function get_max_idv() {
        $this->db->limit('1');
        $this->db->where(array('kategori'=>'Video'));
        $data = parent::get_all();
        if (is_array($data)) {
            foreach ($data as $d) {
                $id = $d->id_galery;
            }
        }
        return $id;
    }
    public function get_sliderv(){
        $this->db->limit(1);
        $this->db->where(array('kategori' => 'Video'));
        return parent::get_all();
    }
    
}
?>