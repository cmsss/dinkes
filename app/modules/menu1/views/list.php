<?php

function menu_kemenag(){
    $d = query_sql("SELECT * FROM t_menu WHERE parent_id = '0' ORDER BY id_menu asc" );
    return $d;
}


function sub_menu($id_menu)
{
    $d = query_sql("SELECT * FROM t_menu WHERE parent_id = '$id_menu' ORDER BY id_menu asc");
    return $d;
}



?>


<section class="content-header">
  <h1>
    Menu
    <small>List menu, menambah menu, mengedit menu</small>
  </h1>
</section>
<section class="content">
  <!-- Default box -->
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">List Menu</h3>
      <div class="box-tools pull-right">
        <a href="javascript::0" data-toggle="modal" data-target="#addmenu" class="btn btn-primary"><i class="fa fa-plus"></i> Menu</a>
      </div>
    </div>
    <div class="box-body">
      <div class="table-responsive">
      <!-- id="list_kategori" -->
        <table class="table table-condensed table-bordered table-hover table-striped" >
          <thead>
            <tr>
              <td>No</td>
              <td>Menu</td>
              <td>Url</td>
              <td>Aksi</td>
            </tr>
          </thead>
          <tbody>
          
            <?php 
              $no=1;
              foreach ($list as $row) {?>
            <tr>
              <td><?php echo $no++; ?></td>
              <td><?php echo $row->menu; ?></td>
              <td><?= $row->url; ?></td>
              <td>
                <a href="javascript::0" data-toggle="modal" data-target="#editmenu<?= $row->id_menu ?>" class="btn btn-info">Edit</a>
                <a href="javascript::0" data-toggle="modal" data-target="#tamsub1<?= $row->id_menu ?>" class="btn btn-primary"><i class="fa fa-plus"></i> SUB</a>
                <a href="<?php echo site_url('adminweb/menu/hapus/'.$row->id_menu); ?>" onclick="return confirm('anda yakin untuk menghapus data ini');" class="btn btn-danger">Hapus</a>
              </td>
            </tr>

            <?php

              if (!empty(sub_menu($row->id_menu))) {
                ?>
                
                  <tr>
                    <td colspan="1">
                      
                    </td>
                    <td colspan="3">
                      <div class="box box-solid bg-blue-gradient collapsed-box" style="cursor: move;">
                        <div class="box-header ui-sortable-handle" >
                          <h3 class="box-title">Sub Menu <?= $row->menu ?></h3>
                          <!-- tools box -->
                          <div class="pull-right box-tools">
                            <!-- button with a dropdown -->
                            <button class="btn btn-primary btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                          </div><!-- /. tools -->
                        </div><!-- /.box-header -->
                        
                        <div class="box-footer text-black tab">
                          <table class="table table-condensed table-bordered table-hover table-striped">
                            <thead>
                              <tr>
                                <td>No</td>
                                <td>Menu</td>
                                <td>Url</td>
                                <td>Aksi</td>
                              </tr>
                            </thead>
                            <tbody>
                              <?php 
                                $no1 = 1;
                                foreach (sub_menu($row->id_menu) as $sub1) {
                                  ?>

                                  <tr>
                                    <td><?php echo $no1++; ?></td>
                                    <td><?php echo $sub1->menu; ?></td>
                                    <td><?= $sub1->url; ?></td>
                                    <td>
                                      <a href="javascript::0" data-toggle="modal" data-target="#editmenu<?= $sub1->id_menu ?>" class="btn btn-info">Edit</a>
                                      <a href="javascript::0" data-toggle="modal" data-target="#tamsub1<?= $sub1->id_menu ?>" class="btn btn-primary"><i class="fa fa-plus"></i> SUB</a>
                                      <a href="<?php echo site_url('adminweb/menu/hapus/'.$sub1->id_menu); ?>" onclick="return confirm('anda yakin untuk menghapus data ini');" class="btn btn-danger">Hapus</a>
                                    </td>
                                  </tr>
                                  <?php

                                    if(!empty(sub_menu($sub1->id_menu))){
                                      ?>

                                        <tr>
                                          <td colspan="1"></td>
                                          <td colspan="3">
                                            <div class="box box-solid bg-yellow-gradient collapsed-box" style="cursor: move;">
                                              <div class="box-header ui-sortable-handle" >
                                                <h3 class="box-title">Sub Menu <?= $sub1->menu ?></h3>
                                                <!-- tools box -->
                                                <div class="pull-right box-tools">
                                                  <!-- button with a dropdown -->
                                                  <button class="btn btn-warning btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                                </div><!-- /. tools -->
                                              </div><!-- /.box-header -->
                                              
                                              <div class="box-footer text-black">
                                                <table class="table table-condensed table-bordered table-hover table-striped">
                                                  <thead>
                                                    <tr>
                                                      <td>No</td>
                                                      <td>Menu</td>
                                                      <td>Url</td>
                                                      <td>Aksi</td>
                                                    </tr>
                                                  </thead>
                                                  <tbody>
                                                    <?php
                                                    $no2 = 1;
                                                    foreach (sub_menu($sub1->id_menu) as $sub2) {
                                                      ?>

                                                        <tr>
                                                          <td><?php echo $no2++; ?></td>
                                                          <td><?php echo $sub2->menu; ?></td>
                                                          <td><?= $sub2->url; ?></td>
                                                          <td>
                                                            <a href="javascript::0" data-toggle="modal" data-target="#editmenu<?= $sub2->id_menu ?>" class="btn btn-info">Edit</a>
                                                            <a href="javascript::0" data-toggle="modal" data-target="#tamsub1<?= $sub2->id_menu ?>" class="btn btn-primary"><i class="fa fa-plus"></i> SUB</a>
                                                            <a href="<?php echo site_url('adminweb/menu/hapus/'.$sub2->id_menu); ?>" onclick="return confirm('anda yakin untuk menghapus data ini');" class="btn btn-danger">Hapus</a>
                                                          </td>
                                                        </tr>
                                                        <?php

                                                          if (!empty(sub_menu($sub2->id_menu))) {
                                                            ?>

                                                            <tr>
                                                              <td colspan="1"></td>
                                                              <td colspan="3">
                                                                <div class="box box-solid bg-red-gradient collapsed-box" style="cursor: move;">
                                                                  <div class="box-header ui-sortable-handle" >
                                                                    <h3 class="box-title">Sub Menu <?= $sub2->menu ?></h3>
                                                                    <!-- tools box -->
                                                                    <div class="pull-right box-tools">
                                                                      <!-- button with a dropdown -->
                                                                      <button class="btn btn-danger btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                                                    </div><!-- /. tools -->
                                                                  </div><!-- /.box-header -->
                                                                  
                                                                  <div class="box-footer text-black">
                                                                    <table class="table table-condensed table-bordered table-hover table-striped">
                                                                      <thead>
                                                                        <tr>
                                                                          <td>No</td>
                                                                          <td>Menu</td>
                                                                          <td>Url</td>
                                                                          <td>Aksi</td>
                                                                        </tr>
                                                                      </thead>
                                                                      <tbody>
                                                                        <?php
                                                                          $no3 = 1;
                                                                          foreach (sub_menu($sub2->id_menu) as $sub3) {
                                                                            ?>

                                                                              <tr>
                                                                                <td><?php echo $no3++; ?></td>
                                                                                <td><?php echo $sub3->menu; ?></td>
                                                                                <td><?= $sub3->url; ?></td>
                                                                                <td>
                                                                                  <a href="javascript::0" data-toggle="modal" data-target="#editmenu<?= $sub3->id_menu ?>" class="btn btn-info">Edit</a>
                                                                                  <a href="<?php echo site_url('adminweb/menu/hapus/'.$sub3->id_menu); ?>" onclick="return confirm('anda yakin untuk menghapus data ini');" class="btn btn-danger">Hapus</a>
                                                                                </td>
                                                                              </tr>

                                                                            <?php
                                                                          }

                                                                        ?>
                                                                      </tbody>
                                                                    </table>
                                                                  </div>
                                                                </div>
                                                              </td>
                                                            </tr>


                                                            <!-- edit modal sub 3 -->
                                                            <div class="modal fade" id="editmenu<?= $sub3->id_menu ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                              <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                  <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                    <h4 class="modal-title" id="myModalLabel">Edit Menu</h4>
                                                                  </div>
                                                                  <?= form_open(site_url('adminweb/menu/edit.asp'),'class="form-horizontal"'); ?>
                                                                  <div class="modal-body">
                                                                    <div class="form-group">
                                                                      <label class="col-md-3 control-label">Menu</label>
                                                                      <div class="col-md-9">
                                                                        <?= form_input('menu',$sub3->menu,'class="form-control" placeholder="Nama Menu" required'); ?>
                                                                        <?= form_hidden('id',$sub3->id_menu); ?>
                                                                      </div>
                                                                    </div>
                                                                    <div class="clearfix"></div><br/>
                                                                    <div class="form-group">
                                                                      <label class="col-md-3 control-label">Url</label>
                                                                      <div class="col-md-9">
                                                                        <?= form_input('url',$sub3->url,'class="form-control" placeholder="Url Menu" required'); ?>
                                                                      </div>
                                                                    </div>
                                                                    <div class="clearfix"></div><br/>
                                                                    
                                                                  </div>
                                                                  <div class="modal-footer">
                                                                    <button type="reset" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                                                    <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                                                                  </div>
                                                                  <?= form_close(); ?>
                                                                </div>
                                                              </div>
                                                            </div>
                                                            <!-- edit modal sub 3 -->

                                                            <?php
                                                          }

                                                        ?>



                                                        <!-- tambah Modal sub 2 -->
                                                        <div class="modal fade" id="tamsub1<?= $sub2->id_menu ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                          <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                              <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                <h4 class="modal-title" id="myModalLabel">Tambah Menu Dari Parent <b><?= $sub2->menu ?></b></h4>
                                                              </div>
                                                              <?= form_open(site_url('adminweb/menu/tambah1edit.asp'),'class="form-horizontal"'); ?>
                                                              <div class="modal-body">
                                                                <div class="form-group">
                                                                  <label class="col-md-3 control-label">Menu</label>
                                                                  <div class="col-md-9">
                                                                    <?= form_input('menu','','class="form-control" placeholder="Nama Menu" required'); ?>
                                                                    <?= form_hidden('id',$sub2->id_menu); ?>
                                                                  </div>
                                                                </div>
                                                                <div class="clearfix"></div><br/>
                                                                <div class="form-group">
                                                                  <label class="col-md-3 control-label">Url</label>
                                                                  <div class="col-md-9">
                                                                    <?= form_input('url','','class="form-control" placeholder="Url Menu" required'); ?>
                                                                  </div>
                                                                </div>
                                                                <div class="clearfix"></div><br/>
                                                                <div class="form-group">
                                                                  <label class="col-md-3 control-label">Parent</label>
                                                                  <div class="col-md-9">
                                                                    <?= form_dropdown('parent_id', $parent_id,$sub2->id_menu, 'required class="form-control" readonly disabled'); ?>
                                                                  </div>
                                                                </div>
                                                                <div class="clearfix"></div><br/>
                                                              </div>
                                                              <div class="modal-footer">
                                                                <button type="reset" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                                                <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                                                              </div>
                                                              <?= form_close(); ?>
                                                            </div>
                                                          </div>
                                                        </div>
                                                        <!-- tambah modal sub2 2 -->

                                                        <!-- edit modal sub 2 -->
                                                        <div class="modal fade" id="editmenu<?= $sub2->id_menu ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                          <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                              <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                <h4 class="modal-title" id="myModalLabel">Edit Menu</h4>
                                                              </div>
                                                              <?= form_open(site_url('adminweb/menu/edit.asp'),'class="form-horizontal"'); ?>
                                                              <div class="modal-body">
                                                                <div class="form-group">
                                                                  <label class="col-md-3 control-label">Menu</label>
                                                                  <div class="col-md-9">
                                                                    <?= form_input('menu',$sub2->menu,'class="form-control" placeholder="Nama Menu" required'); ?>
                                                                    <?= form_hidden('id',$sub2->id_menu); ?>
                                                                  </div>
                                                                </div>
                                                                <div class="clearfix"></div><br/>
                                                                <div class="form-group">
                                                                  <label class="col-md-3 control-label">Url</label>
                                                                  <div class="col-md-9">
                                                                    <?= form_input('url',$sub2->url,'class="form-control" placeholder="Url Menu" required'); ?>
                                                                  </div>
                                                                </div>
                                                                <div class="clearfix"></div><br/>
                                                                
                                                              </div>
                                                              <div class="modal-footer">
                                                                <button type="reset" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                                                <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                                                              </div>
                                                              <?= form_close(); ?>
                                                            </div>
                                                          </div>
                                                        </div>
                                                        <!-- edit modal sub 2 -->

                                                      <?php
                                                    }

                                                    ?>
                                                  </tbody>
                                                </table>
                                              </div>
                                            </div>
                                          </td>
                                        </tr>

                                      <?php

                                    }

                                  ?>


                                  <!-- tambah Modal sub 1 -->
                                  <div class="modal fade" id="tamsub1<?= $sub1->id_menu ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                      <div class="modal-content">
                                        <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                          <h4 class="modal-title" id="myModalLabel">Tambah Menu Dari Parent <b><?= $sub1->menu ?></b></h4>
                                        </div>
                                        <?= form_open(site_url('adminweb/menu/tambah1edit.asp'),'class="form-horizontal"'); ?>
                                        <div class="modal-body">
                                          <div class="form-group">
                                            <label class="col-md-3 control-label">Menu</label>
                                            <div class="col-md-9">
                                              <?= form_input('menu','','class="form-control" placeholder="Nama Menu" required'); ?>
                                              <?= form_hidden('id',$sub1->id_menu); ?>
                                            </div>
                                          </div>
                                          <div class="clearfix"></div><br/>
                                          <div class="form-group">
                                            <label class="col-md-3 control-label">Url</label>
                                            <div class="col-md-9">
                                              <?= form_input('url','','class="form-control" placeholder="Url Menu" required'); ?>
                                            </div>
                                          </div>
                                          <div class="clearfix"></div><br/>
                                          <div class="form-group">
                                            <label class="col-md-3 control-label">Parent</label>
                                            <div class="col-md-9">
                                              <?= form_dropdown('parent_id', $parent_id,$sub1->id_menu, 'required class="form-control" readonly disabled'); ?>
                                            </div>
                                          </div>
                                          <div class="clearfix"></div><br/>
                                        </div>
                                        <div class="modal-footer">
                                          <button type="reset" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                          <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                                        </div>
                                        <?= form_close(); ?>
                                      </div>
                                    </div>
                                  </div>
                                  <!-- tambah modal sub1 1-->

                                  <!-- edit modal sub 1 -->
                                  <div class="modal fade" id="editmenu<?= $sub1->id_menu ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                      <div class="modal-content">
                                        <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                          <h4 class="modal-title" id="myModalLabel">Edit Menu</h4>
                                        </div>
                                        <?= form_open(site_url('adminweb/menu/edit.asp'),'class="form-horizontal"'); ?>
                                        <div class="modal-body">
                                          <div class="form-group">
                                            <label class="col-md-3 control-label">Menu</label>
                                            <div class="col-md-9">
                                              <?= form_input('menu',$sub1->menu,'class="form-control" placeholder="Nama Menu" required'); ?>
                                              <?= form_hidden('id',$sub1->id_menu); ?>
                                            </div>
                                          </div>
                                          <div class="clearfix"></div><br/>
                                          <div class="form-group">
                                            <label class="col-md-3 control-label">Url</label>
                                            <div class="col-md-9">
                                              <?= form_input('url',$sub1->url,'class="form-control" placeholder="Url Menu" required'); ?>
                                            </div>
                                          </div>
                                          <div class="clearfix"></div><br/>
                                          
                                        </div>
                                        <div class="modal-footer">
                                          <button type="reset" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                          <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                                        </div>
                                        <?= form_close(); ?>
                                      </div>
                                    </div>
                                  </div>
                                  <!-- edit modal sub 1 -->

                                  <?php
                                }

                               ?>
                            </tbody>
                          </table>
                        </div>
                      </div>
                      
                    </td>
                  </tr>
                <?php
              }

            ?>

            <!-- tambah Modal 2 -->
            <div class="modal fade" id="tamsub1<?= $row->id_menu ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Tambah Menu Dari Parent <b><?= $row->menu ?></b></h4>
                  </div>
                  <?= form_open(site_url('adminweb/menu/tambah1edit.asp'),'class="form-horizontal"'); ?>
                  <div class="modal-body">
                    <div class="form-group">
                      <label class="col-md-3 control-label">Menu</label>
                      <div class="col-md-9">
                        <?= form_input('menu','','class="form-control" placeholder="Nama Menu" required'); ?>
                        <?= form_hidden('id',$row->id_menu); ?>
                      </div>
                    </div>
                    <div class="clearfix"></div><br/>
                    <div class="form-group">
                      <label class="col-md-3 control-label">Url</label>
                      <div class="col-md-9">
                        <?= form_input('url','','class="form-control" placeholder="Url Menu" required'); ?>
                      </div>
                    </div>
                    <div class="clearfix"></div><br/>
                    <div class="form-group">
                      <label class="col-md-3 control-label">Parent</label>
                      <div class="col-md-9">
                        <?= form_dropdown('parent_id', $parent_id,$row->id_menu, 'required class="form-control" readonly disabled'); ?>
                      </div>
                    </div>
                    <div class="clearfix"></div><br/>
                  </div>
                  <div class="modal-footer">
                    <button type="reset" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                  </div>
                  <?= form_close(); ?>
                </div>
              </div>
            </div>
            <!-- tambah Modal 2 -->

            <!-- edit Modal 1 -->
            <div class="modal fade" id="editmenu<?= $row->id_menu ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Menu</h4>
                  </div>
                  <?= form_open(site_url('adminweb/menu/edit.asp'),'class="form-horizontal"'); ?>
                  <div class="modal-body">
                    <div class="form-group">
                      <label class="col-md-3 control-label">Menu</label>
                      <div class="col-md-9">
                        <?= form_input('menu',$row->menu,'class="form-control" placeholder="Nama Menu" required'); ?>
                        <?= form_hidden('id',$row->id_menu); ?>
                      </div>
                    </div>
                    <div class="clearfix"></div><br/>
                    <div class="form-group">
                      <label class="col-md-3 control-label">Url</label>
                      <div class="col-md-9">
                        <?= form_input('url',$row->url,'class="form-control" placeholder="Url Menu" required'); ?>
                      </div>
                    </div>
                    <div class="clearfix"></div><br/>
                    
                  </div>
                  <div class="modal-footer">
                    <button type="reset" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                  </div>
                  <?= form_close(); ?>
                </div>
              </div>
            </div>
            <!-- edit Modal 1 -->
            <?php } ?>
            
          </tbody>
        </table>        
      </div>
        
    </div><!-- /.box-body -->
  </div><!-- /.box -->

</section><!-- /.content -->


<!-- Modal -->
<div class="modal fade" id="addmenu" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Tambah Menu</h4>
      </div>
      <?= form_open('','class="form-horizontal"'); ?>
      <div class="modal-body">
        <div class="form-group">
          <label class="col-md-3 control-label">Menu</label>
          <div class="col-md-9">
            <?= form_input('menu','','class="form-control" placeholder="Nama Menu" required'); ?>
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-3 control-label">Url</label>
          <div class="col-md-9">
            <?= form_input('url','','class="form-control" placeholder="Url Menu" required'); ?>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="reset" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
      <?= form_close(); ?>
    </div>
  </div>
</div>