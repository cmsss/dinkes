<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Subsub Kategori Produk Kemnag
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-8">
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">List</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                    <table class="table table-hover table-condensed" id="list_subkategori">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kategori</th>
                                <th>Sub Kategori</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            if(!empty($list)){
                            foreach ($list as $row) {
                                ?>
                                <tr>
                                    <td><?php echo $no; ?></td>
                                    <td><?php echo $row->main_kat; ?></td>
                                    <td><?= $row->sub_kat ?></td>
                                    <td>
                                        <a href="#edit_subkategori_<?php echo $row->id_sub_kat; ?>" title="edit" class="btn btn-sm btn-primary" data-toggle="modal"><i class="glyphicon glyphicon-pencil"></i></a>
                                        <a href="<?php echo site_url('dokumen/subkategori/delete/' . $row->id_sub_kat); ?>" title="hapus" class="btn btn-sm btn-warning" onclick="return confirm('Apakah anda yakin akan menghapus data ini?')"><i class="glyphicon glyphicon-trash"></i></a>
                                    </td>
                                </tr>
                            <div class="modal fade" id="edit_subkategori_<?php echo $row->id_sub_kat; ?>" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title" id="myModalLabel">
                                                Edit Kategori
                                            </h4>
                                        </div>
                                        <?php echo form_open('adminweb/produk-kemenag/subkategori/edit.asp'); ?>
                                        <div class="modal-body">
                                            <?php echo form_hidden('id_sub_kat', $row->id_sub_kat); ?>
                                            <?= form_dropdown('main_kat', $main_kat, $row->id_main_kat,'class="form-control"'); ?><br/>
                  <?php echo form_input('sub_kat', $row->sub_kat, 'class="form-control" placeholder="Nama Subkategori" required'); ?>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>  <?php echo form_submit('kirim', 'Simpan Perubahan', 'class="btn btn-primary"'); ?>
                                        </div>
                                        <?php echo form_close(); ?>
                                    </div>
                                </div>
                            </div>
                            <?php
                            $no++;
                        } }
                        ?>
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
      <div class="col-md-4">
          <div class="box box-info">
              <div class="box-header with-border">
                  <h3 class="box-title">Tambah Sub Kategory</h3>
              </div><!-- /.box-header -->
              <!-- form start -->
              <?php echo form_open('', 'class="form-horizontal"'); ?>
              <div class="box-body">
              <?= form_dropdown('main_kat', $main_kat, '','class="form-control" required'); ?><br/>
                  <?php echo form_input('sub_kat', '', 'class="form-control" placeholder="Nama Subkategori" required'); ?>
                  <div class="box-footer">
                      <?php echo form_submit('kirim', 'Tambah', 'class="btn btn-primary"'); ?>
                  </div><!-- /.box-footer -->
                  <?php echo form_close(); ?>
              </div>
          </div>
      </div>
      </div>
</section>
