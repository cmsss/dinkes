<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Album extends MX_Controller {

    

    public function __construct() {
        parent::__construct();
        if (!$this->autentifikasi->sudah_login())
            redirect('adminpage/site-login.asp','refresh');
        $this->load->model('m_album');
    }

    public function index() {
        $idincrement = auto_inc('m_album','id_album');
        if ($this->input->post()) {
            $data= array(
                'id_album'=> $idincrement,
                'album'=>$this->input->post('album'),
                'create'=> date('Y-m-d')." ".gmdate('H:i:s',time()+ 60 * 60 * 8)."",
                'flag'=>url_title($this->input->post('album'),'dash',TRUE).'.html',
                );
            $this->m_album->insert($data);
            redirect('adminweb/album.asp','refresh');
        }
    	$data['list'] = $this->m_album->get_all();
        $data['module'] = "album";
        $data['view_file'] = "list";
        echo Modules::run('template/render_master',$data);
    }
    public function add(){
        $idincrement = auto_inc('m_album','id_album');
        if ($this->input->post()) {
            $data= array(
                'id_album'=> $idincrement,
                'album'=>$this->input->post('album'),
                'create'=> date('Y-m-d')." ".gmdate('H:i:s',time()+ 60 * 60 * 8)."",
                'flag'=>url_title($this->input->post('album'),'dash',TRUE).'.html',
                );
            $this->m_album->insert($data);
            redirect('adminweb/album.asp','refresh');
            
            
        }
        $data['module']="album";
        $data['view_file']="add_album";
        echo Modules::run('template/render_master',$data);
    }
    public function edit($id){
        if ($this->input->post()) {
            $data= array(
                'album'=>$this->input->post('album'),
                'flag'=>url_title($this->input->post('album'),'dash',TRUE).'.html',
                );
            $this->m_album->update($id,$data);
            redirect('adminweb/album.asp','refresh');
            
            
        }
        $data['edit']=$this->m_album->get_by(array('id_album'=>$id));
        $data['module']="album";
        $data['view_file']="edit_album";
        echo Modules::run('template/render_master',$data);
    

    }
    public function hapus($id){
        $this->m_album->delete($id);
        redirect('adminweb/album.asp');
    }

    

}
