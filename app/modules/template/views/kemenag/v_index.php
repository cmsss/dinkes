<!DOCTYPE html>
<html>


<head>
<!-- <img src="http://localhost/kemenag/tmp/frontend/img/logo110.png" alt=""> -->
    
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Kementerian Agama Provinsi Gorontalo</title>
    <link rel="shortcut icon" href="<?php echo base_url(); ?>tmp/frontend/img/logo110.png"  >
    <link href="<?= base_url() ?>asset/kemenag/assets/themes/gorontalo/css/frame.style.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>asset/kemenag/assets/themes/gorontalo/css/fonts.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>asset/kemenag/assets/themes/gorontalo/css/style.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url() ?>asset/kemenag/assets/themes/gorontalo/css/responsive.css" rel="stylesheet" type="text/css" />


    <script src="<?= base_url() ?>asset/kemenag/assets/themes/gorontalo/js/jquery.min.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?= base_url() ?>asset/kemenag/assets/themes/gorontalo/css/b.css">
    <script src="<?= base_url() ?>asset/kemenag/assets/themes/gorontalo/js/j.js"></script>
    <script src="<?= base_url() ?>asset/kemenag/assets/themes/gorontalo/js/b.js"></script>
    <style>
        .dropdown-submenu {
            position: relative;
        }

        .dropdown-submenu .dropdown-menu {
            top: 0;
            left: 100%;
            margin-top: -1px;
        }
    </style>
</head>
<body>
    <div id="fb-root"></div>
    <script>
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id))
                return;
            js = d.createElement(s);
            js.id = id;
            js.src = "../connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5&appId=736855586388639";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <script>
        !function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
            if (!d.getElementById(id)) {
                js = d.createElement(s);
                js.id = id;
                js.src = p + "://platform.twitter.com/widgets.js";
                fjs.parentNode.insertBefore(js, fjs);
            }
            console.log('alfandy');
        }(document, "script", "twitter-wjs");
    </script>
    <div id="nav_menu" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">        
            <span></span>
            <span></span>
            <span></span>
        
    </div>
    <div id="nav_menu_close"><span></span></div>


    <!--==================== header =======================-->
    <div id="header">
        <?= $this->load->view('header') ?>
    </div>
    <!--==================== end header =======================-->

    <!--==================== NAV =======================-->
    <!-- <div id="nav"> -->
    <div id="nav">
        <nav class="navbar navbar-primary" style="background-color: #125889">
          <?= $this->load->view('nav') ?>
        </nav>
    </div>
    

    <!-- </div> -->
    <!--==================== End NAV =======================-->

    <div class="itu">

        <div class="container">
             <!-- ====================tengah ===================  -->
            <?php 
            if ($this->uri->segment(1) == '') {
                $this->load->view('beranda');
            }else {
                $this->load->view($module .'/'.$view);
            }
            ?>




             <!-- ====================tengah ===================  -->
            <div class="clearfix"></div>
        </div>
    </div>
    <div name="script">
        
    </div>


    <!--footer-->
    <?= $this->load->view('footer') ?>
    <!--footer-->


    <!-- Piwik -->
    <script type="text/javascript">
        var _paq = _paq || [];
        _paq.push(['trackPageView']);
        _paq.push(['enableLinkTracking']);
        (function () {
            var u = "http://www.kemenag.go.id/";
            _paq.push(['setTrackerUrl', u + 'piwik.php']);
            _paq.push(['setSiteId', 2]);
            var d = document, g = d.createElement('script'), s = d.getElementsByTagName('script')[0];
            g.type = 'text/javascript';
            g.async = true;
            g.defer = true;
            g.src = u + 'piwik.js';
            s.parentNode.insertBefore(g, s);
        })();
    </script>
    <!-- <noscript><p><img src="http://www.kemenag.go.id/piwik.php?idsite=2" style="border:0;" alt="" /></p></noscript> -->
    <!-- End Piwik Code -->

    <script src="<?= base_url() ?>asset/kemenag/assets/themes/gorontalo/js/plugin.js"></script>  
    <script src="<?= base_url() ?>asset/kemenag/assets/themes/gorontalo/js/controller.js"></script>  
    <!--Start of Tawk.to Script-->
    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/58c8001941acfb239f8cd448/default';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
    </script>
    <!--End of Tawk.to Script-->
    <!--End of Tawk.to Script-->



</body>

</html>
