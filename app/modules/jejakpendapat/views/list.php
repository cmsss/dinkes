<div class="container">


<?php if (!empty($count)){ ?>

<div class="isi">

    <div class="media">
      <center><div id="aaa" style="height: 100%; width:80%;"></div></center>
        <h4 class="media-heading" >
          <img src="" alt="" ="" />
          Detail hasil Jejak Pendapat Saat Ini:
        </h4>


        <div class="media-body">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <td>Kawasan Ekonomi Kecamatan (<?php echo $data1; ?>)	<?php $h1=100/$count*$data1; echo $h1.'%';?></td>
                        </tr>
                        <tr>
                            <td>Infrastruktur Perkotaan (<?php echo $data2; ?>)	 <?php $h2=100/$count*$data2; echo $h2.'%';?></td>
                        </tr>
                        <tr>
                            <td>Kartu Sejahtera (Gratis dari Lahir sampai Mati) (<?php echo $data3; ?>)	 <?php $h3=100/$count*$data3; echo $h3.'%';?></td>
                        </tr>
                        <tr>
                            <td>Penataan Birokrasi (<?php echo $data4; ?>)	 <?php $h4=100/$count*$data4; echo $h4.'%';?></td>
                        </tr>
                        <tr>
                            <td>Penguatan organisasi Agama (<?php echo $data5; ?>)	<?php $h5=100/$count*$data5; echo $h5.'%';?></td>
                        </tr>
                        <tr>
                            <td>Kawasan Ciber City dan Technopark (<?php echo $data6; ?>)	 <?php $h8=100/$count*$data6; echo $h8.'%';?></td>
                        </tr>
                        <tr>
                          <td>Jumlah Pemilih (<?php echo $count; ?>)</td>

                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
<?php }else{
  echo "Data Kosong";
} ?>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script type="text/javascript">

var chart1;// globally available
$(document).ready(function() {


var data1 = "<?php echo json_encode($data1); ?>";
var data2 = "<?php echo json_encode($data2); ?>";
var data3 = "<?php echo json_encode($data3); ?>";
var data4 = "<?php echo json_encode($data4); ?>";
var data5 = "<?php echo json_encode($data5); ?>";
var data6 = "<?php echo json_encode($data6); ?>";
data1 = data1*1;
data2 = data2*1;
data3 = data3*1;
data4 = data4*1;
data5 = data5*1;
data6 = data6*1;
    chart1 = new Highcharts.Chart({
       chart: {
          renderTo: 'aaa',
          type: 'pie'
       },
       title: {
          text: 'Diagram Jejak Pendapat'
       },
       series: [{
          name: ['Jejak Pendapat'],
          data: [
              ['Kawasan Ekonomi Kecamatan', data1],
              ['Infrastruktur Perkotaan', data2],
              ['Kartu Sejahtera (Gratis dari Lahir sampai Mati)', data3],
              ['Penataan Birokrasi', data4],
              ['Penguatan organisasi Agama', data5],
              ['Kawasan Ciber City dan Technopark', data6],

          ],
        }],

    });
 });
</script>


<script src="<?php echo base_url(); ?>tmp/highcharts/highcharts.js"></script>
<script src="<?php echo base_url(); ?>tmp/highcharts/highcharts-3d.js"></script>
<script src="<?php echo base_url(); ?>tmp/highcharts/exporting.js"></script>
