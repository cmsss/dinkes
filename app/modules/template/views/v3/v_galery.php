<div class="row">
    <div class="col-12" style="margin-bottom: -44px;">
        <div class="section-heading text-center mx-auto wow fadeInUp" data-wow-delay="300ms">
            <!-- <span>The Best</span> -->
            <h3>Galeri Foto</h3>
        </div>
    </div>
</div>
<div class="row">
    <!-- <div class="col-12 col-sm-6 col-lg-4"> -->
        <div class="footer-widget wow fadeInUp" data-wow-delay="300ms" style="margin-bottom: 25px;">
            <div class="gallery-list d-flex justify-content-between flex-wrap">
                <?php if (!empty($list_foto)): ?>
                    <?php foreach ($list_foto as $l_foto): ?>
                        <div class="col-sm-4" style="margin-bottom: 15px;">
                            <a href="<?= base_url('files/gorontalo/file/foto/'.$l_foto->id.'.jpg') ?>" class="gallery-img" title="<?php echo strip_tags($l_foto->keterangan) ?>">
                                <img style="object-fit: cover; width: 255px;height: 170px;" src="<?= base_url('files/gorontalo/file/foto/'.$l_foto->id.'.jpg') ?>" alt="<?php echo strip_tags($l_foto->keterangan) ?>">
                            </a>
                        </div>
                    <?php endforeach ?>
                <?php endif ?>
            </div>
        </div>
    <!-- </div> -->
</div>