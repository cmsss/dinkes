<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Profil extends MX_Controller {


    public function __construct() {
        parent::__construct();
        if (!$this->autentifikasi->sudah_login())
            redirect('adminpage/site-login.asp','refresh');
        $this->load->model('m_profil');
    }

    public function index() {
    	$data['list'] = $this->m_profil->get_all();
        $data['module'] = "profil";
        $data['view_file'] = "list";
        echo Modules::run('template/render_master',$data);
    }
    public function struktur()
    {
      $data['list'] = $this->m_profil->get_by(array('kategori'=>'STRUKTUR'));
        $data['module'] = "profil";
        $data['view_file'] = "struktur";
        echo Modules::run('template/render_master',$data);
    }
    public function visi_misi()
    {
      $data['list'] = $this->m_profil->get_by(array('kategori'=>'VISI DAN MISI'));
        $data['module'] = "profil";
        $data['view_file'] = "visi_misi";
        echo Modules::run('template/render_master',$data);
    }
    public function add(){

        $config['upload_path']="./uploads/";
        $config['allowed_types']="png|jpg";
        $config['max_size']="1024";
        $config['max_width']="2000";
        $config['max_height']="1000";
        $config['encrypt_name']= TRUE;
        $this->upload->initialize($config);
        $idincrement = auto_inc('m_profil','id_profil');
        if ($this->input->post()) {
            if ($this->upload->do_upload('img')) {
                     $img = $this->upload->data();
                     $data= array(
                        'id_profil'=> $idincrement,
                        'kategori'=>$this->input->post('kategori'),
                        'isi'=>$this->input->post('isi'),
                        'img'=>$this->upload->file_name,
                        );
                        if (!$this->m_profil->insert($data)){
                            echo "<script>alert('Profil berhasil disimpan');
                    window.location=('" . site_url('profile/struktur') . "');</script>";
                        }else{
                            echo "<script>alert('Gagal simpan prlofi');
                    window.location=('" . site_url('profile/struktur') . "');</script>";
                        }
            }

        }
        $data['module']="profil";
        $data['view_file']="add_profil";
        echo Modules::run('template/render_master',$data);
    }
    public function edit_visimisi($id)
    {
      if ($cek = $this->m_profil->get_by(array('id_profil'=>$id))){
          $config['upload_path']="./uploads/";
          $config['allowed_types']="jpg|png";
          $config['max_size']="1024";
          $config['max_width']="2000";
          $config['max_height']="2000";
          $config['encrypt_name']= TRUE;
          $this->upload->initialize($config);
          if ($this->input->post()) {
              if ($this->upload->do_upload('img')) {
                  $pathgambar = realpath(APPPATH . '../uploads/');
                  if ($cek->img != '') {
                      unlink($pathgambar . '/' . $cek->img);
                  }
                     $img = $this->upload->data();
                     $data= array(
                        'isi'=>$this->input->post('isi'),
                        'img'=>$this->upload->file_name,
                        );
                      $this->m_profil->update($id,$data);
                      redirect('adminweb/visi_misi.asp','refresh');
                  }else {
                    $data= array(

                       'isi'=>$this->input->post('isi'),
                       );
                      $this->m_profil->update($id,$data);
                      redirect('adminweb/visi_misi.asp','refresh');
              }
          }
      }
      $data['edit']=$this->m_profil->get_by(array('id_profil'=>$id));
      $data['module']="profil";
      $data['view_file']="edit_visimisi";
      echo Modules::run('template/render_master',$data);
    }
    public function edit($id){
      if ($cek = $this->m_profil->get_by(array('id_profil'=>$id))){
          $config['upload_path']="./uploads/";
          $config['allowed_types']="jpg|png";
          $config['max_size']="1024";
          $config['max_width']="2000";
          $config['max_height']="1000";
          $config['encrypt_name']= TRUE;
          $this->upload->initialize($config);
          if ($this->input->post()) {
              if ($this->upload->do_upload('img')) {
                  $pathgambar = realpath(APPPATH . '../uploads/');
                  if ($cek->img != '') {
                      unlink($pathgambar . '/' . $cek->img);
                  }
                     $img = $this->upload->data();
                     $data= array(
                        'isi'=>$this->input->post('isi'),
                        'img'=>$this->upload->file_name,
                        );
                      $this->m_profil->update($id,$data);
                      redirect('adminweb/struktur.asp','refresh');
                  }else {
                    $data= array(

                       'isi'=>$this->input->post('isi'),
                       );
                      $this->m_profil->update($id,$data);
                      redirect('adminweb/struktur.asp','refresh');
              }
          }
      }
      $data['edit']=$this->m_profil->get_by(array('id_profil'=>$id));
      $data['module']="profil";
      $data['view_file']="edit_profil";
      echo Modules::run('template/render_master',$data);
    }
    public function hapus($id){
      if ($cek = $this->m_profil->get_by(array('id_profil'=>$id))){
          $pathgambar = realpath(APPPATH . '../uploads/');
          if ($cek->img != '') {
              unlink($pathgambar . '/' . $cek->img);
          }
          $this->m_profil->delete($id);
      }
      redirect('adminweb/profil.asp');
    }



}
