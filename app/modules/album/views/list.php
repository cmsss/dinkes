  <section class="content-header">
    <h1></i>Album</h1>
  </section>
<section class="content">
  <div class="row">
    <div class="col-md-8">
      <div class="box">
        <div class="box-header">
            <h3 class="box-title">List Album</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
			<?php echo form_open(); ?>

			<table class="table table-hover table-condensed" id="list_kategori">
				<thead>
					<tr>
						<td>No</td>
						<td>Album</td>
						<td>Create</td>
						<td>Flag</td>
						<td>Aksi</td>
					</tr>
				</thead>
				<tbody>

					<?php
						$no=1;
						foreach ($list as $row) {?>
					<tr>
						<td><?php echo $no++; ?></td>
						<td><?php echo $row->album; ?></td>
						<td><?php echo $row->create; ?></td>
						<td><?php echo $row->flag; ?></td>
						<td>
							<a href="<?php echo site_url('adminweb/album/update/'.$row->id_album); ?>" class="btn btn-info"><i class="fa fa-edit"></i></a>
							<a href="<?php echo site_url('adminweb/album/delete/'.$row->id_album); ?>" onclick="return confirm('anda yakin untuk menghapus data ini');" class="btn btn-warning"><i class="fa fa-trash"></i></a>
						</td>
					</tr>
					<?php } ?>

				</tbody>
			</table>
			<?php echo form_close(); ?>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">List Album</h3>
				</div>
				<?php echo form_open('',array('class'=>'form-horizontal')); ?>
				<div class="box-body">
					<?php echo form_input('album',set_value('album'),'class="form-control" placeholder="Nama Album"'); ?>
					<div class="box-footer">
                        <?php echo form_submit('kirim', 'Tambah', 'class="btn btn-primary"'); ?>
                    </div><!-- /.box-footer -->
				</div>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</section>
