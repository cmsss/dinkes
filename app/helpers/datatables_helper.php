<?php

/*
 * function that generate the action buttons edit, delete
 * This is just showing the idea you can use it in different view or whatever fits your needs
 */

// function get_btn_pasien($id) {
//     $ci = & get_instance();
//     $html = '<span class="actions">';
//     $html .='<a href="' . site_url('apps/pasien/detail/' . $id) . '" title="Detail"><i class="fa fa-eye"></i></a> &nbsp;';
//     $html .='<a href="' . site_url('apps/pasien/edit/' . $id) . '" title="Edit"><i class="fa fa-edit"></i></a> &nbsp;';
//     if ($ci->auth->role(array('1'))) {
//         $html .='<a href="' . site_url('apps/pasien/hapus/' . $id) . '" onclick="return confirm(\'Apakah anda yakin ingin menghapus data ini ?\');"><i class="fa fa-trash"></i></a> &nbsp;';
//     }
//     $html .= '</span>';

//     return $html;
// }

if (!function_exists('get_btn')) {

    function get_btn($c, $id) {
        $CI = & get_instance();
        $html = '<span class="actions">';
        $html .='<a href="' . site_url('apps/' . $c . '/detail/' . $id) . '" title="Detail"><i class="fa fa-eye"></i></a> &nbsp;';
        $html .='<a href="' . site_url('apps/' . $c . '/edit/' . $id) . '" title="Edit"><i class="fa fa-edit"></i></a> &nbsp;';
        $html .='<a href="' . site_url('apps/' . $c . '/hapus/' . $id) . '" onclick="return confirm(\'Apakah anda yakin ingin menghapus data ini ?\');"><i class="fa fa-trash"></i></a> &nbsp;';
        $html .= '</span>';

        return $html;
    }

}

if (!function_exists('get_btn_b')) {

    function get_btn_b($c, $id) {
        $CI = & get_instance();
        $html = '<span class="actions">';
        $html .='<a href="' . site_url('apps/' . $c . '/detail/' . $id) . '" title="Detail"> Detail </a> | &nbsp;';
        $html .='<a href="' . site_url('apps/' . $c . '/edit/' . $id) . '" title="Edit"> Edit </a> | &nbsp;';
        $html .='<a href="' . site_url('apps/' . $c . '/hapus/' . $id) . '" onclick="return confirm(\'Apakah anda yakin ingin menghapus data ini ?\');"> Hapus</a> &nbsp;';
        $html .= '</span>';

        return $html;
    }

}

if (!function_exists('get_btn_alert')) {

    function get_btn_alert($c, $id) {
        $CI = & get_instance();
        $html = '<span class="actions">';
        $html .='<a href="' . site_url('apps/' . $c . '/detail/' . $id) . '" title="Detail"> Detail </a> | &nbsp;';
        $html .='<a href="' . site_url('apps/' . $c . '/edit/' . $id) . '" title="Edit"> Edit </a> | &nbsp;';
        $html .='<a href="#"  onclick="hapusAlert('.$id.')"> Hapus</a> &nbsp;';
        $html .= '</span>';

        return $html;
    }

}

if (!function_exists('get_btn_pasien')) {

    function get_btn_pasien( $id , $kd) {
        $CI = & get_instance();
        $html = '<span class="actions">';
        $html .='<a href="' . site_url('apps/data-pasien/detail/' . $id . '/' .$kd) . '" title="Detail"> Detail </a> | &nbsp;';
        $html .='<a href="' . site_url('apps/data-pasien/edit/' . $id. '/' .$kd) . '" title="Edit"> Edit </a> &nbsp;';
        $html .= '</span>';

        return $html;
    }

}