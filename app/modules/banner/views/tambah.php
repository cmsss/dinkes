<div class="container-fluid">
	<section class="content-header">
		<h4>Tambah Banner</h4>
	</section>
	<div class="box box-primary">
		<section class="content">

			<?php echo form_open_multipart('adminweb/banner/simpan',array('class'=>'form-horizontal')); ?>
			
			<div class="row form-group">
				<label class="control-label col-sm-2">File Banner</label>
				<div class="col-sm-10">
					<span class="pull-right">Format : JPG|PNG|GIF</span>
					<input type="file" name="nama" value="" required="required" class="form-control" >
				</div>
			</div>
			<div class="row form-group">
				<label class="control-label col-sm-2">Grup</label>
				<div class="col-sm-10">
					<?php echo form_dropdown('grup',array('' => 'Pilih Grup' , '2' => '2' , '10' => '10'),'','class="form-control" required onchange="grupBanner(this)"'); ?>
				</div>
			</div>
			<div id="grupid">
				
			</div>
			<div class="row form-group">
				<label class="control-label col-sm-2">Nomor</label>
				<div class="col-sm-10">
					<input type="number" name="nomor" class="form-control" required="required">
				</div>
			</div>
			
			
			<div class="row form-group">
				<label class="control-label col-sm-2">Tampil</label>
				<div class="col-sm-10">
					<div class="col-sm-12">
						<input type="radio" name="tampil" value="1" checked style="margin-right: 10px;" >
						<label class="" style="margin-right: 20px;">Ya</label >
							
							<input type="radio" name="tampil" value="0" style="margin-right: 10px; padding-left: 10px;">
							<label class="">Tidak</label>
						</div>
					</div>
				</div>
				

				<div class="row form-group">

					<div class="col-sm-12">
						<a href="<?= site_url('adminweb/banner.asp') ?>" class="btn btn-default pull-right">Kembali</a>
						<label class="col-sm-2"></label>
						<?php echo form_submit('submit','Simpan','class="btn btn-primary "'); ?>
					</div>
				</div>

				<?php echo  form_close(); ?>
			</section>
		</div>
	</div>


<script>
	
	function grupBanner(data) {
		var data = data.value;
		if (data == '10') {
			$("#grupid").html(
				'<div class="row form-group">' +
					'<label class="control-label col-sm-2">URL</label>' +
					'<div class="col-sm-10">'+
						'<input type="text" name="url" class="form-control" required="required" placeholder="url">'+
					'</div>'+
				'</div>'
				);	

		}else{
			$("#grupid").html('');
		}
	}
</script>