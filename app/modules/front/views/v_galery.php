
<div class="panel panel-isi">
    <div class="panel-heading">GALERY <?php echo strtoupper($this->uri->segment(2));?></div>
    <div class="panel-body">
    <?php if (!empty($list)): ?>
        <?php foreach ($list as $l): ?>
            <?php if ($l->kategori === 'Foto') {?>
               <!--  -->
                    <div class="col-md-4 portfolio-item">
                        <a href="#" data-toggle="modal" data-target="#modal<?php echo $l->id_galery; ?>">
                            <div class="embed-responsive embed-responsive-16by9">
                                <img class="embed-responsive-item" src="<?php echo base_url('uploads/'.$l->file) ; ?>" />
                            </div>
                            <h4>
                            <a href="#" style="color: black; text-decoration: none; text-align: center;" data-toggle="modal" data-target="#modal<?php echo $l->id_galery; ?>" ><?php echo $l->judul; ?></a>
                        </h4>
                        <p><?php echo $l->deskripsi; ?></p>
                        </a>
                        
                    </div>
        <div class="modal fade" id="modal<?php echo $l->id_galery; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="location.reload();"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel"><?php echo $l->judul; ?></h4>
                            </div>
                            <div class="modal-body">
                                <!--  -->
                                    <img class="img-responsive img-rounded center-block" src="<?php echo base_url('uploads/' . $l->file); ?>" alt="<?php echo $l->judul; ?>" width="100%" height="100%">
                               <!--  -->
                                    
                                
                            </div>
                        </div>
                    </div>
                </div>
      
          <?php }else{ ?>
            <div class="col-md-4 portfolio-item">
                <a href="#" data-toggle="modal" data-target="#modal<?php echo $l->id_galery; ?>">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="<?php echo $l->url; ?>" allowfullscreen></iframe>
                    </div>
                    <h3>
                    <a href="#" style="color: black; text-decoration: none; text-align: center;" data-toggle="modal" data-target="#modal<?php echo $l->id_galery;?>" ><?php echo $l->judul; ?></a>
                </h3>
                <p><?php echo $l->deskripsi; ?></p>
                </a>
                
            </div>
            <div class="modal fade in" id="modal<?php echo $l->id_galery;  ?>"  >
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title"><?php echo $l->judul; ?></h4>                           
                        </div>
                        <div class="modal-body">
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="<?php echo $l->url; ?>" allowfullscreen></iframe>
                            </div>                                  
                        </div>
                    </div>
                </div>
            </div>
          <?php } ?>
    <?php endforeach ?>
    <?php endif ?>

    </div>
    </div>

