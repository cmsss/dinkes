<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Kategori Produk Bappeda
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-8">
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">List</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                    <table class="table table-hover table-condensed" id="list_kategori">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kategori Produk</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            foreach ($list as $row) {
                                ?>
                                <tr>
                                    <td><?php echo $no; ?></td>
                                    <td><?php echo $row->main_kat; ?></td>
                                    <td>
                                        <a href="#edit_kategori_<?php echo $row->id_main_kat; ?>" title="edit" class="btn btn-sm btn-primary" data-toggle="modal"><i class="glyphicon glyphicon-pencil"></i></a>
                                        <a href="<?php echo site_url('adminweb/produk-kemenag/kategori/hapus/' . $row->id_main_kat); ?>" title="hapus" class="btn btn-sm btn-warning" onclick="return confirm('Apakah anda yakin akan menghapus data ini?')"><i class="glyphicon glyphicon-trash"></i></a>
                                    </td>
                                </tr>
                            <div class="modal fade" id="edit_kategori_<?php echo $row->id_main_kat; ?>" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title" id="myModalLabel">
                                                Edit Kategori
                                            </h4>
                                        </div>
                                        <?php echo form_open('adminweb/produk-kemenag/kategori/edit.asp'); ?>
                                        <div class="modal-body">
                                            <?php echo form_hidden('id_main_kat', $row->id_main_kat); ?>
                                            <?php echo form_input('main_kat', $row->main_kat, 'class="form-control" placeholder="Nama Kategori"'); ?>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>  <?php echo form_submit('kirim', 'Simpan Perubahan', 'class="btn btn-primary"'); ?>
                                        </div>
                                        <?php echo form_close(); ?>
                                    </div>
                                </div>
                            </div>
                            <?php
                            $no++;
                        }
                        ?>
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
      <div class="col-md-4">
          <div class="box box-info">
              <div class="box-header with-border">
                  <h3 class="box-title">Tambah Kategori</h3>
              </div><!-- /.box-header -->
              <!-- form start -->
              <?php echo form_open('', 'class="form-horizontal"'); ?>
              <div class="box-body">
                  <?php echo form_input('main_kat', '', 'class="form-control" placeholder="Nama Kategori" required'); ?>
                  <div class="box-footer">
                      <?php echo form_submit('kirim', 'Tambah', 'class="btn btn-primary"'); ?>
                  </div><!-- /.box-footer -->
                  <?php echo form_close(); ?>
              </div>
          </div>
      </div>
      </div>
</section>
