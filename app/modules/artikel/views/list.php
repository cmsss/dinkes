  <section class="content-header">
    <h1>Artikel</h1>
  </section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
            <h3 class="box-title">List Artikel</h3>
            <div class="pull-right">
                <a href="<?php echo site_url('adminweb/artikel/post.asp'); ?>" class="btn btn-info">Buat Tulisan</a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body">
			<?php echo form_open(); ?>
			<div class="table-responsive">
				<table class="table table-hover table-condensed" id="list_kategori">
					<thead>
						<tr>
							<th>No</th>
							<th>Judul</th>
							<th>Kategori</th>
							<th>Penulis</th>
							<th>URL</th>
							<th>status</th>
							<th>aksi</th>
						</tr>
					</thead>
					<tbody>
					
						<?php 
							$no=1;
							foreach ($list as $row) {?>
						<tr>
							<td><?php echo $no++; ?></td>
							<td><?php echo $row->judul; ?></td>
							<td><?php echo $row->kategori; ?></td>
							<td><?php echo $row->nama; ?></td>
							<td>
								<button type="button" class="copyyy btn btn-default" data-klik='<?= site_url("artikel-kemenag/".$row->flag); ?>' >Copy URl <i class="fa fa-fw fa-copy"></i></button>
								<?php if ($row->status === '1'): ?>
									<a  class="btn btn-default" target=" _parent" href="<?= site_url("artikel-kemenag/".$row->flag); ?>">Lihat  <i class="fa fa-fw fa-eye"></i></a>
								<?php endif ?>
							</td>
							
							<td><?php 
								if ($row->status === '1'){
									echo 'Aktif';
								}else{
									echo 'Tidak Aktif';
								}
							?></td>
							<td>
								<a href="<?php echo site_url('adminweb/artikel/update/'.$row->id_artikel); ?>" class="btn btn-info"><i class="fa fa-edit"></i></a>
								<a href="<?php echo site_url('adminweb/artikel/delete/'.$row->id_artikel); ?>" onclick="return confirm('anda yakin untuk menghapus data ini');" class="btn btn-warning"><i class="fa fa-trash"></i></a>
							</td>
						</tr>
						<?php } ?>
						
					</tbody>
				</table>
			</div>
			
			<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
</section>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>

    jQuery(document).ready(function() {
        $('.copyyy').click(function() {
            var data = $(this).data('klik');
            var $temp = $("<input>");
            var tes = $("body").append($temp);
            var tes2 = $temp.val(data).select();
            document.execCommand("copy");
            $temp.remove();
            $.toaster({priority: 'info', title: 'Update', message: 'Berhail Copy URL'});

        });    
    });

 </script>