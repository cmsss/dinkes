<!-- ##### Hero Area Start ##### -->
<section class="hero-area">
    <div class="hero-slides owl-carousel">
        <?php if (!empty($berita1)): ?>

            <?php foreach ($berita1 as $b1): ?>
                <?php 
                    $tgl = $b1->tgl_input;
                    $judul = $b1->judul;
                    
                    $judul_fix = flag($judul);


                 ?>
            <!-- Single Hero Slide -->
            <div class="single-hero-slide bg-img" style="background-image: url(<?= base_url('files/gorontalo/file/fotoberita/' . $b1->id. '.jpg'); ?>); max-height: 500px;">
                <div class="container h-100">
                    <div class="row h-100 align-items-center">
                        <div class="col-12">
                            <div class="hero-slides-content">
                                <h2 data-animation="fadeInUp" data-delay="400ms" style="text-shadow: 2px 2px 5px #000;"><?php echo strip_tags($b1->judul); ?></h2>
                                <a href="<?= site_url('berita/' .$b1->id. '/' .$judul_fix) ?>" class="btn btn-success" data-animation="fadeInUp" data-delay="700ms">Baca Selengkapnya</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php endforeach ?>
        <?php endif ?>            
        
    </div>
</section>
<!-- ##### Hero Area End ##### -->