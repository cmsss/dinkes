<div class="container-fluid">
  <section class="content-header">
    <h4>Tambah Agenda</h4>
	  </section>
	  <div class="box box-primary">
	    <section class="content">

		<?php echo form_open('',array('class'=>'form-horizontal')); ?>
			<div class="row form-group">
					<label class="control-label col-sm-2">Tema</label>
					<div class="col-sm-10">
						<?php echo form_input('tema',set_value('tema'),'class="form-control" placeholder="Tema"required'); ?>
					</div>
			</div>
			<div class="row form-group">
					<label class="control-label col-sm-2">Acara</label>
					<div class="col-sm-10">
						<textarea class="ckeditor" name="acara" rows="30" cols="80" style="width: 100%;" required="required"></textarea>
					</div>
			</div>
			<div class="row form-group">
					<label class="control-label col-sm-2">Tempat</label>
					<div class="col-sm-10">
						<?php echo form_input('tempat',set_value('tempat'),'class="form-control" placeholder="Tempat"required'); ?>
					</div>
			</div>
			<div class="row form-group">
					<label class="control-label col-sm-2">Tanggal</label>
					<div class="col-sm-10">
						<?php echo form_input('tgl',set_value('tgl'),'class="form-control datepicker" placeholder="Tanggal"required'); ?>
					</div>
			</div>
			<div class="row form-group">
					<label class="control-label col-sm-2">Waktu</label>
					<div class="col-sm-10">
						<?php echo form_input('waktu',set_value('waktu'),'class="form-control" placeholder="Waktu"required'); ?>
					</div>
			</div>
			<div class="row form-group">
					<label class="control-label col-sm-2">Status</label>
					<div class="col-sm-10">
	                    <input type="radio" name="status" value="0"  checked="true"> Belum Selesai
	                    <input type="radio" name="status" value="1"> Selesai
					</div>
			</div>
			<div class="row form-group">
					
					<div class="col-sm-10">
						<a href="<?= site_url('adminweb/agenda.asp') ?>" class="btn btn-default">Kembali</a>
						<?php echo form_submit('submit','Tambah Agenda','class="btn btn-primary"'); ?>
					</div>
			</div>
		<?php echo  form_close(); ?>
		</section>
	</div>
</div>



