<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User extends MX_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->autentifikasi->sudah_login())
            redirect('adminpage/site-login.asp','refresh');
        if ($this->autentifikasi->role(array('1')))
            redirect('adminpage/site-login.asp','refresh');
        $this->load->model('profile/m_profile');
    }

    public function index() {
    	if ($this->input->post()){
            if (!$this->m_user->get_by(array('username' => $this->input->post('username')))) {
                // $id_user = auto_inc('m_user','id_user');
                $id_profile = auto_inc('m_profile','id_profile_user');
                $data = array(
                    'id_user' => $this->input->post('username'),
                    'username' => $this->input->post('username'),
                    'password' => md5($this->input->post('password')),
                    'display_name' => $this->input->post('nama_lengkap'),
                    'last_pass_change' => date('Y-m-d H:i:s'),
                    'default_pass' => md5($this->input->post('password')),
                    'level' =>$this->input->post('level'),
                    'status' => '0'
                );
                $this->m_user->insert($data);
                $this->m_profile->insert(array('user_id'=>$this->input->post('username'),'id_profile_user'=>$id_profile,'full_name'=>$this->input->post('nama_lengkap')));
                    
                redirect('adminweb/user.asp','refresh');
            }else{
                echo "<script>alert('Maaf username yang anda masukan telah ada');
                window.location=('".site_url('adminweb/user.asp')."')</script>";
            }
    		
    	}
    	$data['list_level'] = drop_list('m_level','id_level_user','level_user','Pilih Level User');
    	$data['list'] = query_sql("select a.id_user, a.username, a.display_name, a.status, a.level, b.level_user from t_user a, t_level_user b where a.level = b.id_level_user");
        $data['module'] = "user";
        $data['view_file'] = "list";
        echo Modules::run('template/render_master',$data);
    }

    public function edit(){
        if ($this->input->post('password')!='') {
        $data = array(
          'username'=> $this->input->post('username'),
          'password'=> md5($this->input->post('password')),
          'level'=> $this->input->post('level'),
          'display_name'=> $this->input->post('display_name'),

        );
        if ($this->m_user->update($this->input->post('id_user'),$data)) {
            $dataProfil = array(
                'full_name' => $this->input->post('display_name'),
            );
            $this->m_profile->update_by(array('user_id'=>$this->input->post('id_user')),$dataProfil);
            redirect('adminweb/user.asp','refresh');
        }
      }else {
        $data = array(
          'username'=> $this->input->post('username'),
          'level'=> $this->input->post('level'),
          'display_name'=> $this->input->post('display_name'),

        );
        if ($this->m_user->update($this->input->post('id_user'),$data)) {
            $dataProfil = array(
                'full_name' => $this->input->post('display_name'),
            );
            $this->m_profile->update_by(array('user_id'=>$this->input->post('id_user')),$dataProfil);
            redirect('adminweb/user.asp','refresh');
        }
      }
    }
    

    public function level(){
    	if ($this->input->post()){
    		$id_level = auto_inc('m_level','id_level_user');
    		$data = array(
    			'id_level_user' => $id_level,
    			'level_user' => $this->input->post('level',true),
    			'keterangan' => $this->input->post('keterangan',true)
    		);
    		$this->m_level->insert($data);
    		$hak = $this->input->post('hak_akses');
    		foreach ($hak as $val) {
	    		$data_hak = array(
	    			'id_hak_akses' => auto_inc('m_hak_akses','id_hak_akses'),
	    			'level_id' => $id_level,
	    			'menu_id' => $val
	    		);
	    		$this->m_hak_akses->insert($data_hak);
	    	}
    		redirect('adminweb/level-user.asp');
    	}
    	$data['list_menu'] = multiple_list('m_menu','id_menu','menu');
    	$data['list'] = $this->m_level->get_all();
    	$data['module'] = "user";
    	$data['view_file'] = "list_level";
    	echo Modules::run('template/render_master',$data);
    }

    public function edit_level(){
    	if ($this->input->post()){
	    	$id_level = $this->input->post('id');
            $data = array(
                'id_level_user' => $id_level,
                'level_user' => $this->input->post('level',true),
                'keterangan' => $this->input->post('keterangan',true)
            );
            $this->m_level->update($id_level,$data);
            $hak = $this->input->post('hak_akses');
            if (!empty($hak)){
                foreach ($hak as $val) {
                    $data_hak = array(
                        'level_id' => $id_level,
                        'menu_id' => $val
                    );
                    $this->m_hak_akses->update($this->input->post('id_hak'),$data_hak);
                }
            }
            redirect('adminweb/level-user.asp');
   	 	}else{
   	 		redirect('adminweb/level-user.asp','refresh');
   	 	}
    }

    public function delete_level($id){
    	if ($this->m_level->get_by(array('id_level_user'=>$id))){
    		$this->m_level->delete($id);
    	}
    	redirect('adminweb/level-user.asp','refresh');
    }

  
    public function delete($id){
        if ($this->m_user->get_by(array('id_user'=>$id))){
    		$this->m_user->delete($id);
    	}
    	redirect('adminweb/user.asp','refresh');
    }

    public function status($id,$stat){
    	if ($this->m_user->get_by(array('id_user'=>$id))){
    		$this->m_user->update($id,array('status'=>$stat));
    	}
    	redirect('adminweb/user.asp','refresh');
    }

    

}

