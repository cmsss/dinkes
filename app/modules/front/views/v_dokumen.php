<?php 
$seq1 = $this->uri->segment('2');
$seq2 = $this->uri->segment('3');
$kat = str_replace('-', ' ', $seq2);
$sub = strtoupper($kat);
?>
<div class="panel panel-isi">
    <div class="panel-heading">DOKUMEN / <?= strtoupper($seq1); ?> / <?= $sub; ?></div>
    <div class="panel-body">
<div class="isi">
    <?php 
    if (!empty($dokumen)){
    foreach ($dokumen as $doc) {
        ?>
        <div class="media">
            <h4 class="media-heading">
                <div class="pull-left">
                    <b><?php echo $doc->nama_dokumen; ?></b><br/>
                    <small><?php echo $doc->ket_dokumen; ?></small>
                </div>
                <div class="pull-right">
                    <a href="<?= $doc->link; ?>" title="<?php echo $doc->file; ?>"><small><span class="glyphicon glyphicon-download"></span> Download File</small></a>
                </div>
            </h4> 
        </div>
    <hr/>
    <?php } 
        }else{
            echo "Belum ada data";
        }   
    ?>
</div>
</div>
</div>