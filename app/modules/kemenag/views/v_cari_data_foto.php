<div class="panel panel-theme shadow wow zoomIn">
    <div class="panel-heading">
        <h3 class="panel-title">ditemukan <?= $data_yang_dicari_count->id ?> Artikel </h3>
    </div>
    <div class="panel-body">
        
            <?= form_open('', 'class=" form-horizontal"'); ?>
                <div class="form-group">
                    <div class="col-sm-4">
                        <div class="col-sm-6">
                            <?= form_input('tgl_a',$tgl_a, 'class="form-control datepicker" '); ?>
                        </div>
                        <div class="col-sm-1">
                            -
                        </div>
                        <div class="col-sm-5">
                            <?= form_input('tgl_b',$tgl_b, 'class="form-control datepicker"'); ?>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <?= form_dropdown('kategori', $l_kategori,$kategori, 'class="form-control "'); ?>
                    </div>
                    <div class="col-sm-2">
                        <?= form_dropdown('by', $semua,$by, 'class="form-control "'); ?>
                    </div>
                    <div class="col-sm-3">
                        
                        <div class="form-group has-feedback">
                            <input name="data" type="text" class="form-control typeahead rounded" value="<?= $hasil ?>" placeholder="Kata Kunci">
                            <button type="submit" class="btn btn-theme fa fa-search form-control-feedback rounded"></button>
                        </div>
                    </div>
                </div>
            <?= form_close(); ?>
            <?php

                if (!empty($data_yang_dicari)) {
                    foreach ($data_yang_dicari as $l_foto) {
                        // $tgl = explode(' ', $l_foto->tanggal);
                        ?>

                            <div class="col-lg-4 col-md-6 col-sm-6 col-xl-12 ">
                                <div class="cbp cbp-caption-active cbp-caption-overlayBottomReveal cbp-ready">
                                    <div class="cbp-item web-design mobile">
                                        <div class="cbp-item-wrapper">
                                            <div class="cbp-caption">
                                                <div class="cbp-caption-defaultWrap">
                                                    <img src="<?= base_url('files/gorontalo/file/foto/'.$l_foto->id . '.jpg') ?>" alt="...">
                                                </div>
                                                <div class="cbp-caption-activeWrap">
                                                    <div class="cbp-l-caption-alignCenter">
                                                        <div class="cbp-l-caption-body">
                                                            <a href="<?= base_url('files/gorontalo/file/foto/' . $l_foto->id . '.jpg') ?>" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="<?= $l_foto->keterangan ?>">Perbesar</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="projects-title"><?= substr($l_foto->keterangan ,0 ,40) ?></div>
                                            <div class="cbp-l-grid-projects-desc"><?php //echo nama_hari($tgl['0']).' '.  tgl_indo($tgl['0']) ?></div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>

                        <?php
                    }
                }

            ?>
        
        
        
    </div>
</div>