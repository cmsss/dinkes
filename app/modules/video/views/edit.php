
<link href="http://vjs.zencdn.net/c/video-js.css" rel="stylesheet">
<script src="http://vjs.zencdn.net/c/video.js"></script>
<div class="container-fluid">
	<section class="content-header">
		<h4>Edit Video</h4>
	</section>
	<div class="box box-primary">
		<section class="content">

			<?php echo form_open_multipart('adminweb/video/simpan-perubahan',array('class'=>'form-horizontal')); ?>
			<?= form_hidden('id', $e->id); ?>
			<?= form_hidden('ts', $e->ts); ?>
			<div class="row form-group">
				<label class="control-label col-sm-2"></label>
				<div class="col-sm-10">
					<video id="video1" class="video-js vjs-default-skin"  width="100%" height="240" 
                        data-setup='{"controls" : true, "autoplay" : false, "preload" : "auto"}'>
                        <source src="<?= base_url('files/gorontalo/file/video/' .$e->id . '.flv') ?>" type="video/x-flv">
                    </video>
					
				</div>
			</div>
			
			<div class="row form-group">
				<label class="control-label col-sm-2">durasi</label>
				<div class="col-sm-10">
					<?php echo form_input('durasi',$e->durasi,'class="form-control" required placeholder="durasi"'); ?>
				</div>
			</div>
			<div class="row form-group">
				<label class="control-label col-sm-2">Keterangan</label>
				<div class="col-sm-10">
					<?php echo form_input('keterangan',$e->keterangan,'class="form-control" required placeholder="Keterangan"'); ?>
				</div>
			</div>
			<div class="row form-group">
				<label class="control-label col-sm-2">Kategori</label>
				<div class="col-sm-10">
					<?php echo form_dropdown('kategori', $kategori, $e->kategori , 'class="form-control" required'); ?>
				</div>
			</div>
			
			<div class="row form-group">
				<label class="control-label col-sm-2">Tampil</label>
				<div class="col-sm-10">
					<div class="col-sm-12">
						<input type="radio" name="tampil" value="1" style="margin-right: 10px;" <?= $t1 = ($e->tampil == '1' ? 'checked ' : ''); ?> >
						<label class="" style="margin-right: 20px;">Ya</label >
						
						<input type="radio" name="tampil" value="0" style="margin-right: 10px; padding-left: 10px;" <?= $t0 = ($e->tampil == '0' ? 'checked ' : ''); ?>>
						<label class="">Tidak</label>
					</div>
				</div>
			</div>
			<div class="row form-group">
				<label class="control-label col-sm-2">Setuju</label>
				<div class="col-sm-10">
					<div class="col-sm-12">
						<input type="radio" name="setuju" value="1" checked style="margin-right: 10px;" <?= $s1 = ($e->setuju == '1' ? 'checked ' : ''); ?>>
						<label class="" style="margin-right: 20px;">Ya</label >
						
						<input type="radio" name="setuju" value="0" style="margin-right: 10px; padding-left: 10px;" <?= $s0 = ($e->setuju == '0' ? 'checked ' : ''); ?>>
						<label class="">Tidak</label>
					</div>
				</div>
			</div>
			<div class="row form-group">
				<label class="control-label col-sm-2">Lengket</label>
				<div class="col-sm-10">
					<div class="col-sm-12">
						<input type="radio" name="lengket" value="1" checked style="margin-right: 10px;" <?= $l1 = ($e->lengket == '1' ? 'checked ' : ''); ?>>
						<label class="" style="margin-right: 20px;">Ya</label >
						
						<input type="radio" name="lengket" value="0" style="margin-right: 10px; padding-left: 10px;" <?= $l0 = ($e->lengket == '0' ? 'checked ' : ''); ?>>
						<label class="">Tidak</label>
					</div>
				</div>
			</div>

			<div class="row form-group">

				<div class="col-sm-12">
					<a href="<?= site_url('adminweb/video.asp') ?>" class="btn btn-default pull-right">Kembali</a>
					<label class="col-sm-2"></label>
					<?php echo form_submit('submit','Simpan','class="btn btn-primary "'); ?>
				</div>
			</div>

			<?php echo  form_close(); ?>
		</section>
	</div>
</div>


