<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_statistik extends MY_Model {

    public function __construct() {
        parent::__construct();
        parent::set_table('tbl_statistik', 'id_statistik');
    }

    public function get_stat($date = FALSE) {
        if ($date === FALSE) {
            $a = $this->db->count_all('tbl_statistik');
        } else {
            if ($date === date('Y-m-d')) {
                $this->db->where(array('date' => $date));
                $a = $this->db->count_all('tbl_statistik');
            } else if ($date === date('m')) {
                $this->db->where("MONT(date) = $date");
                $a = $this->db->count_all('tbl_statistik');
            } else if ($date === date('Y')) {
                $this->db->where("YEAR(date) = $date");
                $a = $this->db->count_all('tbl_statistik');
            }else{
                $a = $this->db->count_all('tbl_statistik');
            }
        }
        return $a;
    }

}
