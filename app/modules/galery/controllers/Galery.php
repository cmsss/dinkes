<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Galery extends MX_Controller {

   
    public function __construct() {
        parent::__construct();
        if (!$this->autentifikasi->sudah_login())
            redirect('adminpage/site-login.asp','refresh');
        $this->load->model('m_galery');
    }

    public function index() {
    	$data['list'] = $this->m_galery->get_all();
        $data['module'] = "galery";
        $data['view_file'] = "list";
        echo Modules::run('template/render_master',$data);
    }
    public function add(){
        $nama_file = "file_". time();
        $config['upload_path']="./uploads/";
        $config['allowed_types']="jpg|png";
        $config['max_size']="1024";
        $config['max_width']="2000";
        $config['max_height']="1000";
        $config['encrypt_name']= TRUE;
        $this->upload->initialize($config);
        $idincrement = auto_inc('m_galery','id_galery');

        if ($this->input->post()) {            
                if ($this->upload->do_upload('file')) {
                    $data= array(
                    'id_galery'=> $idincrement,
                    'judul'=>$this->input->post('judul'),
                    'deskripsi'=>$this->input->post('deskripsi'),
                    'kategori'=>$this->input->post('kategori'),
                    'create_date'=>date('Y-m-d')." ".gmdate('H:i:s',time()+ 60 * 60 * 8)."",
                    'file'=> $this->upload->file_name,
                    'flag'=>url_title($this->input->post('judul'),'dash',TRUE).'.html',
                    'album'=>$this->input->post('album'),
                );
                    if (!$this->m_galery->insert($data)){
                    echo "<script>alert('Galery berhasil disimpan');
            window.location=('" . site_url('adminweb/galery.asp') . "');</script>";
                }else{
                    echo "<script>alert('Gagal simpan Galery');
            window.location=('" . site_url('adminweb/galery.asp') . "');</script>";
                }           
            
        }else{
            $data= array(
                    'id_galery'=> $idincrement,
                    'judul'=>$this->input->post('judul'),
                    'deskripsi'=>$this->input->post('deskripsi'),
                    'kategori'=>$this->input->post('kategori'),
                    'url'=>$this->input->post('url'),
                    'create_date'=>date('Y-m-d')." ".gmdate('H:i:s',time()+ 60 * 60 * 8)."",
                    'flag'=>url_title($this->input->post('judul'),'dash',TRUE).'.html',
                    
                );
                    if (!$this->m_galery->insert($data)){
                    echo "<script>alert('Galery berhasil disimpan dengan vidieo');
            window.location=('" . site_url('adminweb/galery.asp') . "');</script>";
                }else{
                    echo "<script>alert('Gagal simpan Galery');
            window.location=('" . site_url('adminweb/galery.asp') . "');</script>";
                } 
        }
    }
        $data['l_album']=list_album();
        $data['module']="galery";
        $data['view_file']="add_galery";
        echo Modules::run('template/render_master',$data);
    }
    public function edit($id){
        if ($cek = $this->m_galery->get_by(array('id_galery'=>$id))){
        $config['upload_path']="./uploads/";
        $config['allowed_types']="jpg|png";
        $config['max_size']="1024";
        $config['max_width']="2000";
        $config['max_height']="1000";
        $config['encrypt_name']= TRUE;
        $this->upload->initialize($config);
        $idincrement = auto_inc('m_galery','id_galery');
        if ($this->input->post()) {
                if ($this->upload->do_upload('file')) {
                    $pathgambar = realpath(APPPATH . '../uploads/');
                    if ($cek->file != '') {
                        unlink($pathgambar . '/' . $cek->file);
                    }
                $data= array(
                   'judul'=>$this->input->post('judul'),
                    'deskripsi'=>$this->input->post('deskripsi'),
                    'kategori'=>$this->input->post('kategori'),
                    'file'=> $this->upload->file_name,
                    'flag'=>url_title($this->input->post('judul'),'dash',TRUE).'.html',
                    'album'=>$this->input->post('album'),
                    );
                $this->m_galery->update($id,$data);
                redirect('adminweb/galery.asp','refresh');
            }else{
                $data= array(
                   'judul'=>$this->input->post('judul'),
                    'deskripsi'=>$this->input->post('deskripsi'),
                    'kategori'=>$this->input->post('kategori'),
                    'flag'=>url_title($this->input->post('judul'),'dash',TRUE).'.html',
                    );
                $this->m_galery->update($id,$data);
                redirect('adminweb/galery.asp','refresh');
            }
            
            
        }
    }
        $data['edit']=$this->m_galery->get_by(array('id_galery'=>$id));
        $data['l_album']=list_album();
        $data['module']="galery";
        $data['view_file']="edit_galery";
        echo Modules::run('template/render_master',$data);
    

    }
    public function hapus($id){
        if ($cek = $this->m_galery->get_by(array('id_galery'=>$id))){
            $pathgambar = realpath(APPPATH . '../uploads/');
            if ($cek->file != '') {
                unlink($pathgambar . '/' . $cek->file);
            }
            $this->m_galery->delete($id);
        }
        redirect('adminweb/galery.asp');
    }

    

}
