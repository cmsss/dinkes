<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Teks Berejalan
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
      <div class="col-md-12">
          <div class="box box-info">
              <div class="box-header with-border">
                  <h3 class="box-title">Tambah Teks Berjalan</h3>
              </div><!-- /.box-header -->
              <!-- form start -->
              <?php echo form_open('', 'class="form-horizontal"'); ?>
              <div class="box-body">
                  <?php echo form_input('isi', '', 'class="form-control" placeholder="Text Berjalan" id="isi" required'); ?>
                  <div class="box-footer">
                      <?php echo form_submit('kirim', 'Tambah', 'class="btn btn-primary"'); ?>
                  </div><!-- /.box-footer -->
                  <?php echo form_close(); ?>
              </div>
          </div>
      </div>
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">List</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                    <table class="table table-hover table-condensed" id="list_kategori">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Teks Berjalan</th>
                                <th>Tampil</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            foreach ($list as $row) {
                                ?>
                                <tr>
                                    <td><?php echo $no; ?></td>
                                    <td><?php echo $row->isi; ?></td>
                                    <td>
                                    	<div id="tampil<?= $row->id ?>">
										<?php
											if ($row->tampil == '1') {
												?>
													<a href="#" onclick="tampilA('<?= $row->id ?>');"><small class="label  bg-green">Ya</small></a>
												<?php
											}else {
												?>
													<a href="#" onclick="tampilA('<?= $row->id ?>');"><small class="label  bg-red">Tidak</small></a>
												<?php
											}

										?>
										</div>
                                    </td>
                                    <td>
                                        <a href="#edit_kategori_<?php echo $row->id; ?>" title="edit" class="btn btn-primary" data-toggle="modal"><i class="glyphicon glyphicon-pencil"></i></a>
                                        <a href="<?php echo site_url('adminweb/teksberjalan/delete/' . $row->id); ?>" title="hapus" class="btn btn-warning" onclick="return confirm('Apakah anda yakin akan menghapus data ini?')"><i class="glyphicon glyphicon-trash"></i></a>
                                    </td>
                                </tr>
                            <div class="modal fade" id="edit_kategori_<?php echo $row->id; ?>" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title" id="myModalLabel">
                                                Edit Teks Berjalan
                                            </h4>
                                        </div>
                                        <?php echo form_open('adminweb/teksberjalan/edit'); ?>
                                        <div class="modal-body">
                                            <?php echo form_hidden('id', $row->id); ?>
                                            <?php echo form_input('isi', $row->isi, 'required class="form-control" placeholder="Nama Teks Berjalan"'); ?>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>  <?php echo form_submit('kirim', 'Simpan Perubahan', 'class="btn btn-primary"'); ?>
                                        </div>
                                        <?php echo form_close(); ?>
                                    </div>
                                </div>
                            </div>
                            <?php
                            $no++;
                        }
                        ?>
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
      </div>
</section>


<script>
	
	function tampilA(data) {
		// alert(data);
		var nilai = data;
		$.ajax({
			url: '<?php echo site_url();?>adminweb/teksberjalan/tampila/' + nilai,
			// type: 'POST',
			dataType: 'html',
			beforeSend : function(){

			},
			success : function(data){
				$("#tampil" + nilai).html(data);
			}
		});
		
		
	}
</script>