<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>asset/plugin/maskmoney/jquery.maskMoney.js" type="text/javascript"></script>
<div class="desc_calczakat">
	<h4><strong>Zakat Perternakan dan Perikanan</strong></h4>
	<blockquote> Zakat Perternakan Dikenakan pada hewan ternak yang digembalakan di tempat penggembalaan umum. Nisab dan Kadar Zakat atas <strong>ternak Unta, Sapi/kerbau , kuda dan kambing</strong>. Kadar Zakat atas hasil perikanan sebesar <strong>2,5%.</strong></blockquote><br>
</div>
<div class="table-responsive">
	<table class="table table-striped table-bordered table-hover">
		<tbody>
			<tr>
				<th colspan="2" class="header"><strong>Unta</strong></th>
			</tr>
			<tr>
				<th class="label_zakat">Jumlah ternak Unta</th>
				<th class="value_zakat"><input onkeyup="Unta(this);" type="text" id="unta" class="input_angka" style="width:100px" > Ekor</th>
			</tr>
			<tr class="total_zakat">
				<th class="label_zakat"><strong>Zakat yang Wajib Dikeluarkan Untuk Unta</strong></th>
				<th class="value_zakat">
					<font id="hasil_unta">
						
					</font>
				</th>
			</tr>
			<tr>
				<th colspan="2" class="header"><strong>Sapi/Lembu</strong></th>
			</tr>
			<tr>
				<th class="label_zakat">Jumlah ternak Sapi/Lembu</th>
				<th class="value_zakat"><input onkeyup="Sapi(this);" type="text" id="unta" class="input_angka" style="width:100px" > Ekor</th>
			</tr>
			<tr class="total_zakat">
				<th class="label_zakat"><strong>Zakat yang Wajib Dikeluarkan Untuk Sapi/Lembu</strong></th>
				<th class="value_zakat">
					<font id="hasil_sapi">
						
					</font>
				</th>
			</tr>
			<tr>
				<th colspan="2" class="header"><strong>Kuda</strong></th>
			</tr>
			<tr>
				<th class="label_zakat">Jumlah ternak Kuda</th>
				<th class="value_zakat"><input onkeyup="Kuda(this);" type="text" id="kuda" class="input_angka" style="width:100px" > Ekor</th>
			</tr>
			<tr class="total_zakat">
				<th class="label_zakat"><strong>Zakat yang Wajib Dikeluarkan Untuk Kuda</strong></th>
				<th class="value_zakat">
					<font id="hasil_kuda">
						
					</font>
				</th>
			</tr>
			<tr>
				<th colspan="2" class="header"><strong>Kambing</strong></th>
			</tr>
			<tr>
				<th class="label_zakat">Jumlah ternak Kambing</th>
				<th class="value_zakat"><input onkeyup="Kambing(this);" type="text" id="kambing" class="input_angka" style="width:100px" > Ekor</th>
			</tr>
			<tr class="total_zakat">
				<th class="label_zakat"><strong>Zakat yang Wajib Dikeluarkan Untuk Kambing</strong></th>
				<th class="value_zakat">
					<font id="hasil_kambing">
						
					</font>
				</th>
			</tr>

			<tr>
				<th colspan="2" class="header" id="keterangan">&nbsp;</th>
			</tr>
		</tbody>
	</table>
</div>

<h3>Nisab dan Kadar Zakat Atas Ternak</h3>
<div class="table-responsive">
	<table class="table table-striped table-bordered table-hover">
		<tbody>
			<tr>
				<th colspan="2" class="header"><strong>Unta</strong></th>
			</tr>
			<tr>
				<th class="label_zakat">Nisab (EKOR)</th>
				<th class="value_zakat">Zakat Yang Wajib Dikeluarkan</th>
			</tr>
			<!-- unta -->
			<tr>
				<td class="label_zakat">25-35</td>
				<td >1 Ekor anak Unta betina (umur >1 tahun)</td>
			</tr>
			<tr>
				<td class="label_zakat">36-45</td>
				<td >2 Ekor anak Unta betina (umur >2 tahun)</td>
			</tr>
			<tr>
				<td class="label_zakat">46-60</td>
				<td >3 Ekor anak Unta betina (umur >3 tahun)</td>
			</tr>
			<tr>
				<td class="label_zakat">61-75</td>
				<td >3 Ekor anak Unta betina (umur >4 tahun)</td>
			</tr>
			<tr>
				<td class="label_zakat">76-90</td>
				<td >2 Ekor anak Unta betina (umur >2 tahun)</td>
			</tr>
			<tr>
				<td class="label_zakat">91-120</td>
				<td >2 Ekor anak Unta betina (umur >3 tahun)</td>
			</tr>
			<tr>
				<td class="label_zakat">121-129</td>
				<td >3 Ekor anak Unta betina (umur >2 tahun)</td>
			</tr>
			<tr>
				<td class="label_zakat">130-139</td>
				<td >1 Ekor anak Unta betina (umur >3 tahun)<br>
				 dan 1 ekor anak unta Betina ( >2 tahun)</td>
			</tr>
			<tr>
				<td class="label_zakat">140-149</td>
				<td >2 Ekor anak Unta betina (umur >3 tahun)<br>
				 dan 1 ekor anak unta Betina ( >2 tahun)</td>
			</tr>
			<tr>
				<td class="label_zakat">150-159</td>
				<td >3 Ekor anak Unta betina (umur >3 tahun)</td>
			</tr>
			<tr>
				<td class="label_zakat">160-169</td>
				<td >4 Ekor anak Unta betina (umur >2 tahun)</td>
			</tr>
			<tr>
				<td class="label_zakat">170-179</td>
				<td >3 Ekor anak Unta betina (umur >2 tahun)<br>
				 dan 1 ekor anak unta Betina ( >3 tahun)</td>
			</tr>
			<tr>
				<td class="label_zakat">180-189</td>
				<td >2 Ekor anak Unta betina (umur >2 tahun)<br>
				 dan 2 ekor anak unta Betina ( >2 tahun)</td>
			</tr>
			<tr>
				<td class="label_zakat">190-199</td>
				<td >3 Ekor anak Unta betina (umur >3 tahun)<br>
				 dan 1 ekor anak unta Betina ( >2 tahun)</td>
			</tr>
			<tr>
				<td class="label_zakat">200-209</td>
				<td >4 Ekor anak Unta betina (umur >3 tahun)<br>
				 dan 5 ekor anak unta Betina ( >2 tahun)</td>
			</tr>
			<!-- unta -->
			<tr>
				<th colspan="2" class="header"><strong>Sapi/Lembu</strong></th>
			</tr>
			<tr>
				<th class="label_zakat">Nisab (EKOR)</th>
				<th class="value_zakat">Zakat Yang Wajib Dikeluarkan</th>
			</tr>
			<!-- sapi -->
			<tr>
				<td class="label_zakat">30-59</td>
				<td >1 Ekor Anak Sapi< betina/td>
			</tr>
			<tr>
				<td class="label_zakat">60-69</td>
				<td >2 Ekor Anak Sapi Jantan</td>
			</tr>
			<tr>
				<td class="label_zakat">70-79</td>
				<td >1 Ekor Anak Sapi Betina dan 1 ekor anak sapi jantan</td>
			</tr>
			<tr>
				<td class="label_zakat">80-89</td>
				<td >2 Ekor Anak Sapi Betina</td>
			</tr>
			<tr>
				<td class="label_zakat">90-99</td>
				<td >3 Ekor Anak Sapi Jantan</td>
			</tr>
			<tr>
				<td class="label_zakat">100-109</td>
				<td >1 ekor anak sapi betina dan 2 ekor anak sapi</td>
			</tr>
			<tr>
				<td class="label_zakat">110-119</td>
				<td >2 Ekor Anak Sapi Jantan</td>
			</tr>
			<tr>
				<td class="label_zakat">> 120</td>
				<td >3 ekor anak sapi betina atau 3 ekor anak sapi jantan</td>
			</tr>
			<!-- sapi -->

			<tr>
				<th colspan="2" class="header"><strong>Kuda</strong></th>
			</tr>
			<tr>
				<th class="label_zakat">Nisab (EKOR)</th>
				<th class="value_zakat">Zakat Yang Wajib Dikeluarkan</th>
			</tr>
			<!-- kuda -->
			<tr>
				<td class="label_zakat">30-59</td>
				<td >1 Ekor Anak kuda< betina</td>
			</tr>
			<tr>
				<td class="label_zakat">60-69</td>
				<td >2 Ekor Anak kuda Jantan</td>
			</tr>
			<tr>
				<td class="label_zakat">70-79</td>
				<td >1 Ekor Anak kuda Betina dan 1 ekor anak kuda jantan</td>
			</tr>
			<tr>
				<td class="label_zakat">80-89</td>
				<td >2 Ekor Anak kuda Betina</td>
			</tr>
			<tr>
				<td class="label_zakat">90-99</td>
				<td >3 Ekor Anak kuda Jantan</td>
			</tr>
			<tr>
				<td class="label_zakat">100-109</td>
				<td >1 ekor anak kuda betina dan 2 ekor anak kuda</td>
			</tr>
			<tr>
				<td class="label_zakat">110-119</td>
				<td >2 Ekor Anak kuda Jantan</td>
			</tr>
			<tr>
				<td class="label_zakat">> 120</td>
				<td >3 ekor anak kuda betina atau 3 ekor anak kuda jantan</td>
			</tr>
			<!-- kuda -->

			<tr>
				<th colspan="2" class="header"><strong>Kuda</strong></th>
			</tr>
			<tr>
				<th class="label_zakat">Nisab (EKOR)</th>
				<th class="value_zakat">Zakat Yang Wajib Dikeluarkan</th>
			</tr>
			<!-- Kambing -->
			<tr>
				<td class="label_zakat">5-9</td>
				<td >1 Ekor Anak kambing</td>
			</tr>
			<tr>
				<td class="label_zakat">10-14</td>
				<td >2 Ekor Anak kambing</td>
			</tr>
			<tr>
				<td class="label_zakat">15-19</td>
				<td >3 Ekor Anak kambing</td>
			</tr>
			<tr>
				<td class="label_zakat">20-24</td>
				<td >4 Ekor Anak kambing</td>
			</tr>
			<!-- kambing -->
			<tr>
				<th colspan="2" class="header" id="keterangan">&nbsp;</th>
			</tr>
		</tbody>
	</table>
</div>



<script>
function Unta(data){
	var data = parseInt(data.value);
	// alert(data);
	if (data >= 25) {
		console.log(data);
		if (data >= 25  && data <= 35) {
			$("#hasil_unta").html("1 Ekor anak Unta betina (umur >1 tahun)");
		}
		else if(data >= 36 && data <= 45){
			$("#hasil_unta").html("2 Ekor anak Unta betina (umur >2 tahun)");
		}
		else if (data >= 46 && data <= 60) {
			$("#hasil_unta").html("3 Ekor anak Unta betina (umur >3 tahun)");
		}
		else if (data >= 61 && data <= 75) {
			$("#hasil_unta").html("3 Ekor anak Unta betina (umur >4 tahun)");
		}
		else if (data >= 76 && data <= 90) {
			$("#hasil_unta").html("2 Ekor anak Unta betina (umur >2 tahun)");
		}
		else if (data >= 91 && data <= 120) {
			$("#hasil_unta").html("2 Ekor anak Unta betina (umur >3 tahun)");
		}
		else if (data >= 121 && data <= 129) {
			$("#hasil_unta").html("3 Ekor anak Unta betina (umur >2 tahun)");
		}
		else if (data >= 130 && data <= 139) {
			$("#hasil_unta").html("1 Ekor anak Unta betina (umur >3 tahun)<br>"+
				 "dan 1 ekor anak unta Betina ( >2 tahun)");
		}
		else if (data >= 140 && data <= 149) {
			$("#hasil_unta").html("2 Ekor anak Unta betina (umur >3 tahun)<br>"+
				 "dan 1 ekor anak unta Betina ( >2 tahun)");
		}
		else if (data >= 150 && data <= 159) {
			$("#hasil_unta").html("3 Ekor anak Unta betina (umur >3 tahun)");
		}
		else if (data >= 160 && data <= 169) {
			$("#hasil_unta").html("4 Ekor anak Unta betina (umur >2 tahun)");
		}
		else if (data >= 170 && data <= 179) {
			$("#hasil_unta").html("3 Ekor anak Unta betina (umur >2 tahun)<br>"+
				 "dan 1 ekor anak unta Betina ( >3 tahun)");
		}
		else if (data >= 180 && data <= 189) {
			$("#hasil_unta").html("2 Ekor anak Unta betina (umur >2 tahun)<br>"+
				 "dan 2 ekor anak unta Betina ( >2 tahun)");
		}
		else if (data >= 190 && data <= 199) {
			$("#hasil_unta").html("3 Ekor anak Unta betina (umur >3 tahun)<br>"+
				 +"dan 1 ekor anak unta Betina ( >2 tahun)");
		}
		else if (data >= 200 && data <= 209) {
			$("#hasil_unta").html("4 Ekor anak Unta betina (umur >3 tahun)<br>"+
				 "dan 5 ekor anak unta Betina ( >2 tahun");
		}

		else{
			$("#hasil_unta").html("4 Ekor anak Unta betina (umur >3 tahun)<br>"+
				 "dan 5 ekor anak unta Betina ( >2 tahun");
		}
		// $("#hasil_unta").html(data);
	}else{
		console.log(data + ' gagal');
		$("#hasil_unta").html("");
	}
}

function Sapi(data){
	var data = parseInt(data.value);
	if (data >= 30 ) {
		if (data >= 30  && data <= 59) {
			$("#hasil_sapi").html("1 Ekor Anak Sapi betina");
		}
		else if (data >= 60 && data <= 69){
			$("#hasil_sapi").html("2 Ekor Anak Sapi Jantan");
		}
		else if (data >= 70 && data <= 79){
			$("#hasil_sapi").html("1 Ekor Anak Sapi Betina dan 1 ekor anak sapi jantan");
		}
		else if (data >= 80 && data <= 89){
			$("#hasil_sapi").html("2 Ekor Anak Sapi Betina");
		}
		else if (data >= 90 && data <= 99){
			$("#hasil_sapi").html("3 Ekor Anak Sapi Jantan");
		}
		else if (data >= 100 && data <= 109){
			$("#hasil_sapi").html("1 ekor anak sapi betina dan 2 ekor anak sapi");
		}
		else if (data >= 110 && data <= 119){
			$("#hasil_sapi").html("2 Ekor Anak Sapi Jantan");
		}else{
			$("#hasil_sapi").html("3 ekor anak sapi betina atau 3 ekor anak sapi jantan");
		}

	}else{
		$("#hasil_sapi").html("");
	}
}

function Kuda(data){
	var data = parseInt(data.value);
	if (data >= 30 ) {
		if (data >= 30  && data <= 59) {
			$("#hasil_kuda").html("1 Ekor Anak kuda betina");
		}
		else if (data >= 60 && data <= 69){
			$("#hasil_kuda").html("2 Ekor Anak kuda Jantan");
		}
		else if (data >= 70 && data <= 79){
			$("#hasil_kuda").html("1 Ekor Anak kuda Betina dan 1 ekor anak kuda jantan");
		}
		else if (data >= 80 && data <= 89){
			$("#hasil_kuda").html("2 Ekor Anak kuda Betina");
		}
		else if (data >= 90 && data <= 99){
			$("#hasil_kuda").html("3 Ekor Anak kuda Jantan");
		}
		else if (data >= 100 && data <= 109){
			$("#hasil_sapi").html("1 ekor anak kuda betina dan 2 ekor anak kuda");
		}
		else if (data >= 110 && data <= 119){
			$("#hasil_kuda").html("2 Ekor Anak kuda Jantan");
		}else{
			$("#hasil_kuda").html("3 ekor anak kuda betina atau 3 ekor anak kuda jantan");
		}

	}else{
		$("#hasil_kuda").html("");
	}


	
}


	function Kambing(data){
		var data = parseInt(data.value);
		if (data >= 5) {
			if (data >= 5  && data <= 9) {
				$("#hasil_kambing").html("1 ekor anak kambing");
			}
			else if (data >= 10  && data <= 14) {
				$("#hasil_kambing").html("2 ekor anak kambing");
			}
			else if (data >= 15  && data <= 19) {
				$("#hasil_kambing").html("3 ekor anak kambing");
			}else{
				$("#hasil_kambing").html("4 ekor anak kambing");
			}
		}else{
			$("#hasil_kambing").html("");
		}
	}
	

	$(function() {
		$(".harga").maskMoney({ allowNegative: true, thousands:',', affixesStay: false});
	});
</script>