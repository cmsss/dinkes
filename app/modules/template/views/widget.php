<div class="panel panel-dafault">
    <div class="panel-heading text-center">P R O F I L &nbsp; P E J A B A T</div>
      <div class="panel-body panel-widget">
           <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
              <div class="carousel-inner" role="listbox">
              <?php  $slider=widget('m_profilpejabat','get_profil');
                    $activeid=widget('m_profilpejabat','get_max_id'); ?>
              <?php
                  if (!empty($slider)) {

                      foreach ($slider as $s) {?>
                         <div <?php if ($s->id_profil_pejabat === $activeid) {
                             echo 'class="item active"';
                         }else{
                          echo 'class="item"';
                          } ?>> 
                          <center><img src="<?php if ($s->img != '') {
                            echo base_url('uploads/'.$s->img);
                          }else{
                            echo base_url('uploads/foto/no_img.png');
                            } ?>" alt="<?php echo $s->nama; ?>" class="img-responsive" style="width: 100%; height: 200px;"/></center>

                          <div class="carousel-caption" style="font-size: 11pt; padding: 2px;">
                              <a href="<?php echo site_url('profil-pejabat/' . $s->devisi) ?>" style="text-decoration: none; color: #fff; font-size: 8pt;"><?php echo $s->nama; ?></a><br>

                              <font style="font-size: 8pt;"><?php echo ucwords($s->jabatan); ?></font>

                          </div>
                      </div>
                      <?php }
                      } ?>
                </div>     
            </div>
      </div>
</div>

<div class="panel panel-dafault">
    <div class="panel-heading text-center">A G E N D A</div>
      <div class="panel-body panel-widget">
          <div class="agenda">
              <div class="list-group">
                  <?php $agenda = widget('m_agenda','get_agenda');
                      if (!empty($agenda)){
                          foreach ($agenda as $a) { ?>
                              <a href="<?= site_url('detail-agenda/'.$a->flag) ?>" class="list-group-item"><?= $a->tema; ?></a>          
                          <?php }
                      }
                  ?>                                                    
            </div>
          </div>
    </div>
</div>
<div class="panel panel-dafault">
    <div class="panel-heading">G A L E R Y &nbsp; F O T O</div>
      <div class="panel-body panel-widget">
          <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
            <?php $slide=widget('m_galery','get_slider');
                  $id=widget('m_galery','get_max_id'); ?>

                  <?php if (!empty($slide)) {
                    foreach ($slide as $s) {?>
              <div <?php if ($s->id_galery===$id) {
               echo 'class="item active"';
              }else{
                 echo 'class="item"';
                } ?>>
                <center><img src="<?php echo base_url('uploads/'.$s->file); ?>" style="width: 100%; height: 200px;" class="img-responsive" /> </center>
                <div class="carousel-caption" style="font-size: 11pt;">
                <?php echo $s->judul; ?>
                </div>
              </div>

              <?php  }
                  } ?>
            
            </div>
          </div>
    </div>
</div>
<div class="panel panel-dafault">
    <div class="panel-heading text-center"> G A L E R Y &nbsp; V I D E O</div>
      <div class="panel-body panel-widget">
          <?php  $v = widget('m_galery','get_sliderv'); ?>
           <?php if (!empty($v)) {
              foreach ($v as $vi) {?>
          <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="<?php echo $vi->url; ?>" style="width: 100%;"></iframe>
            </div>
            <?php }
            } ?>
    </div>
</div>

<div class="panel panel-dafault">
  <div class="panel-heading text-center">S P A C E  &nbsp; I K L A N</div>
    <div class="panel-body panel-widget">
        <?php $iklan=query_sql("select * from t_iklan where posisi='space-iklan'"); ?>
        <?php  if (!empty($iklan)) {
           foreach ($iklan as $i) {?>
              <img src="<?php echo base_url('uploads/'.$i->file); ?>" style="width:100%; height:100px; border: 1px solid #ccc;" class="img-responsive">
          <?php }
        }?>
        
    </div>
</div>
