<?php $kategori=array(
	''=>'Pilih Kategori',
	'STRUKTUR'=>'STRUKTUR',
	'VISI DAN MISI'=>'VISI DAN MISI'
); ?>
<div class="container-fluid">
  <section class="content-header">
    <h4>Tambah Penataan Ruang</h4>
	  </section>
	  <div class="box box-primary">
	    <section class="content">
			<?php echo form_open_multipart('',array('class'=>'form-horizontal')); ?>
      <div class="row form-group">
          <label class="control-label col-sm-2">Kaetegori</label>
          <div class="col-sm-10">
            <?php echo form_dropdown('kategori',$kategori,'','class="form-control" placeholder="Kaetegori" required '); ?>
          </div>
      </div>
				<div class="row form-group">
						<label class="control-label col-sm-2">Deskripsi</label>
						<div class="col-sm-10">
							<textarea name="isi" class="ckeditor" rows="8" cols="40"></textarea>
						</div>
				</div>
				<div class="row form-group">
						<label class="control-label col-sm-2">Gambar</label>
						<div class="col-sm-10">
							<input type="file" class="form-control" name="img" required="required" />
						</div>
						<?= $this->session->flashdata('pesan') ?>
				</div>
				<div class="row form-group">

						<div class="col-sm-10">
						<a href="<?= site_url('adminweb/struktur.asp') ?>" class="btn btn-default">Kembali</a>
							<?php echo form_submit('submit','Tambah Profil','class="btn btn-primary"'); ?>
						</div>
				</div>

			<?php echo  form_close(); ?>
		</section>
	</div>
</div>
