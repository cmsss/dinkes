  <section class="content-header">
    <h1></i>Agenda</h1>
  </section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
            <h3 class="box-title">List Agenda</h3>
            <div class="pull-right">
                <a href="<?php echo site_url('adminweb/agenda/post.asp'); ?>" class="btn btn-info">Tambah</a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body">
			<?php echo form_open(); ?>

			<table class="table table-hover table-condensed" id="list_kategori">
				<thead>
					<tr>
						<td>No</td>
						<td>Tema</td>
						<td>Acara</td>
						<td>Tempat</td>
						<td>Tanggal</td>
						<td>Waktu</td>
						<td>Status</td>
						<td>Flag</td>
						<td>Aksi</td>
					</tr>
				</thead>
				<tbody>

					<?php
						$no=1;
						$s = array(
							'0' => 'Belum Selesai',
							'1' => 'Selesai',
							);
						foreach ($list as $row) {?>
					<tr>
						<td><?php echo $no++ ?></td>
						<td><?php echo $row->tema; ?></td>
						<td><?php echo $row->acara; ?></td>
						<td><?php echo $row->tempat; ?></td>
						<td><?php echo $row->tgl; ?></td>
						<td><?php echo $row->waktu; ?></td>
						<td><?php echo $s[$row->status]; ?></td>
						<td><?php echo $row->flag; ?></td>
						<td>
							<a href="<?php echo site_url('adminweb/agenda/update/'.$row->id_agenda); ?>" class="btn btn-info"><i class="fa fa-edit"></i></a>
							<a href="<?php echo site_url('adminweb/agenda/delete/'.$row->id_agenda); ?>" onclick="return confirm('anda yakin untuk menghapus data ini');" class="btn btn-warning"><i class="fa fa-trash"></i></a>
						</td>
					</tr>
					<?php } ?>

				</tbody>
			</table>
			<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
</section>

