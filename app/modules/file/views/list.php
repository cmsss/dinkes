  <section class="content-header">
    <h1>File</h1>
  </section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
            <h3 class="box-title">List File</h3>
            <div class="pull-right">
                <a href="<?php echo site_url('adminweb/file/post.asp'); ?>" class="btn btn-info">File</a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body">
			<?php echo form_open(); ?>
			<div class="table-responsive">
				<table class="table table-hover table-condensed" id="list_kategori">
					<thead>
						<tr>
							<th>No</th>
							<th>Nama</th>
							<th>Direktori</th>
							<th>Ukuran</th>
							<th>Tanggal</th>
							<th>Type</th>
							<th>Copy Link</th>
							<th>aksi</th>
						</tr>
					</thead>
					<tbody>
					
						<?php 
							$no=1;
							foreach ($list as $row) {?>
						<tr>
							<td><?php echo $no++; ?></td>
							<td><?php echo $row->nama; ?></td>
							<td><?php echo $row->dir; ?></td>
							<td><?php echo $row->ukuran; ?></td>
							<td><?php echo $row->tanggal; ?></td>
							<td><?php echo $row->mime; ?></td>
							<td>
							<button type="button" class="copyyy btn btn-primary btn-sm" data-klik='<?= base_url('files/gorontalo/file/file/'.$row->dir.'/'.$row->asli) ?>'>Copy URL</button>

								<button type="button" class="copyyy btn btn-info btn-sm" data-klik='<a href="<?= base_url('files/gorontalo/file/file/'.$row->dir.'/'.$row->asli) ?>"> <?php echo  $row->nama;?></a>'>Copy LINK</button>



								<a  class="btn btn-default btn-sm" target=" _blank" href="<?= site_url('files/gorontalo/file/file/'.$row->dir.'/'.$row->asli); ?>">Lihat  <i class="fa fa-fw fa-eye"></i></a>

							</td>
							
							<td>
								<a href="<?php echo site_url('adminweb/file/delete/'.$row->id); ?>" onclick="return confirm('anda yakin untuk menghapus data ini');" class="btn btn-warning btn-sm"><i class="fa fa-trash"></i></a>
							</td>
						</tr>
						<?php } ?>
						
					</tbody>
				</table>
			</div>
			
			<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
</section>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>


    <script>

    jQuery(document).ready(function() {
        $('.copyyy').click(function() {
            var data = $(this).data('klik');
            var $temp = $("<input>");
            var tes = $("body").append($temp);
            var tes2 = $temp.val(data).select();
            document.execCommand("copy");
            $temp.remove();
            $.toaster({priority: 'info', title: 'Update', message: 'Berhail Copy'});

        });    
    });

 </script>