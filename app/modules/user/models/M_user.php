<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_user extends MY_Model {
    
    public function __construct(){
        parent::__construct();
        parent::set_table('t_user','id_user');
    }
    
    public function get_by_username($username) {
        if($query = parent::get_by(array('username'=>$username))){
            if (!empty($query)) return $query;
        }
        return false;
    }
    
    public function cek_username($username,$self = FALSE){
        if($data = parent::get_by(array('username'=>$username))){
            if($self){
                return $data->id_user;
            }else{
                return false;   
            }
        }
        return true;
    }
    public function hapus($id) {
        if(parent::delete($id)) {
            return true;
        }
        return false;
    }
    
}
?>