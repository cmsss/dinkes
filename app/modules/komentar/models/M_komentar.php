<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_komentar extends MY_Model {
    
    public function __construct(){
        parent::__construct();
        parent::set_table('t_komentar','id_komentar');
    }
    public function get_jml_komen($id){
        $this->db->where(array('id_berita'=>$id));
        $this->db->from('t_komentar');
        return $this->db->count_all_results();
    }
    public function komentar(){
        $this->db->join('t_berita a','a.id_berita = t_komentar.id_berita');
        $this->db->where(array('t_komentar.status'=>'0'));
        return parent::get_all();
    }
    public function get_komen(){
        $this->db->limit('5');
        $this->db->join('tbl_berita a','a.id_berita = tbl_komentar.id_berita');
        return parent::get_all();
    }
    public function countstatus0()
    {
        $data1 = $this->db->query("SELECT SQL_CALC_FOUND_ROWS * FROM t_komentar where status='0'");
      return $data1->num_rows();
    }

    public function get_coment_all(){
        $this->db->join('t_berita a','a.id_berita = t_komentar.id_berita');
        return parent::get_all();
    }
}
?>