<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Iklan extends MX_Controller {

    

    public function __construct() {
        parent::__construct();
        if (!$this->autentifikasi->sudah_login())
            redirect('adminpage/site-login.asp','refresh');
        $this->load->model('m_iklan');
    }

    public function index() {
    	$data['list'] = $this->m_iklan->get_all();
        $data['module'] = "iklan";
        $data['view_file'] = "list";
        echo Modules::run('template/render_master',$data);
    }
    public function add(){
        if ($this->input->post()) {
            $config['upload_path']="./uploads/";
            $config['allowed_types']="jpg|png";
            $config['max_size']="1024";
            $config['max_width']="2000";
            $config['max_height']="1000";
            $config['encrypt_name']= TRUE;
            $this->upload->initialize($config);;
            $idincrement = auto_inc('m_iklan','id_iklan');
            if ($this->upload->do_upload('file')) {
                 $data= array(
                'id_iklan'=> $idincrement,
               'iklan'=>$this->input->post('iklan'),
                'file'=>$this->upload->file_name,
                'url'=>url_title($this->input->post('iklan'),'dash',TRUE).'.html',
                // 'posisi' => $this->input->post('posisi')
                );
            $this->m_iklan->insert($data);
            redirect('adminweb/iklan.asp','refresh');
            }
            
        }
        $list_posisi[''] = "Pilih Posisi";
        $list_posisi['visi-mis'] = "Visi Misi";
        $list_posisi['space-iklan'] = "Space Iklan";
        $data['posisi'] = $list_posisi;
        $data['module']="iklan";
        $data['view_file']="add_iklan";
        echo Modules::run('template/render_master',$data);
    }
    public function edit($id){
         if ($cek = $this->m_iklan->get_by(array('id_iklan'=>$id))){
            $nama_file = "file_". time();
            $config['upload_path']="./uploads/";
            $config['allowed_types']="jpg|png";
            $config['max_size']="1024";
            $config['max_width']="2000";
            $config['max_height']="1000";
            $config['encrypt_name']= TRUE;
            $this->upload->initialize($config);
            if ($this->input->post()) {
                if ($this->upload->do_upload('file')) {
                    $pathgambar = realpath(APPPATH . '../uploads/');
                    if ($cek->file != '') {
                        unlink($pathgambar . '/' . $cek->file);
                    }
                         $data= array(
                           'iklan'=>$this->input->post('iklan'),
                            'file'=>$this->upload->file_name,
                            'url'=>url_title($this->input->post('iklan'),'dash',TRUE).'.html',
                            // 'posisi' => $this->input->post('posisi')
                            );
                        $this->m_iklan->update($id,$data);
                        redirect('adminweb/iklan.asp','refresh');
                    }else{
                        $data= array(
                           'iklan'=>$this->input->post('iklan'),
                            'url'=>url_title($this->input->post('iklan'),'dash',TRUE).'.html',
                            // 'posisi' => $this->input->post('posisi')
                            );
                        $this->m_iklan->update($id,$data);
                        redirect('adminweb/iklan.asp','refresh');
                    }
            }
        }
        $list_posisi[''] = "Pilih Posisi";
        $list_posisi['visi-mis'] = "Visi Misi";
        $list_posisi['space-iklan'] = "Space Iklan";
        $data['posisi'] = $list_posisi;
        $data['edit']=$this->m_iklan->get_by(array('id_iklan'=>$id));
        $data['module']="iklan";
        $data['view_file']="edit_iklan";
        echo Modules::run('template/render_master',$data);
    

    }
    public function hapus($id){
        if ($cek = $this->m_iklan->get_by(array('id_iklan'=>$id))){
            $pathgambar = realpath(APPPATH . '../uploads/');
            if ($cek->file != '') {
                unlink($pathgambar . '/' . $cek->file);
            }
            $this->m_iklan->delete($id);
        }
        redirect('adminweb/iklan.asp');
    }

    

}
