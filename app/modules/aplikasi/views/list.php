<section class="content-header">
  <h1>Aplikasi</h1>
</section>
<section class="content">
  <?php echo form_open(); ?>
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
            <h3 class="box-title">List Aplikasi</h3>
            <div class="pull-right">
              <a href="<?php echo site_url('adminweb/apliksai/post.asp'); ?>" class="btn btn-primary btn-sm">Tambah Aplikasi</a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body">
          <div class="table-responsive">
            <table class="table table-hover table-condensed" id="list_kategori">
              <thead>
                <tr>
                  <td>No</td>
                  <td>Nama Aplikasi</td>
                  <td>Gambar</td>
                  <td>URL</td>
                  <td>Aksi</td>
                </tr>
              </thead>
              <tbody>
                <?php
                $no  = 1;
                foreach ($list as $row) {?>
                <tr>
                  <td><?php echo $no++;   ?></td>
                  <td><?php echo $row->nama; ?></td>
                  <td><img src="<?php echo base_url('uploads/aplikasi/'.$row->img); ?>" height="70px" width="70px" ></td>
                  <td><?php echo $row->link; ?></td>
                  <td>
                    <a href="<?php echo site_url('adminweb/aplikasi/update/'.$row->id_aplikasi); ?>" class="btn btn-info"><i class="fa fa-edit"></i></a>
                    <a href="<?php echo site_url('adminweb/aplikasi/delete/'.$row->id_aplikasi); ?>" onclick="return confirm('anda yakin untuk menghapus data ini');" class="btn btn-sm btn-warning"><i class="fa fa-trash"></i></a>
                  </td>
                </tr>
                <?php } ?>

              </tbody>
            </table>
          </div>
          
        </div>
      </div>
    </div>
  </div>
</section>
