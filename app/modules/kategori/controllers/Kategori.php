<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Kategori extends MX_Controller {

    
    public function __construct() {
        parent::__construct();
        if (!$this->autentifikasi->sudah_login())
            redirect('adminpage/site-login.asp','refresh');
        $this->load->model('m_kategori1');
    }

    public function index() {
        $idincrement = auto_inc('m_kategori1','id');
        if ($this->input->post()) {
            $param = $this->input->post('nama');
      

            $judul_sterlir = str_replace(array('.' ,',' ,'(' , ')' ,'{' ,'}','?' ,'!' , '=' , '/'), '', $param);
            $judul_sterlir2 = str_replace(' ', '', $judul_sterlir);
            // echo $judul_sterlir2;
            if (!$this->m_kategori1->get_by(array('id' => $judul_sterlir2))) {
                $data= array(
                    'id'=> $judul_sterlir2,
                    'nama'=>$param,
                    'domain' => NAMA_DOMAIN,
                    );
                    $this->m_kategori1->insert($data);
                    redirect('adminweb/kategori.asp','refresh');
            }else{
                redirect('adminweb/kategori.asp','refresh');
            }
            
        }
    	$data['list'] = $this->m_kategori1->get_all();
        $data['module'] = "kategori";
        $data['view_file'] = "list";
        echo Modules::run('template/render_master',$data);
    }
    // public function add(){
    //     $idincrement = auto_inc('m_kategori1','id');
    //     if ($this->input->post()) {
    //              $data= array(
    //             'id'=> $idincrement,
    //            'kategori'=>$this->input->post('kategori'),
    //            'flag'=>url_title($this->input->post('kategori'),'dash',TRUE),
    //            'menu' => $this->input->post('menu')
    //             );
    //         $this->m_kategori1->insert($data);
    //         redirect('adminweb/kategori.asp','refresh');
            
            
    //     }
    //     $data['module']="kategori";
    //     $data['view_file']="add_kategori";
    //     echo Modules::run('template/render_master',$data);
    // }
    public function edit(){
      if ($this->input->post()) {
        $data = array(
          'nama'=> $this->input->post('nama'),
        );
        if ($this->m_kategori1->update($this->input->post('id'),$data)) {
            redirect('adminweb/kategori.asp');
        }
      }else{
        redirect('adminweb/kategori.asp');
      }
    }
    public function hapus($id){
        $this->m_kategori1->delete($id);
        redirect('adminweb/kategori.asp');
    }
}

