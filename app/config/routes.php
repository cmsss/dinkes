<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$route['default_controller'] = "template/frontend";
$route['404_override'] = 'front/not_found';

#route admin
$route['adminpage/site-login.asp'] = "login/log_admin";

#route admin

$route['adminpage/site-login.asp'] = "login/log_admin";
$route['adminweb/logout.asp'] = "login/logout";
$route['adminweb/dashboard.asp'] = "admin";
$route['adminweb/user/profile.asp'] = "profile";

$route['adminweb/produk-kemenag/dokumen.asp'] = "dokumen";
$route['adminweb/produk-kemenag/dokumen/add.asp'] = "dokumen/add";
$route['adminweb/produk-kemenag/dokumen/edit/(:num)'] = "dokumen/edit/$1";
$route['adminweb/produk-kemenag/dokumen/hapus/(:num)'] = "dokumen/delete/$1";

$route['adminweb/produk-kemenag/kategori.asp'] = "dokumen/kategori";
$route['adminweb/produk-kemenag/kategori/hapus/(:any)'] = "dokumen/kategori/delete/$1";
$route['adminweb/produk-kemenag/kategori/edit.asp'] = "dokumen/kategori/update";

$route['adminweb/produk-kemenag/subkategori.asp'] = "dokumen/subkategori";
$route['adminweb/produk-kemenag/subkategori/hapus/(:any)'] = "dokumen/subkategori/delete/$1";
$route['adminweb/produk-kemenag/subkategori/edit.asp'] = "dokumen/subkategori/update";

#route aplikasi
$route['adminweb/aplikasi.asp']="aplikasi";
$route['adminweb/apliksai/post.asp']="aplikasi/add";
$route['adminweb/aplikasi/update/(:any)']="aplikasi/edit/$1";
$route['adminweb/aplikasi/delete/(:any)']="aplikasi/hapus/$1";

 #route agenda
 // $route['adminweb/agenda.asp']='agenda';
 $route['adminweb/agenda.asp']='agenda/data';
$route['adminweb/agenda/post.asp']='agenda/add';
$route['adminweb/agenda/update/(:num)']='agenda/edit/$1';
$route['adminweb/agenda/delete/(:num)']='agenda/hapus/$1';

#route user
$route['adminweb/user.asp'] = "user";
$route['adminweb/user/add.asp'] = "user/add";
// $route['adminweb/user/edit/(:num)'] = "user/edit/$1";
$route['adminweb/user/hapus/(:any)'] = "user/delete/$1";
$route['adminweb/user/status/(:any)/(:any)'] = "user/status/$1/$2";
$route['adminweb/user/edit.asp']="user/edit";

#route level user
$route['adminweb/level-user.asp'] = "user/level";
$route['adminweb/level-user/hapus/(:any)'] = "user/delete_level/$1";
$route['adminweb/level-user/edit.asp'] = "user/edit_level";

// #route menu
// $route['adminweb/menu.asp'] = "menu";
// $route['adminweb/menu/edit.asp'] = "menu/edit";
// // sub 1
// $route['adminweb/menu/sub-menu/(:any)'] = "menu/sub1/$1";
// // sub 1

// $route['adminweb/menu/hapus/(:num)'] = "menu/delete/$1";

// // sub 1 menu
// $route['adminweb/menu/tambah1edit.asp'] = "menu/tambah1edit";

$route['adminweb/menu.asp'] = "menu";
$route['cpanel/menu/vtambah'] = "menu/vtambah";
$route['cpanel/menu/simpan'] = "menu/simpan";
$route['cpanel/menu/vupdate'] = "menu/vupdate";
$route['cpanel/menu/update'] = "menu/update";
$route['cpanel/menu/hapus'] = "menu/hapus";
$route['cpanel/menu/update-status'] = "menu/updatestatus";
$route['cpanel/menu/list-sub2'] = "menu/listSub2";
$route['cpanel/menu/vtambah-sub-2'] = "menu/vtambahSub2";
$route['cpanel/menu/simpan-sub-2'] = "menu/simpanSub2";
$route['cpanel/menu/vupdate-sub-2'] = "menu/vupdateSub2";
$route['cpanel/menu/update-sub2'] = "menu/updateSub2";

$route['cpanel/menu/list-sub3'] = "menu/listSub3";
$route['cpanel/menu/vtambah-sub-3'] = "menu/vtambahSub3";
$route['cpanel/menu/simpan-sub-3'] = "menu/simpanSub3";
$route['cpanel/menu/vupdate-sub-3'] = "menu/vupdateSub3";
$route['cpanel/menu/update-sub3'] = "menu/updateSub3";

#route berita
 // $route['adminweb/berita.asp']='berita';
 // $route['adminweb/berita/post.asp']='berita/add';
 // $route['adminweb/berita/update/(:num)']='berita/edit/$1';
 // $route['adminweb/berita/delete/(:num)']='berita/hapus/$1';


 #route album
$route['adminweb/album.asp']='album';
$route['adminweb/album/post.asp']='album/add';
$route['adminweb/album/update/(:num)']='album/edit/$1';
$route['adminweb/album/delete/(:num)']='album/hapus/$1';


 #route galery
$route['adminweb/galery.asp']='galery';
$route['adminweb/galery/post.asp']='galery/add';
$route['adminweb/galery/update/(:num)']='galery/edit/$1';
$route['adminweb/galery/delete/(:num)']='galery/hapus/$1';

#route iklan
$route['adminweb/iklan.asp']='iklan';
$route['adminweb/iklan/post.asp']='iklan/add';
$route['adminweb/iklan/update/(:num)']='iklan/edit/$1';
$route['adminweb/iklan/delete/(:num)']='iklan/hapus/$1';

#route komentar
$route['adminweb/komentar.asp']='komentar/list_komentar';
$route['admin/komentar/delete/(:num)']='komentar/hapus/$1';

#route kategori
$route['adminweb/kategori.asp']='kategori';
$route['adminweb/kategori/post.asp']='kategori/add';
$route['adminweb/kategori/edit']='kategori/edit';
$route['adminweb/kategori/delete/(:any)']='kategori/hapus/$1';

#route runningtext
$route['adminweb/runningtext.asp']='runningtext';

#route linkterkait
$route['adminweb/linkterkait.asp']='linkterkait';
$route['adminweb/linkterkait/post.asp']='linkterkait/add';
$route['adminweb/linkterkait/update/(:num)']='linkterkait/edit/$1';
$route['adminweb/linkterkait/delete/(:num)']='linkterkait/hapus/$1';

#route front
$route['komentar.asp'] = "front/komentar";
$route['agenda'] = "front/agenda";
$route['agenda/(:any)'] = "front/agenda/$1";
$route['detail-agenda/(:any)'] = "front/detail_agenda/$1";
// $route['artikel/(:any)'] = "front/detail_berita/$1";
$route['produk-hukum/(:any)'] = "front/produk_hukum/$1";
$route['dokumen-perencanaan/(:any)'] = "front/dokumen_perencanaan/$1";
$route['dokumen-perencanaan/(:any)/(:any)'] = "front/dokumen_perencanaan/$1/$2";
$route['dokumen-bappeda/(:any)'] = "front/dokumen_bappeda/$1";
$route['penataan-ruang/(:any)'] = "front/penataan_ruang/$1";
$route['galery/(:any)'] = "front/galeri/$1";
$route['kategori/(:any)'] = "front/berita/$1";
$route['public/(:any)'] = "front/get_dokumen/$1";
$route['public/(:any)/(:any)'] = "front/get_dokumen/$1/$2";


#route profil struktur
$route['adminweb/struktur.asp']='profil/struktur';
$route['adminweb/profil/struktur/update/(:num)']='profil/edit/$1';
#route profil visi_misi
$route['adminweb/visi_misi.asp']='profil/visi_misi';
$route['adminweb/profil/visi_misi/update/(:num)']='profil/edit_visimisi/$1';

#route profil profilpejabat
$route['adminweb/profilpejabat.asp']='profilpejabat';
$route['adminweb/profilpejabat/post.asp']='profilpejabat/add';
$route['adminweb/profilpejabat/update/(:num)']='profilpejabat/edit/$1';
$route['adminweb/profilpejabat/delete/(:num)']='profilpejabat/hapus/$1';

#route jejak pendapat
$route['adminweb/jejakpendapat.asp']='jejakpendapat';

// #route front profil dan profilpejabat
// $route['profil/(:any)']='front/profil/$1';
// $route['profil-pejabat']='front/profilpejabat';

#route front profil pejapat detail
$route['profil-pejabat/(:any)']='front/profilpejabat_detail/$1';

#route jejak pendapat
$route['jejakpendapat.html']='jejakpendapat/jejak_pendapat';
$route['hasil-poling-jejak-pendapat.html']='front/data_jejakpedapat';

#bidang
$route['adminweb/bidang.asp'] = 'bidang';
$route['adminweb/bidang/post.asp'] = 'bidang/add';
$route['adminweb/bidang/delete/(:any)'] = 'bidang/delete/$1';
$route['adminweb/bidang/update/(:any)'] = 'bidang/update/$1';


$route['adminweb/seting.asp'] = 'seting';
$route['adminweb/seting/post.asp'] = 'seting/add';
$route['adminweb/seting/delete/(:any)'] = 'seting/delete/$1';




#rote galery foto dan video
$route['galery_bappeda/(:any)']='front/galery/$1';

#route search
$route['search'] = 'front/search';
/* End of file routes.php */
/* Location: ./application/config/routes.php */




// alfandy kemenag

$route['sitemap'] = 'kemenag/sitemap';

$route['cari-data'] = 'kemenag/cari_data';

$route['berita-kategori'] = 'kemenag/kategori_berita';

$route['rss'] = 'kemenag/rss';
$route['renungan'] = 'renungna-gorontalo';
$route['renungna-gorontalo'] = 'kemenag/data_renungan';
$route['renungna-gorontalo/(:any)'] = "kemenag/data_renungan/$1";
// $route['renungan/(:any)/(:any)'] = 'kemenag/detail_renungan/$1/$2';


$route['banner'] = 'banner-gorontalo';
$route['banner-gorontalo'] = 'kemenag/data_banner';
$route['banner-gorontalo/(:any)'] = "kemenag/data_banner/$1";

$route['majalah'] = 'majalah-gorontalo';
$route['majalah-gorontalo'] = 'kemenag/data_majalah';
$route['majalah-gorontalo/(:any)'] = "kemenag/data_majalah/$1";
// $route['majalah/(:any)/(:any)'] = 'kemenag/detail_majalah/$1/$2';

$route['berita'] = 'berita-gorontalo';
$route['berita-gorontalo'] = 'kemenag/data_berita';
$route['berita-gorontalo/(:any)'] = "kemenag/data_berita/$1";
$route['berita/(:any)/(:any)'] = 'kemenag/detail_berita/$1/$2';

$route['opini'] = 'opini-gorontalo';
$route['opini-gorontalo'] = 'kemenag/data_opini';
$route['opini-gorontalo/(:any)'] = "kemenag/data_opini/$1";
$route['opini/(:any)/(:any)'] = 'kemenag/detail_opini/$1/$2'; 

$route['artikel'] = 'artikel-gorontalo';
$route['artikel-gorontalo'] = 'kemenag/data_artikel';
$route['artikel-gorontalo/(:any)'] = "kemenag/data_artikel/$1";
$route['artikel/(:any)/(:any)'] = 'kemenag/detail_artikel/$1/$2';

$route['foto'] = 'foto-gorontalo';
$route['foto-gorontalo'] = 'kemenag/data_foto';
$route['foto-gorontalo/(:any)'] = "kemenag/data_foto/$1";

$route['video'] = 'vidoe-gorontalo';
$route['video-gorontalo'] = 'kemenag/data_video';
$route['video-gorontalo/(:any)'] = 'kemenag/data_video/$1';
$route['video/(:any)'] = 'kemenag/detail_video/$1';

$route['audio'] = 'audio-gorontalo';
$route['audio-gorontalo'] = 'kemenag/data_audio';
$route['audio-gorontalo/(:any)'] = 'kemenag/data_audio/$1';


$route['artikel-kemenag/(:any)'] = 'kemenag/detail_artikel/$1';
$route['berita-kemenag/(:any)'] = 'kemenag/frontend/$1';
$route['data-berita-kemenag']	= 'kemenag/data_berita_kemenag';

$route['profil/visi-misi']='kemenag/profil_visi_misi';
$route['profil/struktur']='kemenag/profil_struktur';
$route['profil-pejabat']='kemenag/profilpejabat';
$route['perhitungan/kalkulator-zakat'] = 'kemenag/kalkulator_zakat';

#route front profil pejapat detail
$route['profil-pejabat/(:any)']='kemenag/profilpejabat_detail/$1';


// artikel
// $route['adminweb/artikel.asp']		= 'artikel';
// $route['adminweb/artikel/post.asp'] = 'artikel/add';
// $route['adminweb/artikel/update/(:any)'] = 'artikel/edit/$1';
// $route['adminweb/artikel/delete/(:any)'] = 'artikel/hapus/$1';
// end artikel


// file
$route['adminweb/file.asp'] = 'file';
$route['adminweb/file/post.asp'] = 'file/tambah';
$route['adminweb/file/simpan']	= 'file/simpan';
$route['adminweb/file/delete/(:any)'] = 'file/hapus/$1';
// end file


//foto
$route['adminweb/foto.asp'] = 'foto';
$route['adminweb/foto/post.asp'] = 'foto/tambah';
$route['adminweb/foto/simpan'] 	= 'foto/simpan';
$route['adminweb/foto/edit/(:any)'] = 'foto/edit/$1';
$route['adminweb/foto/simpan-perubahan'] = 'foto/simpan_perubahan';
$route['adminweb/foto/delete/(:any)'] = 'foto/hapus/$1';
$route['adminweb/foto/tampila/(:any)'] = 'foto/tampil_a/$1';
$route['adminweb/foto/setujua/(:any)']	= 'foto/setuju_a/$1';
// foto


// video
$route['adminweb/video.asp'] = 'video';
$route['adminweb/video/post.asp'] = 'video/tambah';
$route['adminweb/video/simpan']	=  'video/simpan';
$route['adminweb/video/edit/(:any)'] = 'video/edit/$1';
$route['adminweb/video/tampila/(:any)'] = 'video/tampil_a/$1';
$route['adminweb/video/setujua/(:any)']	= 'video/setuju_a/$1';
$route['adminweb/video/simpan-perubahan'] = 'video/simpan_perubahan';
$route['adminweb/video/delete/(:any)'] = 'video/hapus/$1';
// video


// berita 
$route['adminweb/berita.asp'] = 'berita1';
$route['adminweb/berita/post.asp'] = 'berita1/tambah';
$route['adminweb/berita/simpan'] = 'berita1/simpan';
$route['adminweb/berita/edit/(:any)'] = 'berita1/edit/$1';
$route['adminweb/berita/simpan-perubahan'] = 'berita1/simpan_perubahan';
$route['adminweb/berita/delete/(:any)'] = 'berita1/hapus/$1';
$route['adminweb/berita/tampila/(:any)'] = 'berita1/tampil_a/$1';
$route['adminweb/berita/setujua/(:any)']	= 'berita1/setuju_a/$1';



// operator
$route['adminweb/berita/simpann1'] = 'berita1/simpan_opertor';
// berita

// berita 
$route['adminweb/opini.asp'] = 'opini';
$route['adminweb/opini/post.asp'] = 'opini/tambah';
$route['adminweb/opini/simpan'] = 'opini/simpan';
$route['adminweb/opini/edit/(:any)'] = 'opini/edit/$1';
$route['adminweb/opini/simpan-perubahan'] = 'opini/simpan_perubahan';
$route['adminweb/opini/delete/(:any)'] = 'opini/hapus/$1';
$route['adminweb/opini/tampila/(:any)'] = 'opini/tampil_a/$1';
$route['adminweb/opini/setujua/(:any)']	= 'opini/setuju_a/$1';
// berita


// audio
$route['adminweb/audio.asp'] = 'audio';
$route['adminweb/audio/post.asp'] = 'audio/tambah';
$route['adminweb/audio/simpan'] 	= 'audio/simpan';
$route['adminweb/audio/edit/(:any)'] = 'audio/edit/$1';
$route['adminweb/audio/simpan-perubahan'] = 'audio/simpan_perubahan';
$route['adminweb/audio/delete/(:any)'] = 'audio/hapus/$1';
$route['adminweb/audio/tampila/(:any)'] = 'audio/tampil_a/$1';
$route['adminweb/audio/setujua/(:any)']	= 'audio/setuju_a/$1';
// audio

// teksberjalan
$route['adminweb/teksberjalan.asp'] = 'teksberjalan';
$route['adminweb/teksberjalan/edit'] = 'teksberjalan/edit';
$route['adminweb/teksberjalan/tampila/(:any)'] = 'teksberjalan/tampil_a/$1';
$route['adminweb/teksberjalan/delete/(:any)'] = 'teksberjalan/hapus/$1';
// teksberjalan

// banner
$route['adminweb/banner.asp'] = 'banner';
$route['adminweb/banner/post.asp'] = 'banner/tambah';
$route['adminweb/banner/simpan'] = 'banner/simpan';
$route['adminweb/banner/tampila/(:any)'] = 'banner/tampil_a/$1';
$route['adminweb/banner/edit/(:any)'] = 'banner/edit/$1';
$route['adminweb/banner/simpan-perubahan'] = 'banner/simpan_perubahan';
$route['adminweb/banner/delete/(:any)'] = 'banner/hapus/$1';
// banner

// majalah
$route['adminweb/majalah.asp'] = 'majalah';
$route['adminweb/majalah/post.asp'] = 'majalah/tambah';
$route['adminweb/majalah/simpan'] = 'majalah/simpan';
$route['adminweb/majalah/tampila/(:any)'] = 'majalah/tampil_a/$1';
$route['adminweb/majalah/setujua/(:any)']	= 'majalah/setuju_a/$1';
$route['adminweb/majalah/edit/(:any)'] = 'majalah/edit/$1';
$route['adminweb/majalah/simpan-perubahan'] = 'majalah/simpan_perubahan';
$route['adminweb/majalah/delete/(:any)'] = 'majalah/hapus/$1';
// majalah

// renungan
$route['adminweb/renungan.asp'] = 'renungan';
$route['adminweb/renungan/tampila/(:any)'] = 'renungan/tampil_a/$1';
$route['adminweb/renungan/post.asp'] = 'renungan/tambah';
$route['adminweb/renungan/simpan'] = 'renungan/simpan';
$route['adminweb/renungan/edit/(:any)'] = 'renungan/edit/$1';
$route['adminweb/renungan/simpan-perubahan'] = 'renungan/simpan_perubahan';
$route['adminweb/renungan/delete/(:any)'] = 'renungan/hapus/$1';
// renungan

// artikel
$route['adminweb/artikel.asp'] = 'artikel1';
$route['adminweb/artikel/post.asp'] = 'artikel1/tambah';
$route['adminweb/artikel/simpan'] = 'artikel1/simpan';
$route['adminweb/artikel/edit/(:any)'] = 'artikel1/edit/$1';
$route['adminweb/artikel/simpan-perubahan'] = 'artikel1/simpan_perubahan';
$route['adminweb/artikel/delete/(:any)'] = 'artikel1/hapus/$1';
$route['adminweb/artikel/tampila/(:any)'] = 'artikel1/tampil_a/$1';
$route['adminweb/artikel/setujua/(:any)']	= 'artikel1/setuju_a/$1';
// berita
// artikel

// get sub kategory
$route['adminweb/get-sub-kat'] = 'dokumen/dokumen/get_sub_kategori';

// end alfandy Kemenag
