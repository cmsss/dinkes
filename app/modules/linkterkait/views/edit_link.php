<div class="container-fluid">
  <section class="content-header">
    <h4>Tambah Link</h4>
  </section>
  <div class="box box-primary">
    <section class="content">
      <?php echo form_open_multipart('',array('class'=>'form-horizontal')); ?>
        <div class="col-md-8">
          <div class="row form-group">
              <label class="control-label col-sm-2">Nama Link</label>
              <div class="col-sm-10">
                <?php echo form_input('nama',$edit->nama,'class="form-control"'); ?>
              </div>
          </div>
          <div class="row form-group">
              <label class="control-label col-sm-2">Link</label>
              <div class="col-sm-10">
                <?php echo form_input('link',$edit->link,'class="form-control"'); ?>
              </div>
          </div>
          <div class="row form-group">
              <label class="control-label col-sm-2">Gambar</label>
              <div class="col-sm-10">
                <input type="file" name="img" class="form-control" />
              </div>
          </div>
          <div class="row form-group">
              <div class="col-sm-12">
              <a href="<?= site_url('adminweb/linkterkait.asp') ?>" class="btn btn-default">Kembali</a>
                  <?php echo form_submit('submit','Update link','class="btn btn-primary"'); ?>
              </div>
          </div>
        </div>
      <?php echo form_close(); ?>
    </section>
  </div>
</div>
