<div class="container-fluid">
  <section class="content-header">
    <h4>Tambah Dokumen</h4>
	  </section>
	  <div class="box box-primary">
	    <section class="content">
			<?php echo form_open('',array('class'=>'form-horizontal')); ?>
				<div class="row form-group">
						<label class="control-label col-sm-2">Kategori</label>
						<div class="col-sm-5">
							<?php echo form_dropdown('kategori',$main_kat,'','class="form-control" placeholder="Kategori" id="mainkat" onchange="get_sub_kat(this);" required '); ?>
						</div>
						<div class="col-sm-5">
							<div id="sub">
								<?php echo form_dropdown('sub_kat','','','class="form-control" placeholder="Kategori"  required '); ?>
							</div>
						</div>
				</div>

				<div class="row form-group">
						<label class="control-label col-sm-2">Nama Dokumen</label>
						<div class="col-sm-10">
							<?php echo form_input('nama_dokumen',set_value('nama_dokumen'),'class="form-control" placeholder="Nama Dokumen" required '); ?>
						</div>
				</div>
				<div class="row form-group">
						<label class="control-label col-sm-2">Deskripsi</label>
						<div class="col-sm-10">
							<?= form_textarea('ket_dokumen',set_value('ket_dokumen'),'class="form-control" placeholder="Deskripsi" style="height:70px;"') ?>
						</div>
				</div>
				<div idd="link">
					<div class="row form-group">
						<label class="control-label col-sm-2">Link</label>
						<div class="col-sm-10">

							<?php echo form_input('link',set_value('link'),'onblur="closeFile(this)"  class="form-control" placeholder="LINK" required'); ?>
						</div>
					</div>
				</div>
				
				<div id="file">
					<div class="row form-group">
						<label class="control-label col-sm-2">File</label>
						<div class="col-sm-10">
							<input type="file" class="form-control" name="file"  />
						</div>
						<?= $this->session->flashdata('pesan') ?>
					</div>
				</div>
				
				<div class="row form-group">
						<label class="control-label col-sm-2">&nbsp;</label>
						<div class="col-sm-10">
						<a href="<?= site_url('adminweb/produk-kemenag/dokumen.asp') ?>" class="btn btn-default">Kembali</a>
							<?php echo form_submit('submit','Tambah Dokumen','class="btn btn-primary"'); ?>
						</div>
				</div>
				
			<?php echo  form_close(); ?>
		</section>
	</div>
</div>

<script>
	
	function get_sub_kat(data) {
		var data = data.value;
		// alert(data);
		$.ajax({
			url: '<?= site_url('adminweb/get-sub-kat'); ?>',
			type: 'GET',
			dataType: 'html',
			data: {data: data},
			beforeSend : function(data){

			}, 

			success : function(data){
				$("#sub").html(data);
			}
		});
		

	}

	function closeFile(data){
		var data = data.value;
		if (data != '') {
			$("#file").html('');
		}else{
			var datahtml = '<div class="row form-group">' +
						'<label class="control-label col-sm-2">File</label>' +
						'<div class="col-sm-10">' +
							'<input type="file" class="form-control" name="file"  />'+
						'</div>' +
						'<?= $this->session->flashdata("pesan") ?>'+
					'</div>'; 
			$("#file").html(datahtml);
		}
	}
</script>