<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_opini extends MY_Model {

    public function __construct() {
        parent::__construct();
        parent::set_table('opini', 'id');
    }


    public function get_page($limit, $start) {
        $this->db->limit($limit, $start);
        return $this->get_all_v_k();
    }

    public function get_all_v_k()
    {
         $d = $this->db->select('a.* , b.nama as foto ,  c.nama as nama_kategori')
                ->from('opini a ')
                ->join('fm b', 'a.ts = b.id' ,'left')
                ->join('kategori c' , 'a.kategori =  c.id' , 'left' )
                ->where('hapus' , '0')
                ->where(array('a.setuju' => '1'))
                ->order_by('a.tgl_input' , 'desc')
                ->order_by('a.id' , 'desc')
                ->get();
        return $d->result();
    }

    public function get_all_v(){
    	$d = $this->db->select('a.* , b.nama as foto ,  c.nama as nama_kategori')
    			->from('opini a ')
    			->join('fm b', 'a.ts = b.id' ,'left')
    			->join('kategori c' , 'a.kategori =  c.id' , 'left' )
    			->where(array('a.hapus' => '0'))
    			->order_by('a.tgl_input' ,'desc')
                ->order_by('a.id' , 'desc')
    			->get();
    	return $d->result();
    }
    

    public function get_by_v($data)
    {
    	$d = $this->db->select('a.* , b.nama as foto ,  c.nama as nama_kategori')
    			->from('opini a ')
    			->join('fm b', 'a.ts = b.id' ,'left')
    			->join('kategori c' , 'a.kategori =  c.id' , 'left' )
    			->where($data)
    			->get();
    	return $d->row();
    }

    public function get_many_by_v($data , $limit = 50)
    {
        $d = $this->db->select('a.* , b.nama as foto ,  c.nama as nama_kategori')
                ->from('opini a ')
                ->join('fm b', 'a.ts = b.id' ,'left')
                ->join('kategori c' , 'a.kategori =  c.id' , 'left' )
                ->where('hapus' , '0')
                ->where($data)
                ->order_by('a.tgl_input' , 'desc')
                ->order_by('a.id' , 'desc')
                ->limit($limit)
                ->get();
        return $d->result();
    }

    

}
