$(document).ready(function () {
    baseURL = "http://" + location.host + "/";
      $("#runtext").autocomplete({
        minLength: 1,
        source:
          function(req, add){
            var csrf = $("[name='csrf_bappeda_kota']").val();
            $.ajax({
              url: baseURL+"runningtext/search",
              dataType: 'json',
              type: 'POST',
              data: 'req'+"&csrf_bappeda_kota="+csrf,
              success:
                function(data){
                  if (data.response == 'true') {
                    add(data.message);
                  }
                }
            });
          },
      });
     $("#list_dct").dataTable({
        "bPaginate": true,
        "bLengthChange": true,
        "bFilter": true,
        "bSort": false,
        "bInfo": true,
        "bAutoWidth": false
    });
    var kat = $("#kategori").val();
    if (kat == '2'){
       $("#leg").show();
    }else{
        $("#leg").hide();
    }

    $("#list_kategori").dataTable({
        "bPaginate": true,
        "bLengthChange": true,
        "bFilter": true,
        "bSort": false,
        "bInfo": true,
        "bAutoWidth": false
    });
    $("#list_dct").dataTable({
        "bPaginate": true,
        "bLengthChange": true,
        "bFilter": true,
        "bSort": false,
        "bInfo": true,
        "bAutoWidth": false
    });
    $("#tbl_user").dataTable({
        "bPaginate": true,
        "bLengthChange": true,
        "bFilter": true,
        "bSort": false,
        "bInfo": true,
        "bAutoWidth": false
    });
    $('#list_article').dataTable({
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": baseURL + 'adminweb/article/datatable',
                "bJQueryUI": true,
                "aLengthMenu": [
                    [10, 50, 100],
                    [10, 50, 100] // change per page values here
                ],
                "sPaginationType": "full_numbers",
                "iDisplayStart ": 20,
                "sPaginationType": "bootstrap",
                        "oLanguage": {
                            "sProcessing": "<img src='" + baseURL + "asset/ajax-loader_dark.gif'>",
                            "sLengthMenu": "_MENU_ records",
                            "oPaginate": {
                                "sPrevious": "Prev",
                                "sNext": "Next"
                            }
                        },
                "fnInitComplete": function () {
                    //oTable.fnAdjustColumnSizing();
                },
                'fnServerData': function (sSource, aoData, fnCallback) {
                    $.ajax
                            ({
                                'dataType': 'json',
                                'type': 'POST',
                                'url': sSource,
                                'data': aoData,
                                'success': fnCallback
                            });
                }
            });
});
function buat_article() {
    $.ajax({
        type: "GET",
        url: baseURL + 'adminweb/article/buat',
        dataType: "html",
        success: function (data) {
            $("#list_artikel").html(data);
        },
        error: function (XMLHttpRequest) {
            alert(XMLHttpRequest.responseText);
        }

    })
}
;
function fungsiambildata(nilai) {
    $.ajax({
        type: "GET",
        url: baseURL + 'data/bankdata/get_subkategori/'+nilai,
        success: function (data) {
            $("#sub_kat").html(data);
            if (nilai == 2){
                $("#leg").show();
            }else{
                $("#leg").hide();
            }
        },
        error: function (XMLHttpRequest) {
            alert(XMLHttpRequest.responseText);
        }

    })

};
function fungsiambildataa(nilai) {
    var main = $("#kategori").val();
    if (nilai == '1' && main == '2' ){
        $('#leg').show();
    }else{
        $('#leg').hide();
    }
};
function hideWidget(){
    $("#widgetGambar").hide();
}
function showWidget(){
    $("#widgetGambar").show();
}

function get_sub() {
    var csrf = $("[name='csrf_bappeda_kota']").val();
    var id = $("#mainkat").val();
    $.ajax({
        type: "POST",
        url: baseURL + "dokumen/grab_sub",
        data: "id_main=" + id+"&csrf_bappeda_kota="+ csrf,
        success: function (data) {
            $("#sub").html(data);
        },
        error: function (XMLHttpRequest) {
            // alert(XMLHttpRequest.responseText);
            $("#error").html(XMLHttpRequest.responseText);
        }
    });
}
