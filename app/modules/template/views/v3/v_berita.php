<div class="top-popular-courses-area ">
    <div class="container">
        <div class="row">
            <div class="col-sm-9">
                <div class="row">
                    <div class="col-12" style="margin-bottom: -44px;">
                        <div class="section-heading text-center mx-auto wow fadeInUp" data-wow-delay="300ms">
                            <!-- <span>The Best</span> -->
                            <h3>Berita Terpopuler</h3>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <?php if (!empty($terpopuler1)): ?>
                        <?php foreach ($terpopuler1 as $b1): ?>
                            <?php 
                                $tgl = $b1->tgl_input;
                                $judul = $b1->judul;
                                
                                $judul_fix = flag($judul);


                             ?>
                            <!-- Single Top Popular Course -->
                            <div class="col-12 col-lg-6">
                                <div class="single-top-popular-course d-flex align-items-center flex-wrap mb-10 wow fadeInUp" data-wow-delay="400ms" >
                                    <div class="popular-course-content">
                                        <h5><?php echo strip_tags($b1->judul); ?></h5>
                                        <span><?php echo ts($b1->ts) ?></span>
                                        
                                       <!--  <p><?php //echo substr(strip_tags($b1->isi), 0,50); ?> . . . . .</p> -->
                                        <a href="<?= site_url('berita/' .$b1->id. '/' .$judul_fix) ?>" class="btn academy-btn btn-sm">Baca Selengkapnya</a>
                                    </div>
                                    <div class="popular-course-thumb bg-img" style="height: 250px; background-image: url(<?= base_url('files/gorontalo/file/fotoberita/' . $b1->id. '.jpg'); ?>);"></div>
                                </div>
                            </div>
                        <?php endforeach ?>
                    <?php endif ?>
                </div>

                <!-- galery -->
                <?php $this->load->view('v_galery')?>
                <!-- end galery -->

            </div>


            <div class="col-sm-3" style="margin-top: 93px;">
                
                <div class="latest-blog-posts mb-30 wow fadeInUp" data-wow-delay="300ms">
                    <h5>Link Terkait</h5>
                    <!-- Single Latest Blog Post -->
                    
                    <?php if (!empty($banner1)): ?>
                        <?php foreach ($banner1 as $b1): ?>
                            <?php 
                                if ($b1->grup == '2') {
                                    $link = "#";
                                }else {
                                    $link = $b1->url;
                                    
                                }
                             ?>
                             <div class="single-latest-blog-post d-flex mb-30">
                                 <div >
                                    <a href="<?php echo $link ?>">
                                         <img src="<?= base_url('files/gorontalo/file/banner/'.$b1->filegambar) ?>" alt="">
                                    </a>
                                 </div>
                                 <!-- <div class="latest-blog-post-content">
                                     <a href="#" class="post-title">
                                         <h6>New Courses for you</h6>
                                     </a>
                                     <a href="#" class="post-date">March 18, 2018</a>
                                 </div> -->
                             </div>
                            <!-- <a href="<?php echo $link ?>"><img src="<?= base_url('files/gorontalo/file/banner/'.$b1->filegambar) ?>" alt="benner"></a> -->
                        <?php endforeach ?>
                    <?php endif ?>
                    
                </div>
            </div>
        </div>
        
    </div>
</div>
<!-- ##### Top Popular Courses Area End #####