<div class="panel panel-theme shadow  wow zoomIn">
  <div class="panel-heading">
    <h3 class="panel-title">KALKULATOR ZAKAT</h3>
  </div>
  <div class="panel-body ">
    <!-- panel zakat -->
    <div class="panel-heading no-padding">
      <ul class="nav nav-tabs">
        <li class="active">
          <a href="#tab2-1" data-toggle="tab">
            <div>
              <span>Emas dan Perak</span>
            </div>
          </a>
        </li>
        <li>
          <a href="#tab2-2" data-toggle="tab">

            <div>
              <span>Uang</span>
            </div>
          </a>
        </li>
        <li>
          <a href="#tab2-3" data-toggle="tab">

            <div>
              <span>Perniagaan</span>
            </div>
          </a>
        </li>
        <li>
          <a href="#tab2-4" data-toggle="tab">

            <div>
              <span>Pertanian</span>
            </div>
          </a>
        </li>
        <li>
          <a href="#tab2-5" data-toggle="tab">
            <div>
              <span>Pendapatan & Jasa</span>
            </div>
          </a>
        </li>
        <li>
          <a href="#tab2-6" data-toggle="tab">
            <div>
              <span>Personal details</span>
            </div>
          </a>
        </li>
        <li>
          <a href="#tab2-7" data-toggle="tab">
            <div>
              <span>Rikaz (Temuan)</span>
            </div>
          </a>
        </li>
      </ul>
    </div><!-- /.panel-heading -->
    <!--/ End tabs heading -->

    <!-- Start tabs content -->
    <div class="panel-body">
      <div class="tab-content">
        <div class="tab-pane fade in active" id="tab2-1">
        <div >
            <?= $this->load->view('zakat/v_emas_perak') ?>
          </div>
        </div>
        <div class="tab-pane fade" id="tab2-2">
        <div >
            <?= $this->load->view('zakat/uang') ?>
          </div>
        </div>
        <div class="tab-pane fade" id="tab2-3">
        <div >
            <?= $this->load->view('zakat/perniagaan') ?>
          </div>
        </div>
        <div class="tab-pane fade" id="tab2-4">
        <div >
            <?= $this->load->view('zakat/pertanian') ?>
          </div>
        </div>
        <div class="tab-pane fade" id="tab2-5">
        <div >
            <?= $this->load->view('zakat/perternakan') ?>
          </div>
        </div>
        <div class="tab-pane fade" id="tab2-6">
        <div >
            <?= $this->load->view('zakat/pendapatan_jasa') ?>
          </div>
        </div>
        <div class="tab-pane fade" id="tab2-7">
        <div >
            <?= $this->load->view('zakat/rikaz') ?>
          </div>
        </div>

      </div>
    </div><!-- /.panel-body -->
    <!--/ End tabs content -->
  </div><!-- /.panel -->
</div>




