<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Aplikasi extends MX_Controller {

    

    public function __construct() {
        parent::__construct();
        if (!$this->autentifikasi->sudah_login())
            redirect('adminpage/site-login.asp','refresh');
        $this->load->model('m_aplikasi');
        
    }

    public function index() {
    	$data['list'] = $this->m_aplikasi->get_all();
        $data['module'] = "aplikasi";
        $data['view_file'] = "list";
        echo Modules::run('template/render_master',$data);
    }
    public function add(){
        if ($this->input->post()) {
            $config['upload_path']="./uploads/aplikasi/";
            $config['allowed_types']="jpg|png";
            $config['max_size']="1024";
            $config['max_width']="2000";
            $config['max_height']="2000";
            $config['encrypt_name']= TRUE;
            $this->upload->initialize($config);
            $idincrement = auto_inc('m_aplikasi','id_aplikasi');
            if ($this->upload->do_upload('img')) {
                $data= array(
                    'id_aplikasi'=> $idincrement,
                    'nama'=>$this->input->post('nama'),
                    'img'=>$this->upload->file_name,
                    'link'=>$this->input->post('link'),
                    );
                if (!$this->m_aplikasi->insert($data)){
                    echo "<script>alert('Aplikasi berhasil disimpan');
            window.location=('" . site_url('adminweb/aplikasi.asp') . "');</script>";
                }

            }
        }
        $data['kategori']=list_kategori();
        $data['module']="aplikasi";
        $data['view_file']="add_aplikasi";
        echo Modules::run('template/render_master',$data);
    }
    public function edit($id){
        if ($cek = $this->m_aplikasi->get_by(array('id_aplikasi'=>$id))){
            $config['upload_path']="./uploads/aplikasi/";
            $config['allowed_types']="jpg|png";
            $config['max_size']="1024";
            $config['max_width']="2000";
            $config['max_height']="1000";
            $config['encrypt_name']= TRUE;
            $this->upload->initialize($config);
            if ($this->input->post()) {
                if ($this->upload->do_upload('img')) {
                    $pathgambar = realpath(APPPATH . '../uploads/aplikasi/');
                    if ($cek->img != '') {
                        unlink($pathgambar . '/' . $cek->img);
                    }
                       $img = $this->upload->data();
                        $data= array(
                            'nama'=>$this->input->post('nama'),
                            'img'=>$this->upload->file_name,
                            'link'=>$this->input->post('link'),
                            );
                        $this->m_aplikasi->update($id,$data);
                        redirect('adminweb/aplikasi.asp','refresh');
                    }else {
                     $data= array(
                            'nama'=>$this->input->post('nama'),
                            'link'=>$this->input->post('link'),
                            );
                        $this->m_aplikasi->update($id,$data);
                        redirect('adminweb/aplikasi.asp','refresh');
                }   
            }
        }
        $edit = $this->m_aplikasi->get_by(array('id_aplikasi'=>$id));
        $data['edit']= $edit;
        $data['kategori']=list_kategori();
        $data['module']="aplikasi";
        $data['view_file']="edit_aplikasi";
        echo Modules::run('template/render_master',$data);
    }
    public function hapus($id){
        if ($cek = $this->m_aplikasi->get_by(array('id_aplikasi'=>$id))){
            $pathgambar = realpath(APPPATH . '../uploads/aplikasi/');
            if ($cek->img != '') {
                unlink($pathgambar . '/' . $cek->img);
            }
            $this->m_aplikasi->delete($id);
        }
        redirect('adminweb/aplikasi.asp');
    }

    

}
