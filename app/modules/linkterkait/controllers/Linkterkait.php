<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Linkterkait extends MX_Controller {

    

    public function __construct() {
        parent::__construct();
        if (!$this->autentifikasi->sudah_login())
            redirect('adminpage/site-login.asp','refresh');
        $this->load->model('m_link');
    }

    public function index() {
    	  $data['list'] = $this->m_link->get_all();
        $data['module'] = "linkterkait";
        $data['view_file'] = "list";
        echo Modules::run('template/render_master',$data);
    }
    public function  add(){
      $idincrement = auto_inc('m_link','id_link');
      if ($this->input->post()) {
            $config['upload_path']="./uploads/";
            $config['allowed_types']="jpg|png";
            $config['max_size']="1024";
            $config['max_width']="3000";
            $config['max_height']="3000";
            $config['encrypt_name']= TRUE;
            $this->upload->initialize($config);
            if ($this->upload->do_upload('img')) {
              $data = array(
                'id_link'=> $idincrement,
                'img'=>$this->upload->file_name,
                'nama'=> $this->input->post('nama'),
                'link'=> $this->input->post('link'),
              );
              if (!$this->m_link->insert($data)){
                    echo "<script>alert('Link Terkait berhasil disimpan dengan Gambar');
            window.location=('" . site_url('adminweb/linkterkait.asp') . "');</script>";
                }else{
                    echo "<script>alert('Gagal simpan Link Terkait');
            window.location=('" . site_url('adminweb/linkterkait.asp') . "');</script>";
                }
              
            }
            else{
              $data = array(
                'id_link'=> $idincrement,
                'nama'=> $this->input->post('nama'),
                'link'=> $this->input->post('link'),
              );
              if (!$this->m_link->insert($data)){
                    echo "<script>alert('Link Terkait berhasil disimpan Tanpa Gambar');
            window.location=('" . site_url('adminweb/linkterkait.asp') . "');</script>";
                }else{
                    echo "<script>alert('Gagal simpan Link Terkait');
            window.location=('" . site_url('adminweb/linkterkait.asp') . "');</script>";
                }
            }
        
      }
      $data['module'] = "linkterkait";
      $data['view_file'] = "add_link";
      echo Modules::run('template/render_master',$data);
    }
    public function edit($id){
      if ($cek = $this->m_link->get_by(array('id_link'=>$id))){
            $nama_file = "file_". time();
            $config['upload_path']="./uploads/";
            $config['allowed_types']="jpg|png";
            $config['max_size']="1024";
            $config['max_width']="2000";
            $config['max_height']="1000";
            $config['encrypt_name']= TRUE;
            $this->upload->initialize($config);
            if ($this->input->post()) {
                if ($this->upload->do_upload('img')) {
                    $pathgambar = realpath(APPPATH . '../uploads/');
                    if ($cek->img != '') {
                        unlink($pathgambar . '/' . $cek->img);
                    }
                       $img = $this->upload->data();
                        $data = array(
                            'img'=>$this->upload->file_name,
                            'nama'=> $this->input->post('nama'),
                            'link'=> $this->input->post('link'),
                          );
                        $this->m_link->update($id,$data);
                        redirect('adminweb/linkterkait.asp','refresh');
                    }else {
                         $data = array(
                            'nama'=> $this->input->post('nama'),
                            'link'=> $this->input->post('link'),
                          );
                        $this->m_link->update($id,$data);
                        redirect('adminweb/linkterkait.asp','refresh');
                }   
            }
        }
    $data['edit']=$this->m_link->get_by(array('id_link'=>$id));
    $data['module'] = "linkterkait";
    $data['view_file'] = "edit_link";
    echo Modules::run('template/render_master',$data);
  }
  public function hapus($id){
      if ($cek = $this->m_link->get_by(array('id_link'=>$id))){
            $pathgambar = realpath(APPPATH . '../uploads/');
            if ($cek->img != '') {
                unlink($pathgambar . '/' . $cek->img);
            }
            $this->m_link->delete($id);
        }
        redirect('adminweb/linkterkait.asp');
  }
}
