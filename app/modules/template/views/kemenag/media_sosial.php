<div class="sosmed-box shadow">
    <div class="title-box">
        <span class="fl">Media Sosial</span>
        <div class="clearfix"></div>
    </div>
    <div class="tab-sos fr" id="tab1">
        <a href="#tw" class="selected">
            <img src="<?= base_url() ?>asset/kemenag/assets/themes/gorontalo/img/icon_tw.png" alt="">
            <span class="arrow"></span>
        </a>
        <a href="#fb">
            <img src="<?= base_url() ?>asset/kemenag/assets/themes/gorontalo/img/icon_fb.png" alt="">
            <span class="arrow"></span>
        </a>
    </div>
    <div class="sosmed-isi">
        <div id="tw" class="si-isi">
            <div class="t2">Twitter</div>
            <div class="list-tw-box">
                <a class="twitter-timeline" href="https://twitter.com/kemenagGtlo"  width="284" height="270" data-chrome="nofooter noborders noheader"></a>
            </div>
            <div align="center">
                <a href="https://twitter.com/kemenagGtlo" target="_blank" class="more-tw">
                    <img src="<?= base_url() ?>asset/kemenag/assets/themes/gorontalo/img/icon_tw.png" alt="">
                    <span>Follow Us</span>
                </a>
            </div>
        </div>
        <div id="fb" class="si-isi">
            <div class="t2">Facebook</div>
            <div class="list-tw-box">
                <div data-hide-cover="true" class="fb-page" data-href="https://www.facebook.com/kanwilkemenaggorontalo/" data-width="284" data-height="270" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="false" data-show-posts="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/KementerianAgamaRI"><a href="https://www.facebook.com/KementerianAgamaRI">Kementerian Agama RI</a></blockquote></div></div>
            </div>
            <div align="center">
                <a href="https://www.facebook.com/kanwilkemenaggorontalo/" target="_blank" class="more-tw more-fb">
                    <img src="<?= base_url() ?>asset/kemenag/assets/themes/gorontalo/img/icon_fb.png" alt="">
                    <span>Like</span>
                </a>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>