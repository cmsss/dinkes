<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends MX_Controller {

    public function __construct() {
        parent::__construct();
if (!$this->autentifikasi->sudah_login())
            redirect('adminpage/site-login.asp','refresh');
    }

    public function index() {
        $data['module'] = "admin";
        $data['view_file'] = "list";
        echo Modules::run('template/render_master',$data);
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
