<div class="panel panel-tab panel-tab-double shadow wow zoomIn">
    <!-- Start tabs heading -->
    <div class="panel-heading no-padding">
        <ul class="nav nav-tabs">
           
            <li class="active nav-border nav-border-top-info">
                <a href="#tab6-2" data-toggle="tab" class="text-center">
                    <i class="fa fa-twitter"></i>
                </a>
            </li>
            
            <li class="nav-border nav-border-top-primary">
                <a href="#tab6-4" data-toggle="tab" class="text-center">
                    <i class="fa fa-facebook"></i>
                   
                </a>
            </li>
            
            
        </ul>
    </div><!-- /.panel-heading -->
    <!--/ End tabs heading -->

    <!-- Start tabs content -->
    <div class="panel-body">
        <div class="tab-content">
            
            <div class="tab-pane fade in active" id="tab6-2">
                <div class="blog-twitter">

					<a class="twitter-timeline " href="https://twitter.com/kemenagGtlo" height="150"   data-chrome="nofooter noborders noheader">
						
					</a>
				</div>
				<a class="btn btn-info center-block" href="https://twitter.com/kemenagGtlo"  data-chrome="nofooter noborders noheader">Follow Us</a>
            </div>
           
            <div class="tab-pane fade" id="tab6-4">
                <div class="table-responsive">
                	<div data-hide-cover="true" class="fb-page img-responsive" data-href="https://www.facebook.com/kanwilkemenaggorontalo/"   data-height="150"  data-tabs="timeline" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="true">	
						<div class="fb-xfbml-parse-ignore">
								<blockquote cite="https://www.facebook.com/KementerianAgamaRI">
									<a href="https://www.facebook.com/KementerianAgamaRI">Kementerian Agama RI</a>
								</blockquote>
							</div>
					</div>
                </div>
				 <a class="btn btn-primary center-block" href="https://www.facebook.com/kanwilkemenaggorontalo/"  data-chrome="nofooter noborders noheader">Like
				</a>
            </div>
            
        </div>
    </div><!-- /.panel-body -->
    <!--/ End tabs content -->
</div><!-- /.panel -->







