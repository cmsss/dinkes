<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		<?php echo $judul1?>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Dashboard</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">

	<div class="row">
		<!-- Left col -->
		<section class="col-lg-12 connectedSortable">
			<!-- TO DO List -->
			<div class="box box-primary">
				<div class="box-header">
					<i class="ion ion-clipboard"></i>
					<h3 class="box-title"><?php echo $judul2;?></h3>

					<a href="<?php echo site_url('cpanel/berita')?>" class="btn btn-sm btn-danger pull-right" data-toggle="tooltip" title="Kembali ke Halaman <?php echo $judul1?>"> <i class="fa fa-reply"></i> <?php echo $judul1;?></a>

				</div><!-- /.box-header -->
				<div class="box-body">
					<?php echo form_open('','class="form-horizontal" id="formTambah"'); ?>
						<div class="form-group">
		                    <label  class="control-label col-sm-2">Gambar</label>
		                    <div class="col-sm-10">
		                        <span class="pull-right" style="color: red">Format : png/jpg </span>
		                        <input class="form-control" type="file" accept="file" name="imgInp" id="imgInp" >
								<textarea id='imagedata' name='imagedata' style="display: none;" ></textarea>
		                        <textarea id='tes' name='tes' style="display: none;" ></textarea>
		                    </div>
		                </div>
		                <div class="form-group">
		                    <div class="col-sm-12">
		                        <center><canvas id="canvas" width=100 height=100></canvas></center>
		                    </div>
		                </div>
						<div class="form-group">
							<label class="control-label col-sm-2">Judul</label>
							<div class="col-sm-10">
								<input type="text" name="judul" id="judul" class="form-control" placeholder="Judul">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-2">Kategori</label>
							<div class="col-sm-10">
								<input type="text" name="caption" id="caption" class="form-control" placeholder="Caption">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-2"></label>
							<div class="col-sm-10">
								<textarea  name="isi" id="isi" class="ckeditor" rows="20" cols="80" style="width: 100%;" placeholder=""></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-2">Caption</label>
							<div class="col-sm-10">
								<input type="text" name="caption" id="caption" class="form-control" placeholder="Caption">
							</div>
						</div>
						<div class="form-group">
		                    <label class="control-label col-sm-2"></label>
		                    <div class="col-sm-10">
		                        <div id="simpanData">                            
		                            <button type="button" onclick="simpanData()" class="btn btn-primary">Simpan</button>
		                        </div>
		                    </div>
		                </div>
					<?php echo form_close(); ?>
				</div><!-- /.box-body -->
				<div class="box-footer clearfix no-border">

				</div>
			</div><!-- /.box -->



		</section><!-- /.Left col -->
		<!-- right col (We are only adding the ID to make the widgets sortable)-->

	</div><!-- /.row (main row) -->

</section><!-- /.content -->


<script>
	var canvas=document.getElementById("canvas");
    var ctx=canvas.getContext("2d");
    var cw=canvas.width;
    var ch=canvas.height;

    // limit the image to 150x100 maximum size
    var maxW=200;
    var maxH=200;

    var input = document.getElementById('imgInp');
    input.addEventListener('change', handleFiles);

    function handleFiles(e) {
        // console.log(e.target.result);
      var img = new Image;
      var reader = new FileReader();
      reader.onload = function (dc) {
            // console.log(dc);
            // $('#blah').attr('src', e.target.result);

        }
        // console.log(input.files[0]);
        $('#tes').val(input.files[0].name);
        reader.readAsDataURL(input.files[0]);

      img.onload = function(d) {
        // console.log(d.target);
        var iw=img.width;
        var ih=img.height;
        var scale=Math.min((maxW/iw),(maxH/ih));
        var iwScaled=iw*scale;
        var ihScaled=ih*scale;
        canvas.width=iwScaled;
        canvas.height=ihScaled;
        ctx.drawImage(img,0,0,iwScaled,ihScaled);
        $('#imagedata').val(btoa(canvas.toDataURL()));
        // console.log(img);
      }
      img.src = URL.createObjectURL(e.target.files[0]);
    }

    function simpanData(){
      
      // var formdata = $('#dataData').serialize();
      var file_data = $("#imgInp").prop("files")[0];
      var formData = new FormData();
      formData.append("file", file_data);
      $.each($('#dataData').serializeArray(), function(a, b){
        formData.append(b.name, b.value);
      });
      // console.log()

       
        if ($("#imgInp").val() == '') {
            $.toaster({priority: 'info', title: 'Perhatian!', message: 'upload Gamabar'});
            $('#imgInp').focus();
        }
          else {
            // console.log(data);
            $.ajax({
                type: 'POST',
                url: "",
                // data: formdata,
                data: formData,
                dataType: 'html',
                contentType: false,       // The content type used when sending data to the server.
                // enctype: 'multipart/form-data',
                processData: false,  // Important!
                // contentType: 'multipart/form-data',
                cache: false,
                timeout: 600000,
                beforeSend: function() {
                    // setting a timeout
                    $("#simpanData").html('Loading ...');
                },
                success: function (status) {
                	console.log(status);
                },
                error: function (status) {
                    $.toaster({priority: 'error', title: 'Error', message: 'Gagal Tambah Data'});
                    $("#simpanData").html('<button type="button"  onclick="simpanData();" class="btn btn-primary">Simpan</button>');
                }

            });
             return false;
        }
    }
</script>
