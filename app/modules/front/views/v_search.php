<div class="panel panel-default">
  <div class="panel-heading">Hasil Pencarian</div>
  <div class="panel-body">
     <?php 
      if (!empty($result)){
        foreach ($result as $r) { 
            $tgl = explode(' ', $r->create_date);
            $isi = substr($r->isi, 0,250);
        ?>
          <div class="media">
              <h4 class="media-heading">
               <b><a style="color: black; text-decoration: none;" href="<?php echo site_url('artikel/' . $r->flag); ?>"><?php echo $r->judul; ?></a></b><br/>
              </h4>
              <a href="#" class="pull-left">
             <?php if ($r->img !='') {?>
              <img src="<?php echo base_url(); ?>image/berita/<?= $r->img ?>" class="mendia-object img-responsive img-thumb" style="width:100px; height: 100px;"/>
            <?php } ?>
              
          </a>
          <div class="media-body">
                 <?= $isi; ?>
              <a href="<?php echo site_url('artikel/' . $r->flag); ?>" style="color:#000;">[...]</a>
          </div>
          </div>
        <?php }
      }else{
        echo 'Not Found';
      }
     ?>
  </div>
</div>