<style type="text/css">


/*.contain {
  width: 100%;
}*/
.rows {
  overflow: scroll;
  width: 100%;
}
.row__inner {
  transition: 450ms -webkit-transform;
  transition: 450ms transform;
  transition: 450ms transform, 450ms -webkit-transform;
  font-size: 0;
  white-space: nowrap;
  padding-bottom: 10px;
}
.tile {
  position: relative;
  display: inline-block;
  width: 200px;
  height: 140.625px;
  margin-right: 10px;
  font-size: 20px;
  cursor: pointer;
  transition: 450ms all;
  -webkit-transform-origin: center left;
          transform-origin: center left;
}
.tile__img {
  width: 250px;
  height: 140.625px;
  -o-object-fit: cover;
     object-fit: cover;
}

.tile:hover .tile__details {
  opacity: 1;
}
.tile__title {
  position: absolute;
  bottom: 0;
  padding: 10px;
}
.row__inner:hover {
  -webkit-transform: translate3d(-62.5px, 0, 0);
          transform: translate3d(-62.5px, 0, 0);
}
.row__inner:hover .tile {
  opacity: 0.3;
}
.row__inner:hover .tile:hover {
  -webkit-transform: scale(1.5);
          transform: scale(1.5);
  opacity: 1;
}
.tile:hover ~ .tile {
  -webkit-transform: translate3d(125px, 0, 0);
          transform: translate3d(125px, 0, 0);
}
</style>
<section >
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-heading text-center mx-auto wow fadeInUp" data-wow-delay="400ms">
                </div>
            </div>
        </div>

       <!--  <div class="row" >
            <?php if (!empty($profilpejabat)): ?>
                <?php foreach ($profilpejabat as $key => $value): ?>
                    <div class="col-12 col-sm-6 col-lg-2" >
                        <div class="single-teachers-area text-center  wow fadeInUp" data-wow-delay="400ms">
                            <div class="teachers-thumbnail">
                                <img style="object-fit: cover; width: 160px;height: 182px;" src="<?= base_url('uploads/'.$value->img) ?>" alt="" >
                            </div>
                            <div class="teachers-info mt-30">
                                <h5><?php echo $value->nama?></h5>
                                <span> <?php echo $value->jabatan?></span>
                            </div>
                        </div>
                    </div>
                <?php endforeach ?>
            <?php endif ?>
            
            
        </div> -->

        <div class="contain">

          

          <div class="rows">
            <div class="row__inner">
                <?php if (!empty($profilpejabat)): ?>
                <?php foreach ($profilpejabat as $key => $value): ?>
                    <div class="tile ">
                      <div class="tile__media ">
                        <!-- <img class="tile__img" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/70390/show-1.jpg" alt=""  /> -->
                        <img class="tile__img" style="object-fit: cover; width: 160px;height: 182px;" src="<?= base_url('uploads/'.$value->img) ?>" alt="" >
                        
                      </div>
                      <div class="teachers-info mt-30 ">
                            <h5 style="font-weight: 400;margin-bottom: 3px;"><?php echo $value->nama?></h5>
                            <span style="color: #69bc5f;font-size: 14px;"> <?php echo ($value->jabatan == '' ? '-' : $value->jabatan)?></span>
                        </div>
                    </div>

                    
                <?php endforeach ?>
            <?php endif ?>
              

              

            </div>
          </div>

        </div>
    </div>
</section>

