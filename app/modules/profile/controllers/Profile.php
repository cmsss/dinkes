<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Profile extends MX_Controller {

    
    public function __construct() {
        parent::__construct();
        if (!$this->autentifikasi->sudah_login())
            redirect('adminpage/site-login.asp','refresh');
        $this->load->model('m_profile');
    }

    public function index() {
        if ($this->input->post()){
            $data = array(
                'full_name' => $this->input->post('nama_lengkap'),
                'gender' => $this->input->post('gender'),
                'email' => $this->input->post('email'),
                'religion' => $this->input->post('agama'),
                'phone' => $this->input->post('hp')
            );
            $this->m_profile->update_by(array('user_id'=>$this->session->userdata('id_login')),$data);

            $dataUser = array(
                'display_name' => $this->input->post('nama_lengkap')
            );

            $this->m_user->update_by(array('id_user' => $this->session->userdata('id_login')) , $dataUser);
            redirect('adminweb/user/profile.asp','refresh');
        }
    	$data['list'] = $this->m_profile->get_by(array('user_id'=>$this->session->userdata('id_login')));
        $data['module'] = "profile";
        $data['view_file'] = "list";
        echo Modules::run('template/render_master',$data);
    }
}
