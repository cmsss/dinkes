<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_artikel extends MY_Model {
    
    public function __construct(){
        parent::__construct();
        parent::set_table('t_artikel','id_artikel');
    }

    public function  get_all_v_artikel()
    {
    	$d = $this->db->select('a.* , b.display_name as nama')
    			->from('t_artikel a')
    			->join('t_user b' , 'a.penulis = b.id_user' , 'left')
    			->order_by('id_artikel' , 'desc')
    			->get();
    	return $d->result();
    }
 }