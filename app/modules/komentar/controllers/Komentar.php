<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Komentar extends MX_Controller {
    public function __construct() {
        parent::__construct();
        if (!$this->autentifikasi->sudah_login())
            redirect('adminpage/site-login.asp','refresh');
        $this->load->model('m_komentar');
    }
    public function index() { 
        $data['list1'] = $this->m_komentar->get_all();
        $data['module'] = "komentar";
        $data['view_file'] = "list";
        echo Modules::run('template/render_master',$data);
        
    }
    public function list_komentar() {
        if ($this->input->post()) {
            $this->m_komentar->update($this->input->post('id_komentar', TRUE), array('status' => '1'));
            redirect(site_url('adminweb/komentar.asp'));
        } else {
            $data['jumlah_coment']=$this->m_komentar->countstatus0();
            $data['komentar'] = $this->m_komentar->komentar();
            $data['komentar_all'] = $this->m_komentar->get_coment_all();
            $data['module'] = "komentar";
            $data['view_file'] = "list";
            echo Modules::run('template/render_master',$data);
        }
    }

    public function hapus($id){
        $this->m_komentar->delete($id);
        redirect('adminweb/komentar.asp');
    }
}
