<div class="panel panel-theme shadow blog-list-slider wow zoomIn">
    <div class="panel-heading">
        <h3 class="panel-title">Renungan</h3>
    </div>
    <div id="carousel-blog-list" class="carousel slide" data-ride="carousel">
        

        <div class="carousel-inner">

            


            <?php
                $no_renung = 0; 
                if (!empty($renungan1)) {
                   
                    foreach ($renungan1 as $l_renungan) {

                       

                        if ($no_renung == 0) {
                            $aktiveinfoa = "active";
                        }else{
                            $aktiveinfoa = '';
                        }
                        $no_renung++
                        ?>

                            <div class="item <?= $aktiveinfoa ?>">
                                <div class="blog-list">
                                    <div class="media">
                                        
                                        <div class="media-body">
                                            <h4 class="media-heading"><?= $l_renungan->isi ?></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        <?php
                    }
                }

            ?>
           
           
        </div>

    </div>
    <div class="panel-body">
        <a class="btn btn-info pull-right" href="<?= site_url('renungna-gorontalo') ?>">Selengkapnya</a>
        
    </div>

</div>