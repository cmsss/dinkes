<div class="shadow box-1">
    <div class="title-box">
        <span class="fl">Informasi Penting</span>
    </div>
    <div class="list-2">
        <a href="<?= base_url() ?>asset/kemenag/artikel/41870/hasil-akhir-pelaksanaan-seleksi-terbuka-jabatan-administrator.html">
            <span></span>
            <h2>Hasil Akhir Pelaksanaan Seleksi Terbuka Jabatan Administrator </h2>
            <div class="clearfix"></div>
        </a>
    </div>
    <div class="list-2">
        <a href="<?= base_url() ?>asset/kemenag/artikel/41834/surat-edaran-tentang-hari-pemilihan.html">
            <span></span>
            <h2>Surat Edaran Tentang Hari Pemilihan</h2>
            <div class="clearfix"></div>
        </a>
    </div>
    <div class="list-2">
        <a href="<?= base_url() ?>asset/kemenag/artikel/41626/pengumuman-seleksi-wawancara.html">
            <span></span>
            <h2>Pengumuman Seleksi Wawancara </h2>
            <div class="clearfix"></div>
        </a>
    </div>
    <div class="list-2">
        <a href="<?= base_url() ?>asset/kemenag/artikel/41277/surat-edaran-sirup.html">
            <span></span>
            <h2>Surat Edaran SIRUP </h2>
            <div class="clearfix"></div>
        </a>
    </div>
    <div class="list-2">
        <a href="<?= base_url() ?>asset/kemenag/artikel/41064/edaran-publikasi-sapu-bersih-pungutan-liar.html">
            <span></span>
            <h2>Edaran Publikasi Sapu Bersih Pungutan Liar</h2>
            <div class="clearfix"></div>
        </a>
    </div>
    <div class="list-2">
        <a href="<?= base_url() ?>asset/kemenag/artikel/39596/optimalisasi-penggunaan-web-portal-kanwil-kementerian-agama-prov-gorontalo.html">
            <span></span>
            <h2>Optimalisasi Penggunaan Web Portal Kanwil Kementerian Agama Prov. Gorontalo</h2>
            <div class="clearfix"></div>
        </a>
    </div>
    <div class="list-2">
        <a href="<?= base_url() ?>asset/kemenag/artikel/39079/seleksi-terbuka-calon-penyuluh-agama-islam-non-pns-tahun-2017.html">
            <span></span>
            <h2>Seleksi Terbuka Calon Penyuluh Agama Islam Non PNS Tahun 2017</h2>
            <div class="clearfix"></div>
        </a>
    </div>
    <div class="list-2">
        <a href="<?= base_url() ?>asset/kemenag/artikel/38685/pengumuman-hasil-akhir-seleksi-jabatan-pengawas-calon-kepala-kua-kecamatan-boliyohuto-mootilango-dan-tilango.html">
            <span></span>
            <h2>Pengumuman Hasil Akhir Seleksi Jabatan Pengawas (Calon Kepala KUA Kecamatan Boliyohuto, Mootilango, dan Tilango)</h2>
            <div class="clearfix"></div>
        </a>
    </div>
    <div class="list-2">
        <a href="<?= base_url() ?>asset/kemenag/artikel/38425/pengumuman-hasil-seleksi-administrasi-calon-jabatan-pengawas-dan-kepala-madrasah-di-lingkungan-kanwil-kementerian-agama-provinsi-gorontalo.html">
            <span></span>
            <h2>Pengumuman Hasil Seleksi Administrasi Calon Jabatan Pengawas dan Kepala Madrasah di lingkungan Kanwil Kementerian Agama Provinsi Gorontalo</h2>
            <div class="clearfix"></div>
        </a>
    </div>
    <div class="pd10" align="center">
        <a href="<?= base_url() ?>asset/kemenag/artikel.html" class="btn_more">Selengkapnya</a>
    </div>
</div>