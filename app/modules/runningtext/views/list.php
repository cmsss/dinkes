<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Run Text
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
      <div class="col-md-12">
          <div class="box box-info">
              <div class="box-header with-border">
                  <h3 class="box-title">Tambah Run Text</h3>
              </div><!-- /.box-header -->
              <!-- form start -->
              <?php echo form_open('', 'class="form-horizontal"'); ?>
              <div class="box-body">
                  <?php echo form_input('run_text', '', 'class="form-control" placeholder="Text Berjalan" id="runtext" required'); ?>
                  <div class="box-footer">
                      <?php echo form_submit('kirim', 'Tambah', 'class="btn btn-primary"'); ?>
                  </div><!-- /.box-footer -->
                  <?php echo form_close(); ?>
              </div>
          </div>
      </div>
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">List</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                    <table class="table table-hover table-condensed" id="list_kategori">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Run Test</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            foreach ($list as $row) {
                                ?>
                                <tr>
                                    <td><?php echo $no; ?></td>
                                    <td><?php echo $row->run_text; ?></td>
                                    <td>
                                        <a href="#edit_kategori_<?php echo $row->id_run_text; ?>" title="edit" class="btn btn-primary" data-toggle="modal"><i class="glyphicon glyphicon-pencil"></i></a>
                                        <a href="<?php echo site_url('runningtext/hapus/' . $row->id_run_text); ?>" title="hapus" class="btn btn-warning" onclick="return confirm('Apakah anda yakin akan menghapus data ini?')"><i class="glyphicon glyphicon-trash"></i></a>
                                    </td>
                                </tr>
                            <div class="modal fade" id="edit_kategori_<?php echo $row->id_run_text; ?>" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title" id="myModalLabel">
                                                Edit Run Text
                                            </h4>
                                        </div>
                                        <?php echo form_open('runningtext/edit'); ?>
                                        <div class="modal-body">
                                            <?php echo form_hidden('id_run_text', $row->id_run_text); ?>
                                            <?php echo form_input('run_text', $row->run_text, 'class="form-control" placeholder="Nama Run Text"'); ?>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>  <?php echo form_submit('kirim', 'Simpan Perubahan', 'class="btn btn-primary"'); ?>
                                        </div>
                                        <?php echo form_close(); ?>
                                    </div>
                                </div>
                            </div>
                            <?php
                            $no++;
                        }
                        ?>
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
      </div>
</section>
