<div class="container-fluid">
	<section class="content-header">
		<h4>Tambah Majalah</h4>
	</section>
	<div class="box box-primary">
		<section class="content">

			<?php echo form_open_multipart('adminweb/majalah/simpan',array('class'=>'form-horizontal')); ?>

			<div class="row form-group">
				<label class="control-label col-sm-2">Judul</label>
				<div class="col-sm-10">
					<?php echo form_input('judul',set_value('judul'),'class="form-control" required placeholder="judul"'); ?>
				</div>
			</div>			
			<div class="row form-group">
				<label class="control-label col-sm-2">File Majalah</label>
				<div class="col-sm-10">
					<span class="pull-right">Format : PDF</span>
					<?php echo form_upload('namapdf','','multiple required'); ?>
				</div>
			</div>
			<div class="row form-group">
				<label class="control-label col-sm-2">cover</label>
				<div class="col-sm-10">
					<span class="pull-right">Format : JPG|PNG</span>
					<?php echo form_upload('namaco','','multiple required'); ?>
				</div>
			</div>
			
			
			
			<div class="row form-group">
				<label class="control-label col-sm-2">Tampil</label>
				<div class="col-sm-10">
					<div class="col-sm-12">
						<input type="radio" name="tampil" value="1" checked style="margin-right: 10px;" >
						<label class="" style="margin-right: 20px;">Ya</label >
						
						<input type="radio" name="tampil" value="0" style="margin-right: 10px; padding-left: 10px;">
						<label class="">Tidak</label>
					</div>
				</div>
			</div>
			<div class="row form-group">
				<label class="control-label col-sm-2">Setuju</label>
				<div class="col-sm-10">
					<div class="col-sm-12">
						<input type="radio" name="setuju" value="1" checked style="margin-right: 10px;" >
						<label class="" style="margin-right: 20px;">Ya</label >
						
						<input type="radio" name="setuju" value="0" style="margin-right: 10px; padding-left: 10px;">
						<label class="">Tidak</label>
					</div>
				</div>
			</div>
				

				<div class="row form-group">

					<div class="col-sm-12">
						<a href="<?= site_url('adminweb/majalah.asp') ?>" class="btn btn-default pull-right">Kembali</a>
						<label class="col-sm-2"></label>
						<?php echo form_submit('submit','Simpan','class="btn btn-primary "'); ?>
					</div>
				</div>

				<?php echo  form_close(); ?>
			</section>
		</div>
	</div>


