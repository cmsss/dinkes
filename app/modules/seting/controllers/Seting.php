<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Seting extends MX_Controller {

	private $module;
    private $redirect;
    private $level;
    private $id_user;

	public function __construct() {
		parent::__construct();
		if (!$this->autentifikasi->sudah_login())
			redirect('adminpage/site-login.asp','refresh');
		$this->load->model('m_seting');

		$this->module = "seting";
        $this->redirect = "adminweb/seting.asp";
        $this->level = $this->session->userdata('level');
        $this->id_user = $this->session->userdata('id_login');

	}

	public function index() {
		$data['l_kategori']=list_kategori();
		$data['list'] = $this->m_seting->get_all();
		$data['module'] = $this->module;
		$data['view_file'] = "list";
		echo Modules::run('template/render_master',$data);
	}

	public function add(){

		if ($this->input->post()) {
			

			$idincrement = auto_inc('m_seting' , 'id');
            $nmfile = $idincrement;
            $config['upload_path']= './uploads/kemenag/';
            $config['allowed_types']="png";
            $config['max_size']="10240000";
            // $config['max_width']="0";
            // $config['max_height']="0";
            // $config['encrypt_name']= TRUE;
            $config['file_name'] = $nmfile; //nama yang terupload nantinya 
            $this->upload->initialize($config);

            if ($this->upload->do_upload('img')) {
                    $file = $this->upload->data();
                    $data  = array(
						'id' => auto_inc('m_seting', 'id'),
						'ket' => $this->input->post('ket'),
						'img' => $file['file_name'],
						);

					$this->m_seting->insert($data);
					echo "<script>alert('Berhasil Simpan');
					window.location=('".site_url('adminweb/seting.asp')."');</script>";
                }else{
					echo "<script>alert('Gagal Simpan');
					window.location=('".site_url('adminweb/seting.asp')."');</script>";

                }
			
		}
		$data['module'] = $this->module;
		$data['view_file'] = 'add';
		echo Modules::run('template/render_master' ,$data);
	}

	/*public function update($id){
		$cek = $this->m_seting->get_by(array('id' => $id));
		if ($cek) {
			if ($this->input->post()) {
				$flagtes = $this->input->post('flagtes');
				$nama = $this->input->post('nama');
				$nama_kecil = strtolower($nama);
				$replece = str_replace(array(' ','/','-','.',','), '-', $nama_kecil);


				$esekusi_bidang = $this->db->query("UPDATE t_profil_pejabat SET devisi = '$replece'  WHERE devisi =  '$flagtes'");


				
				$data = array(
					'nama' => ucwords($nama),
					'flag' => $replece,
					);
				$this->m_seting->update($id , $data);
				echo "<script>alert('Berhasil Update data');
				window.location=('".site_url('adminweb/bidang.asp')."');</script>";
			}else{
				$data['edit'] = $cek; ;
				$data['module'] = 'bidang';
				$data['view_file'] = 'edit_bidang';
				echo Modules::run('template/render_master' , $data);	
			}

			
		}else{
			redirect('adminweb/bidang.asp','refresh');
		}
	}*/


	public function delete($id){
		$cek = $this->m_seting->get_by( array('id' => $id ));
		if ($cek) {




        
            $patfile = realpath(APPPATH . '../uploads/kemenag/');

            if ($cek->img != '') {
                unlink($patfile . '/' . $cek->img);
            }

			$this->m_seting->delete($id);
			echo "<script>alert('Behasil Hapus');
			window.location=('".site_url('adminweb/seting.asp')."');</script>";
		}else {
			redirect('adminweb/seting.asp','refresh');
		}
	}


}
